
#A single .csv file must be present inside the folder. For indriya which contains multiple file use cat *.csv>combine.csv to make it one file and run this script
#Make a note of the columns being accessed , may vary depending on your program. Change the colomn according to your program

import sys
import os
import csv
import numpy as np
from os import walk
from fileinput import close
create_nearest_neighbour_lst=1	
receive=2
corrupt=1
def analyze():
	node_count=0
	parent_list = []
	print "Nodeid", "\t","Neighbour","\t","RSSI","\t","LQI","\t","Pkt_snt","\t","pkt_rcv","\t","prr"
	f = []
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Downloads/81203'):

		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Downloads/81203',name)
			sender_list=[]
			receiver_list=[]
			frequency_list=[]
			round_list=[]
			f = open(fullName, 'r')
			line_no=0;
			#print f
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				try:
					#print columns
					#print(columns[8])
					if(int(columns[3])==1):
						sender_list.append(int(columns[2]))	
					if(int(columns[3])==0):
						receiver_list.append(int(columns[2]))
					if(int(columns[4]) >=2400):
						frequency_list.append(int(columns[4]))
					if(int(columns[6])>0):
						round_list.append(int(columns[6]))
					
				except (IndexError,ValueError):
					gotdata = 'null'
 			f.close()
			sender_set = set(sender_list)
			receiver_set = set(receiver_list)
			freq_set=set(frequency_list)
			round_set=set(round_list)
			print sorted(sender_set)
			print sorted(receiver_set)
			print sorted(freq_set)
			print sorted(round_set)
			node_neighbor_list=[]
			"""
			for sender in sorted(sender_set):
				for k in sorted(freq_set):
					packet_sent_list=[]
					f = open(fullName, 'r')
					for line in f:
						line = line.strip()
						columns = line.split()
						try:
							if int(columns[4])==k and int(columns[3])==1 and sender==int(columns[2]):
								print columns
								packet_sent_list.append(int(columns[3]))
						except (IndexError,ValueError):
								gotdata = 'null'
					f.close()
					print sender,k,len(packet_sent_list)
			"""
			for i in sorted(receiver_set):
				for k in sorted(freq_set):
					f = open(fullName, 'r')
					for line in f:
						line = line.strip()
						columns = line.split()
						try:
							if int(columns[2]) == i and int(columns[4])==k:
								if(int(columns[14])>0 and int(columns[14])<35 ):
									node_neighbor_list.append(int(columns[14]))
						except (IndexError,ValueError):
							gotdata = 'null'
		 			f.close()
					node_neighbor_set = set(node_neighbor_list)
					#print node_neighbor_set
					for j in sorted(node_neighbor_set):
						rssi_list=[]
						lqi_list=[]
						packet_recieved_list=[]
						packet_corrupt_list=[]
						packet_sent_list=[]
						average_rssi=0.0;
						average_lqi=0.0
						prr=0.0
						f = open(fullName, 'r')
						for line in f:
							line = line.strip()
							columns = line.split()
							try:	
								if int(columns[2])==i and  int(columns[4])==k:
									if int(columns[14])==j or int(columns[14])==0:
										if (int(columns[13])==receive):
											rssi_list.append(int(columns[15]))
											lqi_list.append(int(columns[16]))
										if int(columns[13])==receive:
											#print columns
											packet_recieved_list.append(int(columns[13]))
										"""if int(columns[13])==corrupt:
											packet_corrupt_list.append(int(columns[13]))
											print columns"""
							except (IndexError,ValueError):
								gotdata = 'null'
						f.close()
						for sender in sorted(sender_set):
							if(sender==j):
								f = open(fullName, 'r')
								for line in f:
									line = line.strip()
									columns = line.split()
									try:
										#print j
										if (sender==int(columns[2]) and int(columns[4])==k and int(columns[3])==1):	
											#print columns
											packet_sent_list.append(int(columns[3]))
									except (IndexError,ValueError):
										gotdata = 'null'
								f.close()
						if(len(rssi_list)!=0 or len(lqi_list)!=0):
							average_rssi=np.mean(rssi_list)
							average_lqi=np.mean(lqi_list)
							prr=float(len(packet_recieved_list))/float(len(packet_sent_list))
							print i,"\t",k,"\t",j,"\t",round(average_rssi,2),"\t",round(average_lqi,2),"\t",len(packet_recieved_list),"\t",len(packet_sent_list),"\t",round(prr,2)
					node_neighbor_list = []


def main():

    if len(sys.argv) != 1:
        print '...'
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()


