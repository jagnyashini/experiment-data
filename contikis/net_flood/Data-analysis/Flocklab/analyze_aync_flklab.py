import sys
import os
import csv
import math  
import numpy as np
from os import walk
from fileinput import close
sent=1
receive=2
duty_cycle=3
def analyze():
	
	#group1=[3,6,31,4,8,33,16,22,26,20,24,18,13,23,10,32,15,1,2,28,25,19,27,11,17,7,14]
	#group2=[4,2,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40]	
	
	#group1=[27,24,18,17,13,19,23,20,6,28,16,22,10]
	#group2=[15,2,1,4,8,33,3,31,32,26,25,11,7,14]

	group1=[11,13,17,24,20,19,18,6,22,3,26,27,7,14]
	group2=[2,1,8,15,4,33,32,16,31,10,23,28,25]
	group1_initiator=group1[0]
	group2_initiator=group2[0]
	group_ini=0
	f = []
	count_100per_rel=[]
	victim_nodes=[]
	node_list=[]
	packet_sent1=[]
	packet_sent2=[]
	packet_sent1_time=[]
	packet_sent2_time=[]
	cdf=[0.999,0.990,0.985,0.980,0.975,0.900,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0.05,0]
	len_cdf=len(cdf)
	listy = [[] for i in range(20)]
	n_tx=5
	f=[]
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/contikis/net_flood/Data/Flocklab/GRP_2/D_0.5/11_2/G_2/G_2_F_11_2_I_1.5'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/contikis/net_flood/Data/Flocklab/GRP_2/D_0.5/11_2/G_2/G_2_F_11_2_I_1.5',name)
			f = open(fullName, 'r')
			#print fullName
			i=0
			for line in f:
				line = line.strip()
				columns = line.split()
				try:
					if(i>5):
						column1=(columns[0])
						time=column1.split(",")
						#print time[0]
						if(int(columns[3])==group1_initiator and int(columns[1])==sent):
							packet_sent1.append(int(columns[8]))
							packet_sent1_time.append(time[0])
							#print (columns[9])
						if(int(columns[3])==group2_initiator and int(columns[1])==sent):
							packet_sent2.append(int(columns[8]))
							packet_sent2_time.append(time[0])
						node_list.append(int(columns[3]))
					i=i+1
				except (IndexError,ValueError):
					gotdata = 'null'	
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "Group-Initiator\t|\tPacket_Sent"	
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print group1_initiator,"\t\t|\t",len(packet_sent1)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print group2_initiator,"\t\t|\t",len(packet_sent2)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "SNo\t\t" "Node:\t\t""Init_Id\t\t""R_Self_init\t\t""Nt recv\t\t""PRR""\t\t""Duty Cycle(%)""\t\t""hop-count""\t\t""Latency"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#print packet_sent1
	#print packet_sent1_time
	#print packet_sent2
	#print packet_sent2_time	
	#print set(node_list)
	average_hop_count_list=[]
	average_duty_cycle_list=[]
	average_prr_list=[]
	count=0
	for node in set(node_list):
		f=[]
		for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/contikis/net_flood/Data/Flocklab/GRP_2/D_0.5/11_2/G_2/G_2_F_11_2_I_1.5'):
			f.extend(filenames)
			for name in f:
				fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/contikis/net_flood/Data/Flocklab/GRP_2/D_0.5/11_2/G_2/G_2_F_11_2_I_1.5',name)
				f = open(fullName, 'r')
				#print fullName
				i=0
				total_packets_recv=0.0
				packet_sent=0.0
				group_ini=0
				seq_no_recv=[]
				hops_recv=[]
				not_recv=[]
				duty_cycle_list=[]
				node_id=0
				medium=0
				average_hop_count=0.0
				average_duty_cycle=0.0
				average_prr=0.0
				count=count+1
				packet_sent_array=[]
				packet_sent_time=[]	
				for line in f:
					line = line.strip()
					columns = line.split()
					try:
						if(i>1):
							if(int(columns[3])==node):
								column1=(columns[0])
								time=column1.split(",")
								for j in sorted(group1):
									if(j==int(columns[3])):
										group_ini=group1_initiator
										packet_sent=len(packet_sent1)
										packet_sent_array=packet_sent1
										packet_sent_time=packet_sent1_time
								for j in sorted(group2):
									if(j==int(columns[3])):
										group_ini=group2_initiator
										packet_sent=len(packet_sent2)
										packet_sent_array=packet_sent2
										packet_sent_time=packet_sent1_time
								if(float(columns[1])==receive): 
								#print  int(columns[6]) ,group_ini
								#print columns
									seq_no_index=-1
									if(int(columns[6])==group_ini):
										seq_no_index=packet_sent_array.index(int(columns[8]))
										recieve_time=time[0]
										latency=float(recieve_time)-float(packet_sent_time[seq_no_index])
										#print float(recieve_time),float(packet_sent_time[seq_no_index]),latency,seq_no_index
										seq_no_recv.append(int(columns[8]))
										#print columns[9]
										hops_recv.append(int(columns[9]))
									else:
										not_recv.append(int(columns[8]))
								if(float(columns[1])==duty_cycle):  
									duty_cycle_list.append(float(columns[2]))
									node_id=int(columns[3])
									medium=int(columns[4])
						i=i+1
					except (IndexError,ValueError):
						gotdata = 'null'
				#print len(seq_no_recv),packet_sent
			#print not_recv
			total_packets_recv=len(seq_no_recv)
			not_recv=packet_sent-total_packets_recv
			if(len(hops_recv)!=0):
				average_hop_count=np.mean(hops_recv)
				average_hop_count_list.append(average_hop_count)
			average_duty_cycle=np.mean(duty_cycle_list)
			average_duty_cycle_list.append(average_duty_cycle)
			if(packet_sent!=0):
				average_prr=float(total_packets_recv)/float(packet_sent)
				if(node_id!=group_ini):
					average_prr_list.append(average_prr)
			print count,"\t\t",node_id,"\t\t",group_ini,"\t\t\t",total_packets_recv,"\t\t",not_recv,"\t\t",round(average_prr,3),"\t\t",round(average_duty_cycle,3),"\t\t\t",round(average_hop_count,3),"\t\t\t\t\t\t",medium
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	average_hop_count_ALL=np.mean(average_hop_count_list)
	sd_hop_count_ALL=np.std(average_hop_count_list)
	average_duty_cycle_ALL=np.mean(average_duty_cycle_list)
	sd_duty_cycle_ALL=np.std(average_duty_cycle_list)
	average_prr_ALL=np.mean(average_prr_list)
	sd_prr_ALL=np.std(average_prr_list)
	print "\t\t\t\t\t\t\t\t\t\t\t", round(average_prr_ALL,3),"\t\t",round(average_duty_cycle_ALL,3),"\t\t\t",round(average_hop_count_ALL,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "\t\t\t\t\t\t\t\t\t\t\t",round(sd_prr_ALL,3),"\t\t",round(sd_duty_cycle_ALL,3),"\t\t\t",round(sd_hop_count_ALL,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#print packet_sent1
	#print packet_sent1_time
	#print packet_sent2
	#print packet_sent2_time			
def main():
    if len(sys.argv) != 1:
        print('...')
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()

