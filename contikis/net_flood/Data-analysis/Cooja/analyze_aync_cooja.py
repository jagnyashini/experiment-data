import sys
import os
import csv
import math  
import numpy as np
from os import walk
from fileinput import close
sent=1
receive=2
duty_cycle=3
def analyze():
	
	group1=[3,1,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41]
	group2=[4,2,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40]	
	group1_initiator=group1[0]
	group2_initiator=group2[0]
	
	group_ini=0
	f = []
	count_100per_rel=[]
	victim_nodes=[]
	node_list=[]
	cdf=[0.999,0.990,0.985,0.980,0.975,0.900,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0.05,0]
	len_cdf=len(cdf)
	listy = [[] for i in range(20)]
	n_tx=5
	f=[]
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/contikis/net_flood/Data/F_40_2480_result_1'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/contikis/net_flood/Data/F_40_2480_result_1',name)
			f = open(fullName, 'r')
			#print fullName
			i=0
			for line in f:
				line = line.strip()
				columns = line.split()
				try:
					if(i>5):
						#print (columns)
						if(int(columns[4])==group1_initiator and int(columns[2])==sent):
							packet_sent1=int(columns[9])
							#print (columns[9])
						if(int(columns[4])==group2_initiator and int(columns[2])==sent):
							packet_sent2=int(columns[9])
						node_list.append(int(columns[4]))
					i=i+1
				except (IndexError,ValueError):
					gotdata = 'null'	
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "Group-Initiator\t|\tPacket_Sent"	
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print group1_initiator,"\t\t|\t",packet_sent1+1
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print group2_initiator,"\t\t|\t",packet_sent2+1
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "SNo\t\t" "Node:\t\t""Init_Id\t\t""R_Self_init\t\t""Nt recv\t\t""PRR""\t\t""Duty Cycle(%)""\t\t""hop-count""\t\t""Latency"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print set(node_list)
	packet_sent1=packet_sent1+1
	packet_sent2=packet_sent2+1
	average_hop_count_list=[]
	average_duty_cycle_list=[]
	average_prr_list=[]
	count=0
	for node in set(node_list):
		f=[]
		for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/contikis/net_flood/Data/F_40_2480_result_1'):
			f.extend(filenames)
			for name in f:
				fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/contikis/net_flood/Data/F_40_2480_result_1',name)
				f = open(fullName, 'r')
				#print fullName
				i=0
				total_packets_recv=0.0
				packet_sent=0.0
				group_ini=0
				seq_no_recv=[]
				hops_recv=[]
				not_recv=[]
				duty_cycle_list=[]
				node_id=0
				medium=0
				average_hop_count=0.0
				average_duty_cycle=0.0
				average_prr=0.0
				count=count+1
				for line in f:
					line = line.strip()
					columns = line.split()
					try:
						if(i>1):
							if(int(columns[4])==node):
								for j in sorted(group1):
									if(j==int(columns[4])):
										group_ini=group1_initiator
										packet_sent=packet_sent1
								for j in sorted(group2):
									if(j==int(columns[4])):
										group_ini=group2_initiator
										packet_sent=packet_sent2
								if(float(columns[2])==receive): 
								#print  int(columns[6]) ,group_ini
								#print columns
									if(int(columns[7])==group_ini):
										seq_no_recv.append(int(columns[9]))
										#print columns[9]
										hops_recv.append(int(columns[10]))
									else:
										not_recv.append(int(columns[9]))
								if(float(columns[2])==duty_cycle):  
									duty_cycle_list.append(float(columns[3]))
									node_id=int(columns[4])
									medium=int(columns[5])
						i=i+1
					except (IndexError,ValueError):
						gotdata = 'null'
				#print len(seq_no_recv),packet_sent
			#print not_recv
			total_packets_recv=len(seq_no_recv)
			not_recv=packet_sent-total_packets_recv
			if(len(hops_recv)!=0):
				average_hop_count=np.mean(hops_recv)
				average_hop_count_list.append(average_hop_count)
			average_duty_cycle=np.mean(duty_cycle_list)
			average_duty_cycle_list.append(average_duty_cycle)
			if(packet_sent!=0):
				average_prr=float(total_packets_recv)/float(packet_sent)
				if(node_id!=group_ini):
					average_prr_list.append(average_prr)
			print count,"\t\t",node_id,"\t\t",group_ini,"\t\t\t",total_packets_recv,"\t\t",not_recv,"\t\t",round(average_prr,3),"\t\t",round(average_duty_cycle,3),"\t\t\t",round(average_hop_count,3),"\t\t\t\t\t\t",medium
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	average_hop_count_ALL=np.mean(average_hop_count_list)
	sd_hop_count_ALL=np.std(average_hop_count_list)
	average_duty_cycle_ALL=np.mean(average_duty_cycle_list)
	sd_duty_cycle_ALL=np.std(average_duty_cycle_list)
	average_prr_ALL=np.mean(average_prr_list)
	sd_prr_ALL=np.std(average_prr_list)
	print "\t\t\t\t\t\t\t\t\t\t\t", round(average_prr_ALL,3),"\t\t",round(average_duty_cycle_ALL,3),"\t\t\t",round(average_hop_count_ALL,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "\t\t\t\t\t\t\t\t\t\t\t",round(sd_prr_ALL,3),"\t\t",round(sd_duty_cycle_ALL,3),"\t\t\t",round(sd_hop_count_ALL,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
			
def main():
    if len(sys.argv) != 1:
        print('...')
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()

