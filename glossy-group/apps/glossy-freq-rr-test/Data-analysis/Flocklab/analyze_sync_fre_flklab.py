import sys
import os
import csv
import math  
import numpy as np 

from os import walk
from fileinput import close

def analyze():

	group1=[7,17,19,13,14,11]
	group2=[32,25,10,26,20,24,27]
	group3=[18,23,28,31,15,3,4]
	group4=[16,8,1,2,33,22,6]

	group1_initiator=group1[0]
	group2_initiator=group2[0]
	group3_initiator=group3[0]		
	group4_initiator=group4[0]
	#group3_initiator=group2[0]
	group_ini=0
	start_frequency=2472
	f = []
	count=0	
	count_100per_rel=[]
	victim_nodes=[]
	cdf=[1,0.999,0.990,0.985,0.980,0.975,0.900,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0.05,0]
	len_cdf=len(cdf)
	listy = [[] for i in range(20)]
	avg_relaibility_NC=0.0
	avg_damage_NC=0.0
	avg_intrusion_NC=0.0
	average_radio_on_time_total=0.0
	average_latency_total=0.0
	relay_count_total=0.0
	count_radio_on=0.0
	count_latency=0.0
	reliability_all=[]
	damage_all=[]
	intrusion_all=[]
	radio_on_time_all=[]
	average_latency_all=[]
	relay_count_all=[]
	#Average calculation
	count=0
	frequency_diff=[]
	freq_set=[]
	f=[]
	node_id_list=[]
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/glossy-group/apps/glossy-freq-rr-test/Data/Flocklab/G_4/0.3722/7_32_18_16'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/glossy-group/apps/glossy-freq-rr-test/Data/Flocklab/G_4/0.3722/7_32_18_16',name)
			f = open(fullName, 'r')
			#print fullName
			i=0
			self_initiator=[]
			other_initiator=[]
			no_rcv_cnt=[]
			radio_on_time=[]
			relay_count=[]
			latency=[]
			for line in f:
				line = line.strip()
				columns = line.split()
				#print columns
				try:
					if(i>25):
						#print (columns)
						frequency_diff.append(int(columns[24]))
						node_id_list.append(int(columns[2]))
					i=i+1
				except (IndexError,ValueError):
					gotdata = 'null'
	print (set(frequency_diff));
	#print set(node_id_list)
	for frequency_diff_ in set(frequency_diff):
		print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
		print "SNo\t" "Node:\t""Init_Id\t""R_Self_init\t""R_other1\t""Nt recv\t""Num_Self_init\t""Num_other1\t""No_pkt_recv""\t""Reliability""\t""Damage""\t""Intrusion""\t""Radio-on""\t""Relay-count""\t""Latency"
		print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
		for node_id in set(node_id_list):
			self_initiator=[]
			other_initiator=[]
			no_rcv_cnt=[]
			radio_on_time=[]
			relay_count=[]
			latency=[]
			average_latency=0.0
			average_relay_count=0.0
			average_radio_on_time=0.0
			group_ini=0.0
			f=[]
			count=0
			for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/glossy-group/apps/glossy-freq-rr-test/Data/Flocklab/G_4/0.3722/7_32_18_16'):
				f.extend(filenames)
				for name in f:
					fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/glossy-group/apps/glossy-freq-rr-test/Data/Flocklab/G_4/0.3722/7_32_18_16',name)
					f = open(fullName, 'r')
					#print fullName
					for line in f:
						line = line.strip()
						columns = line.split()
						#print columns
						try:
							if(frequency_diff_ == int(columns[24]) and node_id == int(columns[2])):
								#print columns
								freq_set.append(int(columns[23]))
								#if(frequency_diff_==1):
								#print columns[13]
								if int(columns[13])!=0:							 
        		                                	        self_initiator.append(1);
								elif int(columns[13])==0 and int(columns[14])!=0 and int(columns[15])!=0:
									other_initiator.append(1);
								elif (int(columns[13])==0 and int(columns[14])==0) or  int(columns[15])==0:
									no_rcv_cnt.append(1);
								for j in sorted(group1):
									if(j==int(columns[2])):
										group_ini=group1_initiator
								for j in sorted(group2):
									if(j==int(columns[2])):
										group_ini=group2_initiator
								for j in sorted(group3):
									if(j==int(columns[2])):
										group_ini=group3_initiator
								for j in sorted(group4):
									if(j==int(columns[2])):
										group_ini=group4_initiator
#Calculate relay count and latency only if it recieves packet from its own initiator
								if int(columns[13])!=0:	
									radio_on_time.append(float(columns[21]))
									relay_count.append(float(columns[22]))
								if(float(columns[11])!=0.0 and int(columns[2])!=group_ini):
									latency.append(float(columns[11]))
									if(int(columns[2])==group_ini):
										latency.append(0)
						except (IndexError,ValueError):
							gotdata = 'null'
			
			#print (self_initiator),(other_initiator),(no_rcv_cnt),node_id
			if(len(self_initiator)!=0 or len(other_initiator)!=0 or len(no_rcv_cnt)!=0):
				count=count+1
				total=(np.sum(self_initiator)+np.sum(other_initiator)+np.sum(no_rcv_cnt))
				percent_self_initiator=float(((np.sum(self_initiator))/total)*100)
        			percent_other_initiator=float(((np.sum(other_initiator))/total)*100)
       				percent_initiator_nt_rcv=float(((np.sum(no_rcv_cnt))/total)*100)
				relaibility=percent_self_initiator/100;
				intrusion_factor=percent_other_initiator/total
				damage_factor=percent_initiator_nt_rcv/100;
				reliability_all.append(relaibility)
				damage_all.append(damage_factor)
				intrusion_all.append(intrusion_factor)
				if(len(radio_on_time)!=0):
					average_radio_on_time=np.mean(radio_on_time)
					average_relay_count=np.mean(relay_count)
					radio_on_time_all.append(average_radio_on_time)
					relay_count_all.append(average_relay_count) 
				if(len(latency)!=0):
					average_latency=np.mean(latency)
					average_latency_all.append(average_latency) 
				m=0
				for target in cdf:
					if(relaibility <= target):
						listy[m].append(node_id)
					m=m+1
				if(relaibility >=0.9):
					count_100per_rel.append(node_id)
				else:
					victim_nodes.append(node_id)
							#print latency
				print count,"\t",node_id,"\t",group_ini,"\t",round(percent_self_initiator,2),"\t\t",round(percent_other_initiator,2),"\t\t",round(percent_initiator_nt_rcv,2),"\t",len(self_initiator),"\t\t",len(other_initiator),"\t\t",len(no_rcv_cnt),"\t\t",round(relaibility,4),"\t\t",round(damage_factor,4),"\t",round(intrusion_factor,4),"\t\t",round(average_radio_on_time,3),"\t\t",round(average_relay_count,3),"\t\t",round(average_latency,3)
			self_initiator=[]
			other_initiator=[]
			no_rcv_cnt=[]
			percent_self_initiator=0.0
			percent_other_initiator=0.0
			percent_initiator_nt_rcv=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			average_latency=0.0
			average_relay_count=0.0
			average_radio_on_time=0.0
			group_ini=0.0
		print reliability_all
		avg_relaibility=np.mean(reliability_all)
		avg_damage=np.mean(damage_all)
		avg_intrusion=np.mean(intrusion_all)
		radio_on_time =np.mean(radio_on_time_all)
		latency_final=np.mean(average_latency_all)
		relay_Count_final=np.mean(relay_count_all)
		reliability_all=[]
		damage_all=[]
		intrusion_all=[]
		radio_on_time_all=[]
		average_latency_all=[]
		relay_count_all=[]
		print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
		print frequency_diff_,"\t",set(freq_set)
		print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#print round(relaibility_total,3),round(damage_total,3),round(intrusion_total,3),count
		print "Average------------------------------------------------------------------------------------------------------->\t",round(avg_relaibility,3),"\t\t",round(avg_damage,3),"\t",round(avg_intrusion,3),"\t\t",round(radio_on_time,3),"\t\t",round(relay_Count_final,3),"\t\t",round(latency_final,3)
		print "No of nodes with 100% relaibilty---------------------------------------------------------------------------------------------->\t",len(count_100per_rel)
		print "Non-Victim nodes :",count_100per_rel
		print "No of victim nodes ----------------------------------------------------------------------------------------------------------->\t",len(victim_nodes)
		print "Victim nodes :",victim_nodes
		print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
		print "CDF TABLE "
		print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
		m=0
		for target in cdf:
			print cdf[m],"\t|\t",len(listy[m]),"\t|\t",listy[m]
			m=m+1	
		print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
		listy = [[] for i in range(20)]		
		victim_nodes=[]
		count_100per_rel=[]
		freq_set=[]

def main():

    if len(sys.argv) != 1:
        print('...')
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()

