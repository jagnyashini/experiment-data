import sys
import os
import csv
import math  

from os import walk
from fileinput import close

def analyze():

	node_count=0
	parent_list = []
	node_list_1=[]
	node_list_2=[]
	node_list_3=[]
	#group1=[2,12,7,10,9,29,34,28,30,20,17,26,18,16,51,57,61,53,62,65,46,45,48,47]
	#group2=[11,14,5,1,13,6,15,27,32,37,35,19,22,24,52,60,56,54,55,74,43,42,44,40]
	#group1=[44,38,47,46,48,18,24,19,52,56,51,54,55,30,28,35,37,15,5,13,10,14,1]
	#group2=[65,45,42,40,62,16,17,26,57,60,61,53,49,27,32,34,29,6,2,12,7,11,9]
	#group1=[56,57,60,51,54,61,55,53,62,74,45,38,42,40,6,15,29,5,2,14,11,13,12,7,1,9,10]
	#group2=[17,20,22,18,24,16,52,65,43,44,46,48,27,37,28,35,30,32,34]
	#group1=[44,47,38,45,46,18,22,62,52,57,51,54,53,35,28,32,29,15,5,14,7,11,9]
	#group2=[48,43,42,40,43,65,74,24,16,17,20,60,56,61,55,19,37,30,34,27,10,13,12,1,2]
	#group1=[30,28,34,27,15,13,11,14,1,20,17,22,18,10,51,52,56,55,46,43,42,48,45,40,26]
	#group2=[35,32,29,6,5,2,12,7,9,37,19,16,24,54,60,57,61,53,62,38,65,74,44,47]
	#group1=[60,19,26,20,51,61,1,7,10,6,12,13,2,11,14,5,15,27,29,34,32,28,30,35,37]
	#group2=[57,52,16,17,22,18,24,49,53,55,54,9,46,38,42,48,44,40,47,45,43,74,62,61,56]
	#group1=[40,47,48,44,45,42,46,18,24,26,52,57,49,55,53,61,56,60,22,20]
	#group2=[7,1,9,10,12,13,5,15,29,28,16,17,19,51,54,30,35,37,27]	
	#group1=[26,47,48,44,45,42,46,18,24,40,52,57,49,55,53,61,56,60,22,20]
	#group2=[19,1,9,10,12,13,5,15,29,28,16,17,7,51,54,30,35,37,27]
	#group1=[26,35,37,28,27,29,20,19,17,30,24,18,22,46,42,48,44,45,40,47]
	#group2=[16,55,49,61,54,56,57,52,53,60,51,9,1,7,10,12,13,5,15]	
	#group1=[30,35,37,28,27,29,20,19,17,26,24,18,22,46,42,48,44,45,40,47]
	#group2=[54,55,49,61,16,56,57,52,53,60,51,9,1,7,10,12,13,5,15]	
	
#group1=[30,35,37,28,27,29,20,19,17,26,24,18,22,46,42,48,44,45,40,47]
#	group2=[7,55,49,61,16,56,57,52,53,60,51,9,1,54,10,12,13,5,15]
	#group1=[16,26,20,22,18,48,40,44,60,56,54,55,9,7,12,5,30,37,53]
	#group2=[52,57,49,61,51,10,13,1,15,29,28,35,17,27,19,24,46,42,47,45]	
        #group1=[30,28,29,15,13,5,10,12,7,1,9,54,61,51,55,53,60,56,57,49]
	#group2=[35,37,20,17,19,22,26,16,24,18,46,42,44,45,48,47,40,27,52]
	#group1=[30,28,29,15,13,5,10,12,7,1,9,54,61,51,55,53,60,56,57,49]
	#group2=[40,37,20,17,19,22,26,16,24,18,46,42,44,45,48,47,35,27,52]
	#group1=[40,42,46,22,24,17,20,16,60,51,61,55,37,30,28,15,5,10,1]
	#group2=[47,48,44,45,18,26,19,52,57,56,49,53,54,27,35,29,13,12,7,9]
	#group1=[12,9,1,5,15,27,30,35,22,17,19,52,51,56,54,49,46,48,45,47]
	#group2=[7,10,13,29,28,37,20,26,18,16,60,57,61,55,53,24,42,44,40]
	#group1=[29,28,27,37,35,30,20,17,18,24,46,42,48,44,45,47,40,15,19,22]
	#group2=[10,5,13,12,7,1,9,51,54,61,55,53,49,56,57,60,52,16,26]
	#group1=[29,27,28,37,35,30,20,22,17,19,26,24,18,46,42,48,44,45,47,40]
	#group2=[5,15,13,10,12,7,1,9,51,60,56,54,57,61,55,49,53,52,16]
	#group1=[22,24,16,56,20,57,61,55,53,35,28,29,15,5,7,1,48,44,40]
	#group2=[18,17,52,19,26,60,51,54,49,37,30,27,10,13,12,9,46,42,45,47]
	group1=[28,30,29,35,37,27,15,5,13,12,7,10,1,9,51,61,54,60,56,57]
	group2=[40,47,48,42,44,45,46,18,24,26,16,22,17,19,20,52,49,55,53]


	group1_initiator=group1[0]
	group2_initiator=group2[0]
	count_node_1=0
	count_node_2=0
	count_node_both=0
	group_ini=0
	f = []
	f2 = []
	f3=  []
	f4=[]	
	f5=[]
	f6=[]
	node_hop_cnt = []
	count=0
	ten=0
	not_ten=0
	nine=0
	eight=0
	seven=0
	six=0
	five=0
	four=0
	three=0
	two=0
	count=0

	
	relaibility_total=0.0
	damage_total=0.0
	intrusion_total=0.0
	relaibility_d=0.0
	damage_d=0.0
	intrusion_d=0.0
	relaibility_sd_NC=0.0
	damage_sd_NC=0.0
	intrusion_sd_NC=0.0
	relaibility_sum=0.0
	damage_sum=0.0
	intrusion_sum=0.0	
	count_100per_rel=[]
	victim_nodes=[]
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "With No Change"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	avg_relaibility_NC=0.0
	avg_damage_NC=0.0
	avg_intrusion_NC=0.0	
	#Average calculation
	count=0
	f=[]
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/W_1_28_40'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/W_1_28_40',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:
						if int(columns[13])==group1_initiator:							 
                                                	initiator1=initiator1+1;
						elif int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						#node_list.append(columns[2])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
				total=float(initiator1+initiator2+no_rcv_cnt)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
       		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
				if(group_ini==group1_initiator):
					count=count+1
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2)
				if(group_ini==group2_initiator):
					count=count+1
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1)
				intrusion_factor=intrusion/total
				relaibility_total=relaibility_total+relaibility
				damage_total=damage_total+damage_factor
				intrusion_total=intrusion_total+intrusion_factor
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
	avg_relaibility_NC=relaibility_total/count
	avg_damage_NC=damage_total/count
	avg_intrusion_NC=intrusion_total/count

#Average Calculation ends here
	f = []
	print "SNo\t" "Node:\t""Init_Id\t\t""R_Self_init\t""R_other1\t""Nt recv\t\t""Num_Self_init\t""Num_other1\t""No_pkt_recv""\t""Reliability""\t""Damage""\t\t""Intrusion"

	count=0
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/W_1_28_40'):

		f4.extend(filenames)
		for name in f4:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/W_1_28_40',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
						#print(columns),group1_initiator,group2_initiator 
 						if int(columns[13])==group1_initiator:
                                                	initiator1=initiator1+1;
						if int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						group_ini=0
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						#node_list.append(columns[2])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0  or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
					total=float(initiator1+initiator2+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2)
					intrusion_factor=intrusion/total
					if(group_ini==group1_initiator):
						if(relaibility ==1.0):
							#print relaibility,count_100per_rel;
							count_100per_rel.append(int(columns[2]))
						else:
							victim_nodes.append(int(columns[2]))								
						relaibility_d=(avg_relaibility_NC-relaibility)**2
						damage_d=(avg_damage_NC-damage_factor)**2
						intrusion_d=(avg_intrusion_NC-intrusion_factor)**2
						#print avg_intrusion,intrusion_factor,intrusion_d
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						count=count+1
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator1,"\t\t",initiator2,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3),"\t"#,round(relaibility_sum,3),"\t",round(damage_sum,3),"\t",round(intrusion_sum,3)
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	for (dirpath, dirnames, filenames2) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/W_1_28_40'):
		f3.extend(filenames2)
		for name in f3:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/W_1_28_40',name)
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
						#print(columns)
	 					if int(columns[13])==group1_initiator:
	                                                initiator1=initiator1+1;
						if int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0  or no_rcv_cnt!=0):
					total2=initiator1+initiator2+no_rcv_cnt
					total=float(initiator1+initiator2+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1)
					intrusion_factor=intrusion/total
					if(group_ini==group2_initiator):
						if(relaibility ==1.0):
							#print relaibility,count_100per_rel;
							count_100per_rel.append(int(columns[2]))
						else:
							victim_nodes.append(int(columns[2]))
						count=count+1
						relaibility_d=(avg_relaibility_NC-relaibility)**2
						damage_d=(avg_damage_NC-damage_factor)**2
						intrusion_d=(avg_intrusion_NC-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator2,"\t\t",initiator1,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3)#,"\t",round(relaibility_sum,3),"\t",round(damage_sum,3),"\t",round(intrusion_sum,3)
         		initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	relaibility_sd_NC=math.sqrt(relaibility_sum/(count-1))
	damage_sd_NC=math.sqrt(damage_sum/(count-1))
	intrusion_sd_NC=math.sqrt(intrusion_sum/(count-1))
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#print round(relaibility_total,3),round(damage_total,3),round(intrusion_total,3),count
	print "Average----------------------------------------------------------------------------------------------------------------------->\t",round(avg_relaibility_NC,3),"\t\t",round(avg_damage_NC,3),"\t\t",round(avg_intrusion_NC,3)
	print "Standard Deviation------------------------------------------------------------------------------------------------------------>\t",round(relaibility_sd_NC,3),"\t\t",round(damage_sd_NC,3),"\t\t",round(intrusion_sd_NC,3)
	print "No of nodes with 100% relaibilty---------------------------------------------------------------------------------------------->\t",len(count_100per_rel)
	print "Non-Victim nodes :",count_100per_rel
	print "No of victim nodes ----------------------------------------------------------------------------------------------------------->\t",len(victim_nodes)
	print "Victim nodes :",victim_nodes
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	relaibility_total=0.0
	damage_total=0.0
	intrusion_total=0.0
	relaibility_d=0.0
	damage_d=0.0
	intrusion_d=0.0
	relaibility_sd_C=0.0
	damage_sd_C=0.0
	intrusion_sd_C=0.0
	relaibility_sum=0.0
	damage_sum=0.0
	intrusion_sum=0.0
	count_100per_rel_c=[]
	victim_nodes_c=[]
	extra_nodes_c=[]
	recovered_nodes_c=[]
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "With Channel Change"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#Average calculation
	avg_relaibility_C=0.0
	avg_damage_C=0.0
	avg_intrusion_C=0.0
	count=0
	f=[]
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/C_1_28_40'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/C_1_28_40',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator_nt_rcv=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:	
						#print columns
						if int(columns[14])==group1_initiator:							 
                                                	initiator1=initiator1+1;
						elif int(columns[14])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[14])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						#node_list.append(columns[2])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			#print initiator1,initiator2,no_rcv_cnt
			if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
				total=float(initiator1+initiator2+no_rcv_cnt)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
       		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
				if(group_ini==group1_initiator):
					count=count+1
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2)
					intrusion_factor=intrusion/total
				if(group_ini==group2_initiator):
					count=count+1
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1)
					intrusion_factor=intrusion/total
				relaibility_total=relaibility_total+relaibility
				damage_total=damage_total+damage_factor
				intrusion_total=intrusion_total+intrusion_factor
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
	avg_relaibility_C=relaibility_total/count
	avg_damage_C=damage_total/count
	avg_intrusion_C=intrusion_total/count
	#print relaibility_total,count
#Average Calculation ends here
	f = []
	print "SNo\t" "Node:\t""Init_Id\t\t""R_Self_init\t""R_other1\t""Nt recv\t\t""Num_Self_init\t""Num_other1\t""No_pkt_recv""\t""Reliability""\t""Damage""\t\t""Intrusion""\t""Hopcount"
	count=0

	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/C_1_28_40'):
		f5.extend(filenames)
		for name in f5:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/C_1_28_40',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			avg_hop_count=0
			hop_count=0
			hop_count_entry=0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:
						#print(columns)
						#print(columns),group1_initiator,group2_initiator
						
 						if int(columns[14])==group1_initiator:							 
						#	print(columns)
                                                	initiator1=initiator1+1;
							hop_count=hop_count+int(columns[18])
							hop_count_entry =hop_count_entry+1
						elif int(columns[14])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[14])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						group_ini=0
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
					total=float(initiator1+initiator2+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2)
					intrusion_factor=intrusion/total
					if(group_ini==group1_initiator):
						if(relaibility ==1.0):
							#print relaibility,count_100per_rel;
							count_100per_rel_c.append(int(columns[2]))
							for i in sorted(victim_nodes):
								if (int(columns[2]) == i):
									recovered_nodes_c.append(int(columns[2]))
						else:
							victim_nodes_c.append(int(columns[2]))
							flag_present=0
							for i in sorted(victim_nodes):
								if (int(columns[2]) == i):
									flag_present=1
							if(flag_present==0):
								extra_nodes_c.append(int(columns[2]))
						if hop_count_entry !=0 :
							avg_hop_count= hop_count/hop_count_entry
						relaibility_d=(avg_relaibility_C-relaibility)**2
						damage_d=(avg_damage_C-damage_factor)**2
						intrusion_d=(avg_intrusion_C-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						count=count+1
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator1,"\t\t",initiator2,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3),"\t\t",avg_hop_count #,round(relaibility_sum,3),"\t",round(damage_sum,3),"\t",round(intrusion_sum,3)
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	for (dirpath, dirnames, filenames2) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/C_1_28_40'):
		f6.extend(filenames2)
		for name in f6:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/C_1_28_40',name)
			node_list = []
#			node_neighbor_list = []
			f2 = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			avg_hop_count=0
			hop_count=0
			hop_count_entry=0
			for line in f2:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
				 		#print(columns)
						#print(columns[1],columns[22])
	 					if int(columns[14])==group1_initiator:
	                                                initiator1=initiator1+1;
						if int(columns[14])==group2_initiator:
							initiator2=initiator2+1;
							hop_count=hop_count+int(columns[18])
							hop_count_entry =hop_count_entry+1
						if int(columns[14])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0  or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0  or no_rcv_cnt!=0):
					total2=initiator1+initiator2+no_rcv_cnt
					total=float(initiator1+initiator2+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1)
					intrusion_factor=intrusion/total
					if(group_ini==group2_initiator):
						if(relaibility ==1.0):
							#print relaibility,count_100per_rel;
							count_100per_rel_c.append(int(columns[2]))
							for i in sorted(victim_nodes):
								if (int(columns[2]) == i):
									recovered_nodes_c.append(int(columns[2]))
						else:
							victim_nodes_c.append(int(columns[2]))
							flag_present=0
							for i in sorted(victim_nodes):
								if (int(columns[2]) == i):
									flag_present=1
							if(flag_present==0):
								extra_nodes_c.append(int(columns[2]))
						if hop_count_entry !=0 :
							avg_hop_count= hop_count/hop_count_entry
						count=count+1
						relaibility_d=(avg_relaibility_C-relaibility)**2
						damage_d=(avg_damage_C-damage_factor)**2
						intrusion_d=(avg_intrusion_C-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator2,"\t\t",initiator1,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3),"\t\t",avg_hop_count#,"\t",round(relaibility_sum,3),"\t",round(damage_sum,3),"\t",round(intrusion_sum,3)
         		initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	relaibility_sd_C=math.sqrt(relaibility_sum/(count-1))
	damage_sd_C=math.sqrt(damage_sum/(count-1))
	intrusion_sd_C=math.sqrt(intrusion_sum/(count-1))
	per_recovered_c=(float(len(recovered_nodes_c))/float(len(victim_nodes)))*100
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#print round(relaibility_total,3),round(damage_total,3),round(intrusion_total,3),count
	print "Average----------------------------------------------------------------------------------------------------------------------->\t",round(avg_relaibility_C,3),"\t\t",round(avg_damage_C,3),"\t\t",round(avg_intrusion_C,3)
	print "Standard Deviation------------------------------------------------------------------------------------------------------------>\t",round(relaibility_sd_C,3),"\t\t",round(damage_sd_C,3),"\t\t",round(intrusion_sd_C,3)
	print "No of nodes with 100% relaibilty---------------------------------------------------------------------------------------------->\t",len(count_100per_rel_c)
	print "Non-Victim nodes :",count_100per_rel_c
	print "No of nodes with damage ------------------------------------------------------------------------------------------------------>\t",len(victim_nodes_c)
	print "Victim nodes :",victim_nodes_c
	print "No of nodes recovered ------------------------------------------------------------------------------------------------------>\t",len(recovered_nodes_c)
	print "Recovered nodes :",recovered_nodes_c
	print "No of nodes extra became victim ---------------------------------------------------------------------------------------------->\t",len(extra_nodes_c)
	print "Extra nodes became victim :",extra_nodes_c
	print "Percent recovered with SFD Change :",round(per_recovered_c,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	
		
	f = []
	relaibility_total=0.0
	damage_total=0.0
	intrusion_total=0.0
	relaibility_d=0.0
	damage_d=0.0
	intrusion_d=0.0
	relaibility_sd_S=0.0
	damage_sd_S=0.0
	intrusion_sd_S=0.0
	relaibility_sum=0.0
	damage_sum=0.0
	intrusion_sum=0.0
	avg_relaibility_S=0.0
	avg_damage_S=0.0
	avg_intrusion_S=0.0
	count_dam_intr=0
	count=0
	count_100per_rel_s=[]
	victim_nodes_s=[]
	recovered_nodes_s=[]
	extra_nodes_s=[]
#Average calculation

	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/S_1_28_40'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/S_1_28_40',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:
						#print columns
						if int(columns[13])==group1_initiator:							 
                                                	initiator1=initiator1+1;
						elif int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
				total=float(initiator1+initiator2+no_rcv_cnt)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
       		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
				if(group_ini==group1_initiator):
					count=count+1
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2)
				if(group_ini==group2_initiator):
					count=count+1
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1)
				intrusion_factor=intrusion/total
				relaibility_total=relaibility_total+relaibility
				damage_total=damage_total+damage_factor
				intrusion_total=intrusion_total+intrusion_factor
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
	avg_relaibility_S=relaibility_total/count
	#print relaibility_total,avg_relaibility_S
	avg_damage_S=damage_total/count
	avg_intrusion_S=intrusion_total/count

#Average Calculation ends here
	f = []
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "With SFD Change"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	count=0
	print "SNo\t" "Node:\t""Init_Id\t\t""R_Self_init\t""R_other1\t""Nt recv\t\t""Num_Self_init\t""Num_other1\t""No_pkt_recv""\t""Reliability""\t""Damage""\t\t""Intrusion"

	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/S_1_28_40'):

		f.extend(filenames)
		for name in f:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/S_1_28_40',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			no_rcv_cnt=0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:
						#print(columns)
						
						#print(columns),group1_initiator,group2_initiator
						
 						if int(columns[13])==group1_initiator:							 
							#print((columns[20]))
                                                	initiator1=initiator1+1;
						elif int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						group_ini=0
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			#print(hop_count,hop_count_entry)
			if(initiator1!=0 or initiator2!=0  or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
					total=float(initiator1+initiator2+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2)
					intrusion_factor=intrusion/total
					if(group_ini==group1_initiator):
						if(relaibility ==1.0):
							#print relaibility,count_100per_rel;
							count_100per_rel_s.append(int(columns[2]))
							for i in sorted(victim_nodes):
								if (int(columns[2]) == i):
									recovered_nodes_s.append(int(columns[2]))
						else:
							victim_nodes_s.append(int(columns[2]))
							flag_present=0
							for i in sorted(victim_nodes):
								if (int(columns[2]) == i):
									flag_present=1
							if(flag_present==0):
								extra_nodes_s.append(int(columns[2]))
						relaibility_d=(avg_relaibility_S-relaibility)**2
						damage_d=(avg_damage_S-damage_factor)**2
						intrusion_d=(avg_intrusion_S-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						count=count+1
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator1,"\t\t",initiator2,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3),"\t"#,round(relaibility_sum,3),"\t",round(damage_sum,3),"\t",round(intrusion_sum,3)initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	f2 = []
	for (dirpath, dirnames, filenames2) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/S_1_28_40'):
		f2.extend(filenames2)
		for name in f2:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/S_1_28_40',name)
			node_list = []
#			node_neighbor_list = []
			f2 = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f2:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
						#print(columns)
						#print(columns[1],columns[22])
	 					if int(columns[13])==group1_initiator:
	                                                initiator1=initiator1+1;
						if int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(hop_count,hop_count_entry)
			if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
					total2=initiator1+initiator2+no_rcv_cnt
					total=float(initiator1+initiator2+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1)
					intrusion_factor=intrusion/total
					if(group_ini==group2_initiator):
						if(relaibility ==1.0):
							#print relaibility,count_100per_rel;
							count_100per_rel_s.append(int(columns[2]))
							for i in sorted(victim_nodes):
								if (int(columns[2]) == i):
									recovered_nodes_s.append(int(columns[2]))
						else:
							victim_nodes_s.append(int(columns[2]))
							flag_present=0
							for i in sorted(victim_nodes):
								if (int(columns[2]) == i):
									flag_present=1
							if(flag_present==0):
								extra_nodes_s.append(int(columns[2]))
						#avg_hop_count= hop_count/hop_count_entry
						count=count+1
						relaibility_d=(avg_relaibility_S-relaibility)**2
						damage_d=(avg_damage_S-damage_factor)**2
						intrusion_d=(avg_intrusion_S-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator2,"\t\t",initiator1,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3),"\t"#,"\t",round(relaibility_sum,3),"\t",round(damage_sum,3),"\t",round(intrusion_sum,3)
                	initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	relaibility_sd_S=math.sqrt(relaibility_sum/(count-1))
	damage_sd_S=math.sqrt(damage_sum/(count-1))
	intrusion_sd_S=math.sqrt(intrusion_sum/(count-1))
	per_recovered_s=(float(len(recovered_nodes_s))/float(len(victim_nodes)))*100
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#print round(relaibility_total,3),round(damage_total,3),round(intrusion_total,3),count
	print "Average----------------------------------------------------------------------------------------------------------------------->\t",round(avg_relaibility_S,3),"\t\t",round(avg_damage_S,3),"\t\t",round(avg_intrusion_S,3)
	print "Standard Deviation------------------------------------------------------------------------------------------------------------>\t",round(relaibility_sd_S,3),"\t\t",round(damage_sd_S,3),"\t\t",round(intrusion_sd_S,3)
	print "No of nodes with 100% relaibilty---------------------------------------------------------------------------------------------->\t",len(count_100per_rel_s)
	print "Non-Victim nodes :",count_100per_rel_s
	print "No of nodes with damage ------------------------------------------------------------------------------------------------------>\t",len(victim_nodes_s)
	print "Victim nodes :",victim_nodes_s
	print "No of nodes recovered ------------------------------------------------------------------------------------------------------>\t",len(recovered_nodes_s)
	print "Recovered nodes :",recovered_nodes_s
	print "No of nodes extra became victim ---------------------------------------------------------------------------------------------->\t",len(extra_nodes_s)
	print "Extra nodes became victim :",extra_nodes_s
	print "Percent recovered with SFD Change :",round(per_recovered_s,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	relaibility_total=0.0
	damage_total=0.0
	intrusion_total=0.0
	relaibility_d=0.0
	damage_d=0.0
	intrusion_d=0.0
	relaibility_sd_H=0.0
	damage_sd_H=0.0
	intrusion_sd_H=0.0
	relaibility_sum=0.0
	damage_sum=0.0
	intrusion_sum=0.0
	count_100per_rel_h=[]
	victim_nodes_h=[]
	recovered_nodes_h=[]
	extra_nodes_h=[]
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "With Header Change"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	count=0
	print "SNo\t" "Node:\t""Init_Id\t\t""R_Self_init\t""R_other1\t""Nt recv\t\t""Num_Self_init\t""Num_other1\t""No_pkt_recv""\t""Reliability""\t""Damage""\t\t""Intrusion"
	f=[]
	f5=[]
	f6=[]
#Average calculation

	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-hdr-test/Data/H_1_28_40'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-hdr-test/Data/H_1_28_40',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:
						#print(columns)
						if int(columns[13])==group1_initiator:							 
                                                	initiator1=initiator1+1;
						elif int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
				total=float(initiator1+initiator2+no_rcv_cnt)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
       		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
				if(group_ini==group1_initiator):
					count=count+1
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2)
				if(group_ini==group2_initiator):
					count=count+1
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1)
				intrusion_factor=intrusion/total
				relaibility_total=relaibility_total+relaibility
				damage_total=damage_total+damage_factor
				intrusion_total=intrusion_total+intrusion_factor
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
	avg_relaibility_H=relaibility_total/count
	avg_damage_H=damage_total/count
	avg_intrusion_H=intrusion_total/count

#Average Calculation ends here
	count=0
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-hdr-test/Data/H_1_28_40'):
		f5.extend(filenames)
		for name in f5:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-hdr-test/Data/H_1_28_40',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:
						#print(columns)
						#print(columns),group1_initiator,group2_initiator
						
 						if int(columns[13])==group1_initiator:							 
						#	print(columns)
                                                	initiator1=initiator1+1;
						elif int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						group_ini=0
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0 or  no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
					total=float(initiator1+initiator2+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2)
					intrusion_factor=intrusion/total
					if(group_ini==group1_initiator):
						if(relaibility ==1.0):
							#print relaibility,count_100per_rel;
							count_100per_rel_h.append(int(columns[2]))
							for i in sorted(victim_nodes):
								if (int(columns[2]) == i):
									recovered_nodes_h.append(int(columns[2]))
						else:
							victim_nodes_h.append(int(columns[2]))
							flag_present=0
							for i in sorted(victim_nodes):
								if (int(columns[2]) == i):
									flag_present=1
							if(flag_present==0):
								extra_nodes_h.append(int(columns[2]))
						relaibility_d=(avg_relaibility_H-relaibility)**2
						damage_d=(avg_damage_H-damage_factor)**2
						intrusion_d=(avg_intrusion_H-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						count=count+1
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator1,"\t\t",initiator2,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3),"\t"#,round(relaibility_sum,3),"\t",round(damage_sum,3),"\t",round(intrusion_sum,3)
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	for (dirpath, dirnames, filenames2) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-hdr-test/Data/H_1_28_40'):
		f6.extend(filenames2)
		for name in f6:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-hdr-test/Data/H_1_28_40',name)
			node_list = []
#			node_neighbor_list = []
			f2 = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
			for line in f2:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
				 		#print(columns)
						#print(columns[1],columns[22])
	 					if int(columns[13])==group1_initiator:
	                                                initiator1=initiator1+1;
						if int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						group_ini=0
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						for i in sorted(group1):
							if(i==int(columns[2])):
								group_ini=group1_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0 or  no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
					total2=initiator1+initiator2+no_rcv_cnt
					total=float(initiator1+initiator2+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1)
					intrusion_factor=intrusion/total
					if(group_ini==group2_initiator):
						if(relaibility ==1.0):
							#print relaibility,count_100per_rel;
							count_100per_rel_h.append(int(columns[2]))
							for i in sorted(victim_nodes):
								if (int(columns[2]) == i):
									recovered_nodes_h.append(int(columns[2]))
						else:
							victim_nodes_h.append(int(columns[2]))
							flag_present=0
							for i in sorted(victim_nodes):
								if (int(columns[2]) == i):
									flag_present=1
							if(flag_present==0):
								extra_nodes_h.append(int(columns[2]))
						count=count+1
						relaibility_d=(avg_relaibility_H-relaibility)**2
						damage_d=(avg_damage_H-damage_factor)**2
						intrusion_d=(avg_intrusion_H-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator2,"\t\t",initiator1,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3)#,"\t",round(relaibility_sum,3),"\t",round(damage_sum,3),"\t",round(intrusion_sum,3)
				
                	initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	relaibility_sd_H=math.sqrt(relaibility_sum/(count-1))
	damage_sd_H=math.sqrt(damage_sum/(count-1))
	intrusion_sd_H=math.sqrt(intrusion_sum/(count-1))
	per_recovered_h=(float(len(recovered_nodes_h))/float(len(victim_nodes)))*100
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#print round(relaibility_total,3),round(damage_total,3),round(intrusion_total,3),count
	print "Average----------------------------------------------------------------------------------------------------------------------->\t",round(avg_relaibility_H,3),"\t\t",round(avg_damage_H,3),"\t\t",round(avg_intrusion_H,3)
	print "Standard Deviation------------------------------------------------------------------------------------------------------------>\t",round(relaibility_sd_H,3),"\t\t",round(damage_sd_H,3),"\t\t",round(intrusion_sd_H,3)
	print "No of nodes with 100% relaibilty---------------------------------------------------------------------------------------------->\t",len(count_100per_rel_h)
	print "Non-Victim nodes :",count_100per_rel_h
	print "No of nodes with damage ------------------------------------------------------------------------------------------------------>\t",len(victim_nodes_h)
	print "Victim nodes :",victim_nodes_h
	print "No of nodes recovered ------------------------------------------------------------------------------------------------------>\t",len(recovered_nodes_h)
	print "Recovered nodes :",recovered_nodes_h
	print "No of nodes extra became victim ---------------------------------------------------------------------------------------------->\t",len(extra_nodes_h)
	print "Extra nodes became victim :",extra_nodes_h
	print "Percent recovered with Header Change :",round(per_recovered_h,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	increase_relaibilty_S = (avg_relaibility_S-avg_relaibility_NC)
	increase_damage_S=avg_damage_S-avg_damage_NC
	decrease_intrusion_S=avg_intrusion_NC-avg_intrusion_S
	improvement_relaibilty_S=(increase_relaibilty_S/avg_relaibility_NC)*100
	improvement_intrusion_S=((decrease_intrusion_S)/avg_intrusion_NC)*100
	improvement_damage_S=((increase_damage_S)/avg_damage_NC)*100
	print "SFD Change Improvement Details"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "Relaibility Inceases: ",round(improvement_relaibilty_S,3)
	print "Damage Increases: ",round(improvement_damage_S,3)
	print "Intrusion Decreases: ",round(improvement_intrusion_S,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	increase_relaibilty_C = (avg_relaibility_C-avg_relaibility_NC)
	increase_damage_C=avg_damage_C-avg_damage_NC
	decrease_intrusion_C=avg_intrusion_NC-avg_intrusion_C
	improvement_relaibilty_C=(increase_relaibilty_C/avg_relaibility_NC)*100
	improvement_intrusion_C=((decrease_intrusion_C)/avg_intrusion_NC)*100
	improvement_damage_C=((increase_damage_C)/avg_damage_NC)*100
	print "Channel Change Improvement Details"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "Relaibility Inceases: ",round(improvement_relaibilty_C,3)
	print "Damage Increases: ",round(improvement_damage_C,3)
	print "Intrusion Decreases: ",round(improvement_intrusion_C,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	increase_relaibilty_H = (avg_relaibility_H -avg_relaibility_NC)
	increase_damage_H =avg_damage_H -avg_damage_NC
	decrease_intrusion_H =avg_intrusion_NC-avg_intrusion_H 
	improvement_relaibilty_H =(increase_relaibilty_H/avg_relaibility_NC)*100
	improvement_intrusion_H =((decrease_intrusion_H )/avg_intrusion_NC)*100
	improvement_damage_H =((increase_damage_H )/avg_damage_NC)*100
	print "Header Change Improvement Details"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "Relaibility Inceases: ",round(improvement_relaibilty_H ,3)
	print "Damage Increases: ",round(improvement_damage_H ,3)
	print "Intrusion Decreases: ",round(improvement_intrusion_H ,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "Overall Result: "	
	print "Group Selected as follows:"
	print "Group 1:",group1
	print "Group 2:",group2
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "\tExp_No\t|" "\tType        \t|""\t\t\t\tAverage Value\t\t\t|""\t\t\tImprovement Statistics"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "\t      \t|" "\t            \t|""\tReliability\t|""\tDamage\t|""\tIntrusion\t|"" Vic-Nodes |"" Non-Vic-nodes |"" Rec-nodes |"" Ext-nodes-vic |""% Rec  |""Inc_Rel\t|"" Inc_Dam |""Inc_Intr|"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "\t1_28_40 |" "\tNo change   \t|""\t",round(avg_relaibility_NC,3),"\t\t|""\t",round(avg_damage_NC,3),"\t|""\t",round(avg_intrusion_NC,3),"\t\t|   ",len(victim_nodes),"    |","    ", len(count_100per_rel),"\t    |""\t---- \t|""\t-----\t|------ |""---\t\t|""---      |"" ---    |"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "\t1_28_40 |" "\tSFD change  \t|""\t",round(avg_relaibility_S,3),"\t\t|""\t",round(avg_damage_S,3),"\t|""\t",round(avg_intrusion_S,3),"\t\t|   ",len(victim_nodes_s),"    |","    ", len(count_100per_rel_s),"\t    |""\t",len(recovered_nodes_s),"\t|""\t",len(extra_nodes_s),"\t|",round(per_recovered_s,3),"|",round(improvement_relaibilty_S,2),"\t|",round(improvement_damage_S,2)    ,"|", round(improvement_intrusion_S,2)     ,"|"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "\t1_28_40 |" "\tHeader change\t|""\t",round(avg_relaibility_H,3),"\t\t|""\t",round(avg_damage_H,3),"\t|""\t",round(avg_intrusion_H,3),"\t\t|   ",len(victim_nodes_h),"    |","    ", len(count_100per_rel_h),"\t    |""\t",len(recovered_nodes_h),"\t|""\t",len(extra_nodes_h),"\t|",round(per_recovered_h,3),"|",round(improvement_relaibilty_H,2),"\t|",round(improvement_damage_H,2)    ,"|", round(improvement_intrusion_H,2)     ,"|"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "\t1_28_40 |" "\tChannel change\t|""\t",round(avg_relaibility_C,3),"\t\t|""\t",round(avg_damage_C,3),"\t|""\t",round(avg_intrusion_C,3),"\t\t|   ",len(victim_nodes_c),"    |","    ", len(count_100per_rel_c),"\t    |""\t",len(recovered_nodes_c),"\t|""\t",len(extra_nodes_c),"\t|",round(per_recovered_c,3),"|",round(improvement_relaibilty_C,2),"\t\t|",round(improvement_damage_C,2)    ,"|", round(improvement_intrusion_C,2)     ,"|"
	
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
		
def main():

    if len(sys.argv) != 1:
        print('...')
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()

