import sys
import os
import csv
import math  

from os import walk
from fileinput import close

def analyze():

	node_count=0
	parent_list = []
	node_list_1=[]
	node_list_2=[]
	node_list_3=[]
	#group1=[2,12,7,10,9,29,34,28,30,20,17,26,18,16,51,57,61,53,62,65,46,45,48,47]
	#group2=[11,14,5,1,13,6,15,27,32,37,35,19,22,24,52,60,56,54,55,74,43,42,44,40]
	#group1=[44,38,47,46,48,18,24,19,52,56,51,54,55,30,28,35,37,15,5,13,10,14,1]
	#group2=[65,45,42,40,62,16,17,26,57,60,61,53,49,27,32,34,29,6,2,12,7,11,9]
	#group1=[56,57,60,51,54,61,55,53,62,74,45,38,42,40,6,15,29,5,2,14,11,13,12,7,1,9,10]
	#group2=[17,20,22,18,24,16,52,65,43,44,46,48,27,37,28,35,30,32,34]
	#group1=[44,47,38,45,46,18,22,62,52,57,51,54,53,35,28,32,29,15,5,14,7,11,9]
	#group2=[48,43,42,40,43,65,74,24,16,17,20,60,56,61,55,19,37,30,34,27,10,13,12,1,2]
	#group1=[30,20,22,35,24,42,18,47,40,41,46,45,48,24,23]
	#group2=[28,17,44,27,19,26,52,16,57,56,60,51,58,49]
	#group3=[13,15,5,12,7,9,10,1,54,61,55,53,29]
	#group1=[41,45,46,48,44,42,40,47,18,28,24,35]
	#group2=[19,17,22,26,52,16,23,27,30,29,20,57,15,60]
	#group3=[10,56,61,51,58,55,49,53,13,7,12,5,9,1,54]
	#group1=[47,45,48,18,35,19,29,5,15,12,9,10]
	#group2=[60,40,28,17,27,26,30,20,46,13,7,1,54]
	#group3=[22,41,42,44,24,30,52,23,16,57,56,61,58,55,49,53]
	#group1=[46,45,41,48,44,42,40,47,18,28,24,35]
	#group2=[16,17,22,26,52,19,23,27,30,29,20,57,15,60]
	#group3=[9,56,61,51,58,55,49,53,13,7,12,5,10,1,54]
	#group1=[35,20,22,30,24,42,18,47,40,41,46,45,48,24]
	#group2=[27,17,44,28,19,26,52,16,57,56,60,51,58,49]
	#group3=[15,13,5,12,7,9,10,1,54,61,55,53,29]
	
	#group1=[41,45,46,48,44,42,40,47,18,28,24,35]
	#group2=[19,17,22,26,52,16,23,27,30,29,20,57,15,60]
	#group3=[10,56,61,51,58,55,49,53,13,7,12,5,9,1,54]
	#group1=[46,45,41,48,44,42,40,47,18,28,24,35]
	#group2=[16,17,22,26,52,19,23,27,30,29,20,57,15,60]
	#group3=[9,56,61,51,58,55,49,53,13,7,12,5,10,1,54]
	#group1=[18,45,41,48,44,42,40,47,46,28,24,35]
	#group2=[27,17,22,26,52,19,23,16,30,29,20,57,15,60]
	#group3=[13,56,61,51,58,55,49,53,9,7,12,5,10,1,54]
	#group1=[24,45,41,48,44,42,40,47,18,28,46,35]
	#group2=[26,17,22,16,52,19,23,27,30,29,20,57,15,60]
	#group3=[56,9,61,51,58,55,49,53,13,7,12,5,10,1,54]	
	
	#group1=[48,45,41,46,44,42,40,47,18,28,24,35]
	#group2=[23,17,22,26,52,19,16,27,30,29,20,57,15,60]
	#group3=[5,56,61,51,58,55,49,53,13,7,12,9,10,1,54]
	#group1=[47,45,41,48,44,42,40,18,46,28,24,35]
	#group2=[29,17,22,26,52,19,23,16,30,27,20,57,15,60]
	#group3=[12,56,61,51,58,55,49,53,9,7,13,5,10,1,54]

	#group1=[47,45,41,48,44,42,40,18,46,28,24,35]
	#group2=[29,17,22,26,52,19,23,16,30,27,20,57,15,60]
	#group3=[12,56,61,51,58,55,49,53,9,7,13,5,10,1,54]
	#group1=[28,45,41,48,44,42,40,47,46,18,24,35]
	#group2=[17,27,22,26,52,19,23,16,30,29,20,57,15,60]
	#group3=[51,56,61,13,58,55,49,53,9,7,12,5,10,1,54]
	#group1=[42,45,41,48,44,28,40,47,46,18,24,35]
	#group2=[30,27,22,26,52,19,23,16,17,29,20,57,15,60]
	#group3=[58,56,61,13,51,55,49,53,9,7,12,5,10,1,54]


	#Mixed Case
	#group1=[35,20,22,30,24,42,18,47,40,41,46,45,48,24]
	#group2=[27,17,44,28,19,26,52,16,57,56,60,51,58,49]
	#group3=[15,13,5,12,7,9,10,1,54,61,55,53,29]
	
	#group1=[46,20,22,30,24,42,18,47,40,41,35,45,48,23]
	#group2=[16,17,44,28,19,26,52,27,57,56,60,51,58,49]
	#group3=[9,13,5,12,7,15,10,1,54,61,55,53,29]

	#group1=[23,20,22,30,24,42,18,47,40,41,35,45,48,46]
	#group2=[57,17,44,28,19,26,52,27,16,56,60,51,58,49]
	#group3=[61,13,5,12,7,15,10,1,54,9,55,53,29]
	
	#group1=[41,20,22,30,24,42,18,47,40,23,35,45,48,46]
	#group2=[19,17,44,28,57,26,52,27,16,56,60,51,58,49]
	#group3=[10,13,5,12,7,15,61,1,54,9,55,53,29]
	
	#group1=[45,20,22,30,24,42,18,47,40,23,35,41,48,46]
	#group2=[52,17,44,28,57,26,19,27,16,56,60,51,58,49]
	#group3=[53,13,5,12,7,15,61,1,54,9,55,10,29]

	group1=[20,45,22,30,24,42,18,47,40,23,35,41,48,46]
	group2=[27,17,44,28,57,26,19,52,16,56,60,51,58,49]
	group3=[29,13,5,12,7,15,61,1,54,9,55,10,53]

	group1_initiator=group1[0]
	group2_initiator=group2[0]
	group3_initiator=group3[0]
	count_node_1=0
	count_node_2=0
	count_node_both=0
	group_ini=0
	f = []
	f2 = []
	f3=  []
	f4=[]	
	f5=[]
	f6=[]			
	count=0
	ten=0
	not_ten=0
	nine=0
	eight=0
	seven=0
	six=0
	five=0
	four=0
	three=0
	two=0
	count=0
	relaibility_total=0.0
	damage_total=0.0
	intrusion_total=0.0
	relaibility_d=0.0
	damage_d=0.0
	intrusion_d=0.0
	relaibility_sd_S=0.0
	damage_sd_S=0.0
	intrusion_sd_S=0.0
	relaibility_sum=0.0
	damage_sum=0.0
	intrusion_sum=0.0
	avg_relaibility_S=0.0
	avg_damage_S=0.0
	avg_intrusion_S=0.0
	cdf=[0.999,0.990,0.985,0.980,0.975,0.900,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0.05,0]
	len_cdf=len(cdf)
	listy_s = [[] for i in range(20)]
#Average calculation

	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Downloads/S_57_16_result_1'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Downloads/S_57_16_result_1',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:
						print columns[13]
						if int(columns[13])==group1_initiator:							 
                                                	initiator1=initiator1+1;
						elif int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[13])==group3_initiator:
							initiator3=initiator3+1;
						elif int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						for j in sorted(group3):
							if(j==int(columns[2])):
								group_ini=group3_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
				total=float(initiator1+initiator2+initiator3+no_rcv_cnt)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
				percent_initiator3=float((initiator3/total)*100)
       		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
				if(group_ini==group1_initiator):
					count=count+1
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2+initiator3)
				if(group_ini==group2_initiator):
					count=count+1
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1+initiator3)
				if(group_ini==group3_initiator):
					count=count+1
					relaibility=initiator3/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2+initiator1)
				#print intrusion_factor
				intrusion_factor=intrusion/total
				relaibility_total=relaibility_total+relaibility
				damage_total=damage_total+damage_factor
				intrusion_total=intrusion_total+intrusion_factor
				#print intrusion_total
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
	#print avg_intrusion_S
	avg_relaibility_S=relaibility_total/count
	avg_damage_S=damage_total/count
	avg_intrusion_S=intrusion_total/count
	#print avg_intrusion_S

#Average Calculation ends here
	f = []
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "With SFD Change"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	count=0
	print "SNo\t" "Node:\t""Init_Id\t\t""R_Self_init\t""R_other1\t""R_other2\t""Nt recv\t\t""Num_Self_init\t""Num_other1\t""Num_other2""\t""No_pkt_recv""\t""Reliability""\t""Damage""\t\t""Intrusion"

	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Downloads/S_57_16_result_1'):

		f.extend(filenames)
		for name in f:

			fullName = os.path.join('/home/jagnyashini/Downloads/S_57_16_result_1',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			no_rcv_cnt=0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:
						#print(columns)
						
						#print(columns),group1_initiator,group2_initiator
						
 						if int(columns[13])==group1_initiator:							 
						#	print(columns)
                                                	initiator1=initiator1+1;
						elif int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[13])==group3_initiator:
							initiator3=initiator3+1;
						elif int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						group_ini=0
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						for j in sorted(group3):
							if(j==int(columns[2])):
								group_ini=group3_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
					total=float(initiator1+initiator2+initiator3+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
					percent_initiator3=float((initiator3/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2+initiator3)
					intrusion_factor=intrusion/total
					if(group_ini==group1_initiator):
						relaibility_d=(avg_relaibility_S-relaibility)**2
						damage_d=(avg_damage_S-damage_factor)**2
						intrusion_d=(avg_intrusion_S-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						count=count+1
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy_s[m].append(int(columns[2]))
							m=m+1
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator3,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator1,"\t\t",initiator2,"\t\t",initiator3,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3)
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.	
	f2=[]
	for (dirpath, dirnames, filenames2) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/S_20_27_29_result_1'):
		f2.extend(filenames2)
		for name in f2:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/S_20_27_29_result_1',name)
			node_list = []
#			node_neighbor_list = []
			f2 = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f2:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
				#	print(columns)
						#print(columns[1],columns[22])
	 					if int(columns[13])==group1_initiator:
	                                                initiator1=initiator1+1;
						if int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[13])==group3_initiator:
							initiator3=initiator3+1;
						if int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						for j in sorted(group3):
							if(j==int(columns[2])):
								group_ini=group3_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
					total2=initiator1+initiator2+initiator3+no_rcv_cnt
					total=float(initiator1+initiator2+initiator3+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator3=float((initiator3/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1+initiator3)
					intrusion_factor=intrusion/total
					if(group_ini==group2_initiator):
						count=count+1
						relaibility_d=(avg_relaibility_S-relaibility)**2
						damage_d=(avg_damage_S-damage_factor)**2
						intrusion_d=(avg_intrusion_S-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy_s[m].append(int(columns[2]))
							m=m+1
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator2),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator3,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator2,"\t\t",initiator1,"\t\t",initiator3,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3)
                	initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	f2=[]
	for (dirpath, dirnames, filenames2) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/S_20_27_29_result_1'):
		f2.extend(filenames2)
		for name in f2:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/S_20_27_29_result_1',name)
			node_list = []
#			node_neighbor_list = []
			f2 = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f2:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
				#	print(columns)
						#print(columns[1],columns[22])
	 					if int(columns[13])==group1_initiator:
	                                                initiator1=initiator1+1;
						if int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[13])==group3_initiator:
							initiator3=initiator3+1;
						if int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						for j in sorted(group3):
							if(j==int(columns[2])):
								group_ini=group3_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
					total2=initiator1+initiator2+initiator3+no_rcv_cnt
					total=float(initiator1+initiator2+initiator3+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator3=float((initiator3/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator3/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1+initiator2)
					intrusion_factor=intrusion/total
					if(group_ini==group3_initiator):
						count=count+1
						relaibility_d=(avg_relaibility_S-relaibility)**2
						damage_d=(avg_damage_S-damage_factor)**2
						intrusion_d=(avg_intrusion_S-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy_s[m].append(int(columns[2]))
							m=m+1
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator3,1),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator3,"\t\t",initiator1,"\t\t",initiator2,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3)
                	initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	relaibility_sd_S=math.sqrt(relaibility_sum/(count-1))
	damage_sd_S=math.sqrt(damage_sum/(count-1))
	intrusion_sd_S=math.sqrt(intrusion_sum/(count-1))

	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#print round(relaibility_total,3),round(damage_total,3),round(intrusion_total,3),count
	print "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAverage------->",round(avg_relaibility_S,3),"\t\t",round(avg_damage_S,3),"\t\t",round(avg_intrusion_S,3)
	print "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tStandard Deviation---->",round(relaibility_sd_S,3),"\t\t",round(damage_sd_S,3),"\t\t",round(intrusion_sd_S,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	m=0
	for target in cdf:
		print cdf[m],"\t|\t",len(listy_s[m]),"\t|\t",listy_s[m]
		m=m+1
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
		
	count=0
	relaibility_total=0.0
	damage_total=0.0
	intrusion_total=0.0
	relaibility_d=0.0
	damage_d=0.0
	intrusion_d=0.0
	relaibility_sd_NC=0.0
	damage_sd_NC=0.0
	intrusion_sd_NC=0.0
	relaibility_sum=0.0
	damage_sum=0.0
	intrusion_sum=0.0
	avg_relaibility_NC=0.0
	avg_damage_NC=0.0
	avg_intrusion_NC=0.0
	f=[]
#Average calculation
	listy = [[] for i in range(20)]
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/W_20_27_29_result_1'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/W_20_27_29_result_1',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:
						#print columns
						if int(columns[13])==group1_initiator:							 
                                                	initiator1=initiator1+1;
						elif int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[13])==group3_initiator:
							initiator3=initiator3+1;
						elif int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						for j in sorted(group3):
							if(j==int(columns[2])):
								group_ini=group3_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
				total=float(initiator1+initiator2+initiator3+no_rcv_cnt)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
				percent_initiator3=float((initiator3/total)*100)
       		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
				if(group_ini==group1_initiator):
					count=count+1
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2+initiator3)
				if(group_ini==group2_initiator):
					count=count+1
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1+initiator3)
				if(group_ini==group3_initiator):
					count=count+1
					relaibility=initiator3/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2+initiator1)
				#print intrusion_factor
				intrusion_factor=intrusion/total
				relaibility_total=relaibility_total+relaibility
				damage_total=damage_total+damage_factor
				intrusion_total=intrusion_total+intrusion_factor
				#print intrusion_total
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
	#print avg_intrusion_S
	avg_relaibility_NC=relaibility_total/count
	avg_damage_NC=damage_total/count
	avg_intrusion_NC=intrusion_total/count
	#print avg_intrusion_S

#Average Calculation ends here
	f = []
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "With No Change"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	count=0
	print "SNo\t" "Node:\t""Init_Id\t\t""R_Self_init\t""R_other1\t""R_other2\t""Nt recv\t\t""Num_Self_init\t""Num_other1\t""Num_other2""\t""No_pkt_recv""\t""Reliability""\t""Damage""\t\t""Intrusion"

	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/W_20_27_29_result_1'):

		f.extend(filenames)
		for name in f:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/W_20_27_29_result_1',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			no_rcv_cnt=0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:
						#print(columns)
						
						#print(columns),group1_initiator,group2_initiator
						
 						if int(columns[13])==group1_initiator:							 
						#	print(columns)
                                                	initiator1=initiator1+1;
						elif int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[13])==group3_initiator:
							initiator3=initiator3+1;
						elif int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						group_ini=0
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						for j in sorted(group3):
							if(j==int(columns[2])):
								group_ini=group3_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
					total=float(initiator1+initiator2+initiator3+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
					percent_initiator3=float((initiator3/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2+initiator3)
					intrusion_factor=intrusion/total
					if(group_ini==group1_initiator):
						relaibility_d=(avg_relaibility_NC-relaibility)**2
						damage_d=(avg_damage_NC-damage_factor)**2
						intrusion_d=(avg_intrusion_NC-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						count=count+1
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy[m].append(int(columns[2]))
							m=m+1
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator3,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator1,"\t\t",initiator2,"\t\t",initiator3,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3)
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.	
	f2=[]
	for (dirpath, dirnames, filenames2) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/W_20_27_29_result_1'):
		f2.extend(filenames2)
		for name in f2:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/W_20_27_29_result_1',name)
			node_list = []
#			node_neighbor_list = []
			f2 = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f2:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
				#	print(columns)
						#print(columns[1],columns[22])
	 					if int(columns[13])==group1_initiator:
	                                                initiator1=initiator1+1;
						if int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[13])==group3_initiator:
							initiator3=initiator3+1;
						if int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						for j in sorted(group3):
							if(j==int(columns[2])):
								group_ini=group3_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
					total2=initiator1+initiator2+initiator3+no_rcv_cnt
					total=float(initiator1+initiator2+initiator3+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator3=float((initiator3/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1+initiator3)
					intrusion_factor=intrusion/total
					if(group_ini==group2_initiator):
						count=count+1
						relaibility_d=(avg_relaibility_NC-relaibility)**2
						damage_d=(avg_damage_NC-damage_factor)**2
						intrusion_d=(avg_intrusion_NC-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy[m].append(int(columns[2]))
							m=m+1
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator2),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator3,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator2,"\t\t",initiator1,"\t\t",initiator3,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3)
                	initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	f2=[]
	for (dirpath, dirnames, filenames2) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/W_20_27_29_result_1'):
		f2.extend(filenames2)
		for name in f2:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/W_20_27_29_result_1',name)
			node_list = []
#			node_neighbor_list = []
			f2 = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f2:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
				#	print(columns)
						#print(columns[1],columns[22])
	 					if int(columns[13])==group1_initiator:
	                                                initiator1=initiator1+1;
						if int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[13])==group3_initiator:
							initiator3=initiator3+1;
						if int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						for j in sorted(group3):
							if(j==int(columns[2])):
								group_ini=group3_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
					total2=initiator1+initiator2+initiator3+no_rcv_cnt
					total=float(initiator1+initiator2+initiator3+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator3=float((initiator3/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator3/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1+initiator2)
					intrusion_factor=intrusion/total
					if(group_ini==group3_initiator):
						count=count+1
						relaibility_d=(avg_relaibility_NC-relaibility)**2
						damage_d=(avg_damage_NC-damage_factor)**2
						intrusion_d=(avg_intrusion_NC-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy[m].append(int(columns[2]))
							m=m+1
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator3,1),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator3,"\t\t",initiator1,"\t\t",initiator2,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3)
                	initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	relaibility_sd_NC=math.sqrt(relaibility_sum/(count-1))
	damage_sd_NC=math.sqrt(damage_sum/(count-1))
	intrusion_sd_NC=math.sqrt(intrusion_sum/(count-1))

	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#print round(relaibility_total,3),round(damage_total,3),round(intrusion_total,3),count
	print "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAverage------->",round(avg_relaibility_NC,3),"\t\t",round(avg_damage_NC,3),"\t\t",round(avg_intrusion_NC,3)
	print "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tStandard Deviation---->",round(relaibility_sd_NC,3),"\t\t",round(damage_sd_NC,3),"\t\t",round(intrusion_sd_NC,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	m=0
	for target in cdf:
		print cdf[m],"\t|\t",len(listy[m]),"\t|\t",listy[m]
		m=m+1
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	

	count=0
	relaibility_total=0.0
	damage_total=0.0
	intrusion_total=0.0
	relaibility_d=0.0
	damage_d=0.0
	intrusion_d=0.0
	relaibility_sd_C=0.0
	damage_sd_C=0.0
	intrusion_sd_C=0.0
	relaibility_sum=0.0
	damage_sum=0.0
	intrusion_sum=0.0
	avg_relaibility_C=0.0
	avg_damage_C=0.0
	avg_intrusion_C=0.0
	f=[]
	listy_c = [[] for i in range(20)]
#Average calculation

	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/C_20_27_29_result_1'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/C_20_27_29_result_1',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:
						#print columns
						if int(columns[14])==group1_initiator:							 
                                                	initiator1=initiator1+1;
						elif int(columns[14])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[14])==group3_initiator:
							initiator3=initiator3+1;
						elif int(columns[14])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						for j in sorted(group3):
							if(j==int(columns[2])):
								group_ini=group3_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
				total=float(initiator1+initiator2+initiator3+no_rcv_cnt)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
				percent_initiator3=float((initiator3/total)*100)
       		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
				if(group_ini==group1_initiator):
					count=count+1
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2+initiator3)
				if(group_ini==group2_initiator):
					count=count+1
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1+initiator3)
				if(group_ini==group3_initiator):
					count=count+1
					relaibility=initiator3/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2+initiator1)
				#print relaibility_total
				intrusion_factor=intrusion/total
				relaibility_total=relaibility_total+relaibility
				damage_total=damage_total+damage_factor
				intrusion_total=intrusion_total+intrusion_factor
				#print relaibility_total
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
	#print avg_relaibility_C
	avg_relaibility_C=relaibility_total/count
	avg_damage_C=damage_total/count
	avg_intrusion_C=intrusion_total/count
	#print avg_relaibility_C

#Average Calculation ends here
	f = []
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "With Channel Change"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	count=0
	print "SNo\t" "Node:\t""Init_Id\t\t""R_Self_init\t""R_other1\t""R_other2\t""Nt recv\t\t""Num_Self_init\t""Num_other1\t""Num_other2""\t""No_pkt_recv""\t""Reliability""\t""Damage""\t\t""Intrusion"

	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/C_20_27_29_result_1'):

		f.extend(filenames)
		for name in f:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/C_20_27_29_result_1',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			no_rcv_cnt=0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:
						#print(columns)
						
						#print(columns),group1_initiator,group2_initiator
						
 						if int(columns[14])==group1_initiator:							 
						#	print(columns)
                                                	initiator1=initiator1+1;
						elif int(columns[14])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[14])==group3_initiator:
							initiator3=initiator3+1;
						elif int(columns[14])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						group_ini=0
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						for j in sorted(group3):
							if(j==int(columns[2])):
								group_ini=group3_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
					total=float(initiator1+initiator2+initiator3+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
					percent_initiator3=float((initiator3/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2+initiator3)
					intrusion_factor=intrusion/total
					if(group_ini==group1_initiator):
						relaibility_d=(avg_relaibility_C-relaibility)**2
						damage_d=(avg_damage_C-damage_factor)**2
						intrusion_d=(avg_intrusion_C-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						count=count+1
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy_c[m].append(int(columns[2]))
							m=m+1
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator3,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator1,"\t\t",initiator2,"\t\t",initiator3,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3)
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.	
	f2=[]
	for (dirpath, dirnames, filenames2) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/C_20_27_29_result_1'):
		f2.extend(filenames2)
		for name in f2:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/C_20_27_29_result_1',name)
			node_list = []
#			node_neighbor_list = []
			f2 = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f2:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
				#	print(columns)
						#print(columns[1],columns[22])
	 					if int(columns[14])==group1_initiator:
	                                                initiator1=initiator1+1;
						if int(columns[14])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[14])==group3_initiator:
							initiator3=initiator3+1;
						if int(columns[14])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						for j in sorted(group3):
							if(j==int(columns[2])):
								group_ini=group3_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
					total2=initiator1+initiator2+initiator3+no_rcv_cnt
					total=float(initiator1+initiator2+initiator3+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator3=float((initiator3/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1+initiator3)
					intrusion_factor=intrusion/total
					if(group_ini==group2_initiator):
						count=count+1
						relaibility_d=(avg_relaibility_C-relaibility)**2
						damage_d=(avg_damage_C-damage_factor)**2
						intrusion_d=(avg_intrusion_C-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy_c[m].append(int(columns[2]))
							m=m+1
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator2),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator3,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator2,"\t\t",initiator1,"\t\t",initiator3,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3)
                	initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	f2=[]
	for (dirpath, dirnames, filenames2) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/C_20_27_29_result_1'):
		f2.extend(filenames2)
		for name in f2:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/C_20_27_29_result_1',name)
			node_list = []
#			node_neighbor_list = []
			f2 = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f2:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
				#	print(columns)
						#print(columns[1],columns[22])
	 					if int(columns[14])==group1_initiator:
	                                                initiator1=initiator1+1;
						if int(columns[14])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[14])==group3_initiator:
							initiator3=initiator3+1;
						if int(columns[14])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						for j in sorted(group3):
							if(j==int(columns[2])):
								group_ini=group3_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
					total2=initiator1+initiator2+initiator3+no_rcv_cnt
					total=float(initiator1+initiator2+initiator3+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator3=float((initiator3/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator3/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1+initiator2)
					intrusion_factor=intrusion/total
					if(group_ini==group3_initiator):
						count=count+1
						relaibility_d=(avg_relaibility_C-relaibility)**2
						damage_d=(avg_damage_C-damage_factor)**2
						intrusion_d=(avg_intrusion_C-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy_c[m].append(int(columns[2]))
							m=m+1
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator3,1),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator3,"\t\t",initiator1,"\t\t",initiator2,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3)
                	initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	relaibility_sd_C=math.sqrt(relaibility_sum/(count-1))
	damage_sd_C=math.sqrt(damage_sum/(count-1))
	intrusion_sd_C=math.sqrt(intrusion_sum/(count-1))

	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#print round(relaibility_total,3),round(damage_total,3),round(intrusion_total,3),count
	print "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tAverage------->",round(avg_relaibility_C,3),"\t\t",round(avg_damage_C,3),"\t\t",round(avg_intrusion_C,3)
	print "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tStandard Deviation---->",round(relaibility_sd_C,3),"\t\t",round(damage_sd_C,3),"\t\t",round(intrusion_sd_C,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	m=0
	for target in cdf:
		print cdf[m],"\t|\t",len(listy_c[m]),"\t|\t",listy_c[m]
		m=m+1


	count=0
	relaibility_total=0.0
	damage_total=0.0
	intrusion_total=0.0
	relaibility_d=0.0
	damage_d=0.0
	intrusion_d=0.0
	relaibility_sd_H=0.0
	damage_sd_H=0.0
	intrusion_sd_H=0.0
	relaibility_sum=0.0
	damage_sum=0.0
	intrusion_sum=0.0
	avg_relaibility_H=0.0
	avg_damage_H=0.0
	avg_intrusion_H=0.0
	f=[]
	listy_h = [[] for i in range(20)]
#Average calculation

	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/H_20_27_29_result_1'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/H_20_27_29_result_1',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:
						#print columns
						if int(columns[13])==group1_initiator:							 
                                                	initiator1=initiator1+1;
						elif int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[13])==group3_initiator:
							initiator3=initiator3+1;
						elif int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						for j in sorted(group3):
							if(j==int(columns[2])):
								group_ini=group3_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
				total=float(initiator1+initiator2+initiator3+no_rcv_cnt)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
				percent_initiator3=float((initiator3/total)*100)
       		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
				if(group_ini==group1_initiator):
					count=count+1
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2+initiator3)
				if(group_ini==group2_initiator):
					count=count+1
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1+initiator3)
				if(group_ini==group3_initiator):
					count=count+1
					relaibility=initiator3/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2+initiator1)
				#print relaibility_total
				intrusion_factor=intrusion/total
				relaibility_total=relaibility_total+relaibility
				damage_total=damage_total+damage_factor
				intrusion_total=intrusion_total+intrusion_factor
				#print relaibility_total
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
	#print avg_relaibility_C
	avg_relaibility_H=relaibility_total/count
	avg_damage_H=damage_total/count
	avg_intrusion_H=intrusion_total/count
	#print avg_relaibility_C

#Average Calculation ends here
	f = []
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "With Header Change"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	count=0
	print "SNo\t" "Node:\t""Init_Id\t\t""R_Self_init\t""R_other1\t""R_other2\t""Nt recv\t\t""Num_Self_init\t""Num_other1\t""Num_other2""\t""No_pkt_recv""\t""Reliability""\t""Damage""\t\t""Intrusion"

	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/H_20_27_29_result_1'):

		f.extend(filenames)
		for name in f:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/H_20_27_29_result_1',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			no_rcv_cnt=0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:
						#print(columns)
						
						#print(columns),group1_initiator,group2_initiator
						
 						if int(columns[13])==group1_initiator:							 
						#	print(columns)
                                                	initiator1=initiator1+1;
						elif int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[13])==group3_initiator:
							initiator3=initiator3+1;
						elif int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						group_ini=0
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						for j in sorted(group3):
							if(j==int(columns[2])):
								group_ini=group3_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
					total=float(initiator1+initiator2+initiator3+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
					percent_initiator3=float((initiator3/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2+initiator3)
					intrusion_factor=intrusion/total
					if(group_ini==group1_initiator):
						relaibility_d=(avg_relaibility_H-relaibility)**2
						damage_d=(avg_damage_H-damage_factor)**2
						intrusion_d=(avg_intrusion_H-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						count=count+1
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy_h[m].append(int(columns[2]))
							m=m+1
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator3,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator1,"\t\t",initiator2,"\t\t",initiator3,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3)
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.	
	f2=[]
	for (dirpath, dirnames, filenames2) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/H_20_27_29_result_1'):
		f2.extend(filenames2)
		for name in f2:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/H_20_27_29_result_1',name)
			node_list = []
#			node_neighbor_list = []
			f2 = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f2:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
				#	print(columns)
						#print(columns[1],columns[22])
	 					if int(columns[13])==group1_initiator:
	                                                initiator1=initiator1+1;
						if int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[13])==group3_initiator:
							initiator3=initiator3+1;
						if int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						for j in sorted(group3):
							if(j==int(columns[2])):
								group_ini=group3_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
					total2=initiator1+initiator2+initiator3+no_rcv_cnt
					total=float(initiator1+initiator2+initiator3+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator3=float((initiator3/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1+initiator3)
					intrusion_factor=intrusion/total
					if(group_ini==group2_initiator):
						count=count+1
						relaibility_d=(avg_relaibility_H-relaibility)**2
						damage_d=(avg_damage_H-damage_factor)**2
						intrusion_d=(avg_intrusion_H-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy_h[m].append(int(columns[2]))
							m=m+1
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator2),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator3,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator2,"\t\t",initiator1,"\t\t",initiator3,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3)
                	initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	f2=[]
	for (dirpath, dirnames, filenames2) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/H_20_27_29_result_1'):
		f2.extend(filenames2)
		for name in f2:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Indriya/Hop_count/3_Grp_Clustering/Mixed/20_27_29/H_20_27_29_result_1',name)
			node_list = []
#			node_neighbor_list = []
			f2 = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f2:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
				#	print(columns)
						#print(columns[1],columns[22])
	 					if int(columns[13])==group1_initiator:
	                                                initiator1=initiator1+1;
						if int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[13])==group3_initiator:
							initiator3=initiator3+1;
						if int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						for j in sorted(group3):
							if(j==int(columns[2])):
								group_ini=group3_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or initiator3!=0 or no_rcv_cnt!=0):
					total2=initiator1+initiator2+initiator3+no_rcv_cnt
					total=float(initiator1+initiator2+initiator3+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator3=float((initiator3/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator3/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1+initiator2)
					intrusion_factor=intrusion/total
					if(group_ini==group3_initiator):
						count=count+1
						relaibility_d=(avg_relaibility_H-relaibility)**2
						damage_d=(avg_damage_H-damage_factor)**2
						intrusion_d=(avg_intrusion_H-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy_h[m].append(int(columns[2]))
							m=m+1
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator3,1),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator3,"\t\t",initiator1,"\t\t",initiator2,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3)
                	initiator1=0
			initiator2=0
			initiator3=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			percent_initiator3=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	relaibility_sd_H=math.sqrt(relaibility_sum/(count-1))
	damage_sd_H=math.sqrt(damage_sum/(count-1))
	intrusion_sd_H=math.sqrt(intrusion_sum/(count-1))

	
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#print round(relaibility_total,3),round(damage_total,3),round(intrusion_total,3),count
	print "\t\t\t\t\t\t\t\t\t\t\t\t\t\tAverage------->",round(avg_relaibility_H,3),"\t\t",round(avg_damage_H,3),"\t\t",round(avg_intrusion_H,3)
	print "\t\t\t\t\t\t\t\t\t\t\t\t\tStandard Deviation---->",round(relaibility_sd_H,3),"\t\t",round(damage_sd_H,3),"\t\t",round(intrusion_sd_H,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	m=0
	for target in cdf:
		print cdf[m],"\t|\t",len(listy_h[m]),"\t|\t",listy_h[m]
		m=m+1
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
		
	


	increase_relaibilty_S = (avg_relaibility_S-avg_relaibility_NC)
	increase_damage_S=avg_damage_S-avg_damage_NC
	decrease_intrusion_S=avg_intrusion_NC-avg_intrusion_S
	improvement_relaibilty_S=(increase_relaibilty_S/avg_relaibility_NC)*100
	improvement_intrusion_S=((decrease_intrusion_S)/avg_intrusion_NC)*100
	improvement_damage_S=((increase_damage_S)/avg_damage_NC)*100
	print "SFD Change Improvement Details"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "Relaibility Inceases: ",round(improvement_relaibilty_S,3)
	print "Damage Increases: ",round(improvement_damage_S,3)
	print "Intrusion Decreases: ",round(improvement_intrusion_S,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	increase_relaibilty_C = (avg_relaibility_C-avg_relaibility_NC)
	increase_damage_C=avg_damage_C-avg_damage_NC
	decrease_intrusion_C=avg_intrusion_NC-avg_intrusion_C
	improvement_relaibilty_C=(increase_relaibilty_C/avg_relaibility_NC)*100
	improvement_intrusion_C=((decrease_intrusion_C)/avg_intrusion_NC)*100
	improvement_damage_C=((increase_damage_C)/avg_damage_NC)*100
	print "Channel Change Improvement Details"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "Relaibility Inceases: ",round(improvement_relaibilty_C,3)
	print "Damage Increases: ",round(improvement_damage_C,3)
	print "Intrusion Decreases: ",round(improvement_intrusion_C,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	increase_relaibilty_H = (avg_relaibility_H -avg_relaibility_NC)
	increase_damage_H =avg_damage_H -avg_damage_NC
	decrease_intrusion_H =avg_intrusion_NC-avg_intrusion_H 
	improvement_relaibilty_H =(increase_relaibilty_H/avg_relaibility_NC)*100
	improvement_intrusion_H =((decrease_intrusion_H )/avg_intrusion_NC)*100
	improvement_damage_H =((increase_damage_H )/avg_damage_NC)*100
	print "Header Change Improvement Details"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "Relaibility Inceases: ",round(improvement_relaibilty_H ,3)
	print "Damage Increases: ",round(improvement_damage_H ,3)
	print "Intrusion Decreases: ",round(improvement_intrusion_H ,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	print "--------------------------------------------------------------------------------------------------------"
	print "%","\t|\t","No change","\t|\t","Channel change","\t|\t","SFD change","\t|\t","Header change","\t|\t"
	print "--------------------------------------------------------------------------------------------------------"
	m=0
	for target in cdf:
		print cdf[m],"\t|\t\t",len(listy[m]),"\t|\t\t",len(listy_c[m]),"\t|\t\t",len(listy_s[m]),"\t|\t\t",len(listy_h[m]),"\t|\t"
		m=m+1
	print "--------------------------------------------------------------------------------------------------------"	
		
def main():

    if len(sys.argv) != 1:
        print('...')
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()

