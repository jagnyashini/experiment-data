import sys
import os
import csv

from os import walk
from fileinput import close

def analyze():

	node_count=0
	parent_list = []
	node_list_1=[]
	node_list_2=[]
	node_list_3=[]
	data_count=[]
	data_count_set=[]
	#group1=[2,12,7,10,9,29,34,28,30,20,17,26,18,16,51,57,61,53,62,65,46,45,48,47]
	#group2=[11,14,5,1,13,6,15,27,32,37,35,19,22,24,52,60,56,54,55,74,43,42,44,40]
	#group1=[44,38,47,46,48,18,24,19,52,56,51,54,55,30,28,35,37,15,5,13,10,14,1]
	#group2=[65,45,42,40,62,16,17,26,57,60,61,53,49,27,32,34,29,6,2,12,7,11,9]
	#group1=[56,57,60,51,54,61,55,53,62,74,45,38,42,40,6,15,29,5,2,14,11,13,12,7,1,9,10]
	#group2=[17,20,22,18,24,16,52,65,43,44,46,48,27,37,28,35,30,32,34]
	#group1=[44,47,38,45,46,18,22,62,52,57,51,54,53,35,28,32,29,15,5,14,7,11,9]
	#group2=[48,43,42,40,43,65,74,24,16,17,20,60,56,61,55,19,37,30,34,27,10,13,12,1,2]
	group1=[10,1]
	group2=[9,7]
	group1_initiator=group1[0]
	group2_initiator=group2[0]
	count_node_1=0
	count_node_2=0
	count_node_both=0
	group_ini=0
	total_reliability=0
	total_reliability_count=0
	f = []
	f2 = []
	f3=  []
	f4=[]	
	f5=[]
	f6=[]
	f7=[]
	f8=[]			
	count=0
	ten=0
	not_ten=0
	nine=0
	eight=0
	seven=0
	six=0
	five=0
	four=0
	three=0
	two=0
	i=0
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-mint-test/Data/Indriya/SFD_intrusion/Same_SFD_result_1'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-mint-test/Data/Indriya/SFD_intrusion/Same_SFD_result_1',name)
			#print fullName
#			node_neighbor_list = []
			f = open(fullName, 'r')
			for line in f:
				line = line.strip()
				columns = line.split()
				if(i>7):
					try:
						#print(columns)
						#print(columns[26])
						data_count.append(int(columns[26]))
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1;
			f.close()
			#print len(data_count_set)
	data_count_set = set(data_count)	
	print sorted(data_count_set)
	j=0
	print "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "With SFD Change"
	print "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "SNo\t\t" "Node Id:\t""Initiator_Id\t""Reception_from_Grp_Initiator\t""Reception_from_other\t""No from Self initiator\t""No from other initiator\t\t""No of times not recieved pkt"
	
	for k in sorted(data_count_set):	#print j
		count=0
		initiator1=0
		initiator2=0
		zero=0
		percent_initiator1=0.0
		percent_initiator2=0.0
		data_count_current=0
		print	"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------"	
		sfd=[]
		sfd_set=[]
		for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-mint-test/Data/Indriya/SFD_intrusion/Same_SFD_result_1'):
			f2.extend(filenames)
			for name in f2:
				fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-mint-test/Data/Indriya/SFD_intrusion/Same_SFD_result_1',name)
				f = open(fullName, 'r')
				#print fullName
				i=0
				for line in f:
					line = line.strip()
					#print(line)
					columns = line.split()
					if(i>10):
						try:
							if(int(k)==int(columns[26])):
								#print columns
								sfd.append((columns[23]))
						except (IndexError,ValueError):
								gotdata='null'
					i=i+1	
			sfd_set = set(sfd)
			print sorted(sfd_set)
		print	"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------"	
		for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-mint-test/Data/Indriya/SFD_intrusion/Same_SFD_result_1'):
			f3.extend(filenames)
			#print f3
			self_initiator=0
			other_initiator=0
			no_rcv_cnt=0
			percent_self_initiator=0.0
			percent_other_initiator=0.0
			percent_initiator_nt_rcv=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			count=0
			total=0
			for name in f3:
				fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-mint-test/Data/Indriya/SFD_intrusion/Same_SFD_result_1',name)
				f = open(fullName, 'r')
				#print fullName
				for line in f:
					line = line.strip()
					#print(line)
					columns = line.split()
					try:
						#print columns
						if(int(k)==int(columns[26])):
							total=total+1
							node_id=columns[2]
 							if int(columns[13])!=0:							 
                                        	        	self_initiator=self_initiator+1;
							elif int(columns[13])==0 and int(columns[13])!=0 and int(columns[15])!=0:
								other_initiator=other_initiator+1
							elif (int(columns[13])==0 and int(columns[13])==0) or  int(columns[15])==0:
								no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
					except (IndexError,ValueError):
						abc = 'null'
				if(self_initiator!=0 or other_initiator!=0 or no_rcv_cnt!=0):
					count=count+1
					total=float(self_initiator+other_initiator+no_rcv_cnt)
					percent_self_initiator=float((self_initiator/total)*100)
        		                percent_other_initiator=float((other_initiator/total)*100)
       			                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=self_initiator/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(other_initiator)
					intrusion_factor=intrusion/total
					print count,"\t",node_id,"\t",group_ini,"\t",round(percent_self_initiator,2),"\t\t",round(percent_other_initiator,2),"\t\t",round(percent_initiator_nt_rcv,2),"\t",self_initiator,"\t\t",other_initiator,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,4),"\t\t",round(damage_factor,4),"\t",round(intrusion_factor,4)
					total_reliability=total_reliability+relaibility
					total_reliability_count=total_reliability_count+1	
				self_initiator=0
				other_initiator=0
				no_rcv_cnt=0
				percent_self_initiator=0.0
				percent_other_initiator=0.0
				percent_initiator_nt_rcv=0.0
				relaibility=0.0
				damage_factor=0.0
				intrusion=0.0
				intrusion_factor=0.0
			f3=[]
	avg_reliability=float(total_reliability)/float(total_reliability_count)
	print avg_reliability
def main():

    if len(sys.argv) != 1:
        print('...')
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()

