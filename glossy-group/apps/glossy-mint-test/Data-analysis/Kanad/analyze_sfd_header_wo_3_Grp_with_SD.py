import sys
import os
import csv
import math  

from os import walk
from fileinput import close

def analyze():

	node_count=0
	parent_list = []
	node_list_1=[]
	node_list_2=[]
	node_list_3=[]
	
	group1=[4,1]
	group2=[5,2]
	group=[1,2,5,4]
	group1_initiator=group1[0]
	group2_initiator=group2[0]
	count_node_1=0
	count_node_2=0
	count_node_both=0
	group_ini=0
	f = []
	node_hop_cnt = []
	count=0
	ten=0
	not_ten=0
	nine=0
	eight=0
	seven=0
	six=0
	five=0
	four=0
	three=0
	two=0
	count=0
	n_tx=5
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "With No Change"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	print "SNo\t" "Node:\t""Init_Id\t\t""R_Self_init\t""R_other1\t""Nt recv\t\t""Num_Self_init\t""Num_other1\t""No_pkt_recv""\t""Reliability""\t""Damage""\t\t""Intrusion"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	relaibility_total=0.0
	damage_total=0.0
	intrusion_total=0.0
	relaibility_d=0.0
	damage_d=0.0
	intrusion_d=0.0
	relaibility_sd_NC=0.0
	damage_sd_NC=0.0
	intrusion_sd_NC=0.0
	relaibility_sum=0.0
	damage_sum=0.0
	intrusion_sum=0.0	
	avg_relaibility_cal=0.0
	avg_damage_cal=0.0
	avg_intrusion_cal=0.0	
	#Average calculation
	count=0
	f=[]
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-mint-test/Data/Local/Power_Change_Delay_Experiment/Data/old/Power_31/Current_Results/N_TX_2/S_4_5'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-mint-test/Data/Local/Power_Change_Delay_Experiment/Data/old/Power_31/Current_Results/N_TX_2/S_4_5',name)
			f = open(fullName, 'r')
			rx_cnt=0
			self_initiator=0
			other_initiator=0
			no_rcv_cnt=0
			percent_self_initiator=0.0
			percent_other_initiator=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			recieved_less=0
			recieved_n_tx=0
			radio_on_time=0.0
			average_radio_on_time=0.0
			average_relay_count=0.0
			relay_count=0
			one=0
			two=0
			three=0
			four=0
			five=0
			zero=0
			count=count+1
			number_of_iteration=0
			for line in f:
				line = line.strip()
				columns = line.split()
				try:
					#print(columns)
					if int(columns[12])!=0:							 
                                                self_initiator=self_initiator+1;
						if int(columns[12])==1:
							one=one+1;
						if int(columns[12])==2:
							two=two+1;
						if int(columns[12])==3:
							three=three+1;
						if int(columns[12])==4:
							four=four+1;
						if int(columns[12])==5:
							five=five+1;
					elif int(columns[12])==0 and int(columns[13])!=0:
						other_initiator=other_initiator+1;
						zero=zero+1;	
					elif int(columns[12])==0 and int(columns[13])==0 or int(columns[14])==0:
						no_rcv_cnt=no_rcv_cnt+1;
						zero=zero+1;								
					for j in sorted(group1):
						if(j==int(columns[1])):
							group_ini=group1_initiator
					for j in sorted(group2):
						if(j==int(columns[1])):
							group_ini=group2_initiator
				except (IndexError,ValueError):
					gotdata = 'null'
			if(self_initiator!=0 or other_initiator!=0 or no_rcv_cnt!=0):
				total=float(self_initiator+other_initiator+no_rcv_cnt)
				percent_self_initiator=float((self_initiator/total)*100)
        	                percent_other_initiator=float((other_initiator/total)*100)
       		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
				relaibility=self_initiator/total;
				damage_factor=no_rcv_cnt/total;
				intrusion=float(other_initiator)
				intrusion_factor=intrusion/total
				relaibility_total=relaibility_total+relaibility
				damage_total=damage_total+damage_factor
				intrusion_total=intrusion_total+intrusion_factor
			self_initiator=0
			other_initiator=0
			no_rcv_cnt=0
			percent_self_initiator=0.0
			percent_other_initiator=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			one=0
			two=0
			three=0
			four=0
			five=0
			zero=0
	avg_relaibility_cal=relaibility_total/count
	avg_damage_cal=damage_total/count
	avg_intrusion_cal=intrusion_total/count
	print avg_relaibility_cal,avg_damage_cal,avg_intrusion_cal
	relaibility_total=0.0
	damage_total=0.0
	intrusion_total=0.0
	relaibility_d=0.0
	damage_d=0.0
	intrusion_d=0.0
	relaibility_sd_NC=0.0
	damage_sd_NC=0.0
	intrusion_sd_NC=0.0
	relaibility_sum=0.0
	damage_sum=0.0
	intrusion_sum=0.0	
	count_100per_rel=[]
	victim_nodes=[]
	cdf=[0.999,0.990,0.985,0.980,0.975,0.900,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0.05,0]
	len_cdf=len(cdf)
	listy = [[] for i in range(20)]
	avg_relaibility_NC=0.0
	avg_damage_NC=0.0
	avg_intrusion_NC=0.0	
	#Average calculation
	count=0
	f=[]
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-mint-test/Data/Local/Power_Change_Delay_Experiment/Data/old/Power_31/Current_Results/N_TX_2/S_4_5'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-mint-test/Data/Local/Power_Change_Delay_Experiment/Data/old/Power_31/Current_Results/N_TX_2/S_4_5',name)
			f = open(fullName, 'r')
			rx_cnt=0
			self_initiator=0
			other_initiator=0
			no_rcv_cnt=0
			percent_self_initiator=0.0
			percent_other_initiator=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			recieved_less=0
			recieved_n_tx=0
			radio_on_time=0.0
			average_radio_on_time=0.0
			average_relay_count=0.0
			relay_count=0
			relaibility_sd_NC=0.0
			damage_sd_NC=0.0
			intrusion_sd_NC=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
			one=0
			two=0
			three=0
			four=0
			five=0
			zero=0
			count=count+1
			number_of_iteration=0
			for line in f:
				line = line.strip()
				columns = line.split()
				try:
					#print(columns)
					if int(columns[12])!=0:							 
                                                self_initiator=self_initiator+1;
						if int(columns[12])==1:
							one=one+1;
						if int(columns[12])==2:
							two=two+1;
						if int(columns[12])==3:
							three=three+1;
						if int(columns[12])==4:
							four=four+1;
						if int(columns[12])==5:
							five=five+1;
					elif int(columns[12])==0 and int(columns[13])!=0:
						other_initiator=other_initiator+1;
						zero=zero+1;	
					elif int(columns[12])==0 and int(columns[13])==0 or int(columns[14])==0:
						no_rcv_cnt=no_rcv_cnt+1;
						zero=zero+1;								
					for j in sorted(group1):
						if(j==int(columns[1])):
							group_ini=group1_initiator
					for j in sorted(group2):
						if(j==int(columns[1])):
							group_ini=group2_initiator
				except (IndexError,ValueError):
					gotdata = 'null'
			if(self_initiator!=0 or other_initiator!=0 or no_rcv_cnt!=0):
				total=float(self_initiator+other_initiator+no_rcv_cnt)
				percent_self_initiator=float((self_initiator/total)*100)
        	                percent_other_initiator=float((other_initiator/total)*100)
       		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
				relaibility=self_initiator/total;
				damage_factor=no_rcv_cnt/total;
				intrusion=float(other_initiator)
				intrusion_factor=intrusion/total
				relaibility_total=relaibility_total+relaibility
				damage_total=damage_total+damage_factor
				intrusion_total=intrusion_total+intrusion_factor
				relaibility_d=(avg_relaibility_cal-relaibility)**2
				damage_d=(avg_damage_cal-damage_factor)**2
				intrusion_d=(avg_intrusion_cal-intrusion_factor)**2
				relaibility_sum=relaibility_sum+relaibility_d
				damage_sum=damage_sum+damage_d
				intrusion_sum=intrusion_sum+intrusion_d
			m=0
			for target in cdf:
				if(relaibility >= target):
					listy[m].append(int(columns[1]))
				m=m+1
			if(relaibility >=0.9):
							#print relaibility,count_100per_rel;
				count_100per_rel.append(int(columns[1]))
			else:
				victim_nodes.append(int(columns[1]))
			print count,"\t",columns[1],"\t",group_ini,"\t\t",round(percent_self_initiator,2),"\t\t",round(percent_other_initiator,2),"\t\t",round(percent_initiator_nt_rcv,2),"\t\t",self_initiator,"\t\t",other_initiator,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,4),"\t\t",round(damage_factor,4),"\t\t",round(intrusion_factor,4),"\t\t"
			self_initiator=0
			other_initiator=0
			no_rcv_cnt=0
			percent_self_initiator=0.0
			percent_other_initiator=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			one=0
			two=0
			three=0
			four=0
			five=0
			zero=0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	avg_relaibility_NC=relaibility_total/count
	avg_damage_NC=damage_total/count
	avg_intrusion_NC=intrusion_total/count
	relaibility_sd_H=math.sqrt(relaibility_sum/(count))
	damage_sd_H=math.sqrt(damage_sum/(count))
	intrusion_sd_H=math.sqrt(intrusion_sum/(count))
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#print round(relaibility_total,3),round(damage_total,3),round(intrusion_total,3),count
	print "Average----------------------------------------------------------------------------------------------------------------------->\t",round(avg_relaibility_NC,3),"\t\t",round(avg_damage_NC,3),"\t\t",round(avg_intrusion_NC,3)
	print "Average----------------------------------------------------------------------------------------------------------------------->\t",round(relaibility_sd_H,3),"\t\t",round(damage_sd_H,3),"\t\t",round(intrusion_sd_H,3)
	print "No of nodes with 100% relaibilty---------------------------------------------------------------------------------------------->\t",len(count_100per_rel)
	print "Non-Victim nodes :",count_100per_rel
	print "No of victim nodes ----------------------------------------------------------------------------------------------------------->\t",len(victim_nodes)
	print "Victim nodes :",victim_nodes
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "CDF TABLE-NO CHANGE "
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	m=0
	for target in cdf:
		print cdf[m],"\t|\t",len(listy[m]),"\t|\t",listy[m]
		m=m+1	
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"	
	print "Node:\t""ZERO""\t""ONE\t""TWO\t""THREE\t""FOUR\t""FIVE\t"	
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	
	f=[]
	zero_avg=0.0
	one_avg=0.0
	two_avg=0.0
	three_avg=0.0
	four_avg=0.0
	five_avg=0.0
	cdf_total=0.0
      	cdf_zero=0.0
	cdf_one=0.0
	cdf_two=0.0
	cdf_three=0.0
	cdf_four=0.0
	cdf_five=0.0
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-mint-test/Data/Local/Power_Change_Delay_Experiment/Data/old/Power_31/Current_Results/N_TX_2/S_4_5'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-mint-test/Data/Local/Power_Change_Delay_Experiment/Data/old/Power_31/Current_Results/N_TX_2/S_4_5',name)
			f = open(fullName, 'r')
			rx_cnt=0
			one=0
			two=0
			three=0
			four=0
			five=0
			zero=0
			count=count+1
			for line in f:
				line = line.strip()
				columns = line.split()
				try:
					#print columns
					if int(columns[12])!=0:							 
						if int(columns[12])==1:
							one=one+1;
						if int(columns[12])==2:
							two=two+1;
						if int(columns[12])==3:
							three=three+1;
						if int(columns[12])==4:
							four=four+1;
						if int(columns[12])==5:
							five=five+1;
					elif int(columns[12])==0 and int(columns[13])!=0:
						zero=zero+1;	
					elif int(columns[12])==0 and int(columns[13])==0 or int(columns[14])==0:
						zero=zero+1;
				except (IndexError,ValueError):
					gotdata = 'null'
			print columns[1],"\t",zero,"\t",one,"\t",two,"\t",three,"\t",four,"\t",five
			if int(columns[1]) !=group1_initiator and int(columns[1]) !=group2_initiator:
				zero_avg=zero_avg+zero
				one_avg=one_avg+one
				two_avg=two_avg+two
				three_avg=three_avg+three
				four_avg=four_avg+four
				five_avg=five_avg+five
			self_initiator=0
			other_initiator=0
			no_rcv_cnt=0
			percent_self_initiator=0.0
			percent_other_initiator=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			one=0
			two=0
			three=0
			four=0
			five=0
			zero=0
		cdf_total=zero_avg+one_avg+two_avg+three_avg+four_avg+five_avg
      	        #cdf_zero=(zero_avg+one_avg+two_avg+three_avg+four_avg+five_avg)/(cdf_total)
		#cdf_one=(one_avg+two_avg+three_avg+four_avg+five_avg)/(cdf_total)
		#cdf_two=(two_avg+three_avg+four_avg+five_avg)/(cdf_total)
	        #cdf_three=(three_avg+four_avg+five_avg)/(cdf_total)
	        #cdf_four=(four_avg+five_avg)/(cdf_total)
	        #cdf_five=(five_avg)/(cdf_total)
		cdf_zero=(zero_avg)/(cdf_total)
		cdf_one=(zero_avg+one_avg)/(cdf_total)
		cdf_two=(zero_avg+one_avg+two_avg)/(cdf_total)
	        cdf_three=(zero_avg+one_avg+two_avg+three_avg)/(cdf_total)
	        cdf_four=(zero_avg+one_avg+two_avg+three_avg+four_avg)/(cdf_total)
	        cdf_five=(zero_avg+one_avg+two_avg+three_avg+four_avg+five_avg)/(cdf_total)
		#print zero_avg,"\t",one_avg,"\t",two_avg,"\t",three_avg,"\t",four_avg,"\t",five_avg
		print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
                print round(cdf_zero,3),"\n",round(cdf_one,3),"\n",round(cdf_two,3),"\n",round(cdf_three,3),"\n",round(cdf_four,3),"\n",round(cdf_five,3)
def main():

    if len(sys.argv) != 1:
        print('...')
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()

