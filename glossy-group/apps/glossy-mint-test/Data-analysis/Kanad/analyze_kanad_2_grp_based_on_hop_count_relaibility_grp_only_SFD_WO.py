import sys
import os
import csv
import math  

from os import walk
from fileinput import close

def analyze():

	node_count=0
	parent_list = []
	node_list_1=[]
	node_list_2=[]
	node_list_3=[]
	
	group1=[2,5,6]
	group2=[3,1,4]
	
	group=[1,2,3,4,5,6]
	group1_initiator=group1[0]
	group2_initiator=group2[0]
	count_node_1=0
	count_node_2=0
	count_node_both=0
	group_ini=0
	f = []
	f2 = []
	f3=  []
	f4=[]	
	f5=[]
	f6=[]
	node_hop_cnt = []
	count=0
	ten=0
	not_ten=0
	nine=0
	eight=0
	seven=0
	six=0
	five=0
	four=0
	three=0
	two=0
	count=0

	
	relaibility_total=0.0
	damage_total=0.0
	intrusion_total=0.0
	relaibility_d=0.0
	damage_d=0.0
	intrusion_d=0.0
	relaibility_sd_NC=0.0
	damage_sd_NC=0.0
	intrusion_sd_NC=0.0
	relaibility_sum=0.0
	damage_sum=0.0
	intrusion_sum=0.0	
	count_100per_rel=[]
	victim_nodes=[]
	cdf=[0.999,0.990,0.985,0.980,0.975,0.900,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0.05,0]
	len_cdf=len(cdf)
	listy = [[] for i in range(20)]
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "With No Change"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	avg_relaibility_NC=0.0
	avg_damage_NC=0.0
	avg_intrusion_NC=0.0	
	#Average calculation
	count=0
	f=[]
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/W_2_3'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/W_2_3',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
						channel = int(columns[18])
						if int(columns[12])==group1_initiator:							 
                                                	initiator1=initiator1+1;
						elif int(columns[12])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[12])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[1])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[1])):
								group_ini=group2_initiator
						#node_list.append(columns[2])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
				total=float(initiator1+initiator2+no_rcv_cnt)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
       		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
				if(group_ini==group1_initiator):
					count=count+1
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2)
				if(group_ini==group2_initiator):
					count=count+1
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1)
				intrusion_factor=intrusion/total
				relaibility_total=relaibility_total+relaibility
				damage_total=damage_total+damage_factor
				intrusion_total=intrusion_total+intrusion_factor
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
	avg_relaibility_NC=relaibility_total/count
	avg_damage_NC=damage_total/count
	avg_intrusion_NC=intrusion_total/count

#Average Calculation ends here
	f = []
	print "SNo\t" "Node:\t""Init_Id\t\t""R_Self_init\t""R_other1\t""Nt recv\t\t""Num_Self_init\t""Num_other1\t""No_pkt_recv""\t""Reliability""\t""Damage""\t\t""Intrusion"

	count=0
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/W_2_3'):

		f4.extend(filenames)
		for name in f4:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/W_2_3',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
						#print(columns),group1_initiator,group2_initiator 
 						if int(columns[12])==group1_initiator:
                                                	initiator1=initiator1+1;
						if int(columns[12])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[12])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						group_ini=0
						for j in sorted(group1):
							if(j==int(columns[1])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[1])):
								group_ini=group2_initiator
						#node_list.append(columns[2])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0  or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
					total=float(initiator1+initiator2+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2)
					intrusion_factor=intrusion/total
					if(group_ini==group1_initiator):
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy[m].append(int(columns[1]))
							m=m+1
						if(relaibility >=0.9):
							#print relaibility,count_100per_rel;
							count_100per_rel.append(int(columns[1]))
						else:
							victim_nodes.append(int(columns[1]))
						relaibility_d=(avg_relaibility_NC-relaibility)**2
						damage_d=(avg_damage_NC-damage_factor)**2
						intrusion_d=(avg_intrusion_NC-intrusion_factor)**2
						#print avg_intrusion,intrusion_factor,intrusion_d
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						count=count+1
						print count,"\t",columns[1],"\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator1,"\t\t",initiator2,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3),"\t"#,round(relaibility_sum,3),"\t",round(damage_sum,3),"\t",round(intrusion_sum,3)
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	for (dirpath, dirnames, filenames2) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/W_2_3'):
		f3.extend(filenames2)
		for name in f3:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/W_2_3',name)
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
						#print(columns)
	 					if int(columns[12])==group1_initiator:
	                                                initiator1=initiator1+1;
						if int(columns[12])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[12])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[1])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[1])):
								group_ini=group2_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0  or no_rcv_cnt!=0):
					total2=initiator1+initiator2+no_rcv_cnt
					total=float(initiator1+initiator2+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1)
					intrusion_factor=intrusion/total
					if(group_ini==group2_initiator):
						m=0
						for target in cdf:
							if(relaibility >= target):
								#print columns[2];
								listy[m].append(int(columns[1]))
							m=m+1
						if(relaibility >=0.9):
							#print relaibility,count_100per_rel;
							count_100per_rel.append(int(columns[1]))
						else:
							victim_nodes.append(int(columns[1]))
						count=count+1
						relaibility_d=(avg_relaibility_NC-relaibility)**2
						damage_d=(avg_damage_NC-damage_factor)**2
						intrusion_d=(avg_intrusion_NC-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						print count,"\t",columns[1],"\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator2,"\t\t",initiator1,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3)#,"\t",round(relaibility_sum,3),"\t",round(damage_sum,3),"\t",round(intrusion_sum,3)
         		initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	relaibility_sd_NC=math.sqrt(relaibility_sum/(count-1))
	damage_sd_NC=math.sqrt(damage_sum/(count-1))
	intrusion_sd_NC=math.sqrt(intrusion_sum/(count-1))
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#print round(relaibility_total,3),round(damage_total,3),round(intrusion_total,3),count
	print "Average----------------------------------------------------------------------------------------------------------------------->\t",round(avg_relaibility_NC,3),"\t\t",round(avg_damage_NC,3),"\t\t",round(avg_intrusion_NC,3)
	print "Standard Deviation------------------------------------------------------------------------------------------------------------>\t",round(relaibility_sd_NC,3),"\t\t",round(damage_sd_NC,3),"\t\t",round(intrusion_sd_NC,3)
	print "No of nodes with 100% relaibilty---------------------------------------------------------------------------------------------->\t",len(count_100per_rel)
	print "Non-Victim nodes :",count_100per_rel
	print "No of victim nodes ----------------------------------------------------------------------------------------------------------->\t",len(victim_nodes)
	print "Victim nodes :",victim_nodes
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "CDF TABLE-NO CHANGE "
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	m=0
	for target in cdf:
		print cdf[m],"\t|\t",len(listy[m]),"\t|\t",listy[m]
		m=m+1
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

 	
	f = []
	relaibility_total=0.0
	damage_total=0.0
	intrusion_total=0.0
	relaibility_d=0.0
	damage_d=0.0
	intrusion_d=0.0
	relaibility_sd_S=0.0
	damage_sd_S=0.0
	intrusion_sd_S=0.0
	relaibility_sum=0.0
	damage_sum=0.0
	intrusion_sum=0.0
	avg_relaibility_S=0.0
	avg_damage_S=0.0
	avg_intrusion_S=0.0
	count_dam_intr=0
	count=0
	count_100per_rel_s=[]
	victim_nodes_s=[]
	recovered_nodes_s=[]
	extra_nodes_s=[]
	listy_s = [[] for i in range(20)]
#Average calculation

	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/S_2_3'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/S_2_3',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:
						#print columns
						if int(columns[12])==group1_initiator:							 
                                                	initiator1=initiator1+1;
						elif int(columns[12])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[12])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[1])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[1])):
								group_ini=group2_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
				total=float(initiator1+initiator2+no_rcv_cnt)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
       		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
				if(group_ini==group1_initiator):
					count=count+1
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2)
				if(group_ini==group2_initiator):
					count=count+1
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1)
				intrusion_factor=intrusion/total
				relaibility_total=relaibility_total+relaibility
				damage_total=damage_total+damage_factor
				intrusion_total=intrusion_total+intrusion_factor
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
	avg_relaibility_S=relaibility_total/count
	#print relaibility_total,avg_relaibility_S
	avg_damage_S=damage_total/count
	avg_intrusion_S=intrusion_total/count

#Average Calculation ends here
	f = []
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "With SFD Change"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	count=0
	print "SNo\t" "Node:\t""Init_Id\t\t""R_Self_init\t""R_other1\t""Nt recv\t\t""Num_Self_init\t""Num_other1\t""No_pkt_recv""\t""Reliability""\t""Damage""\t\t""Intrusion"

	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/S_2_3'):

		f.extend(filenames)
		for name in f:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/S_2_3',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			no_rcv_cnt=0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:
						#print(columns)
						
						#print(columns),group1_initiator,group2_initiator
						
 						if int(columns[12])==group1_initiator:							 
							#print((columns[20]))
                                                	initiator1=initiator1+1;
						elif int(columns[12])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[12])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						group_ini=0
						for j in sorted(group1):
							if(j==int(columns[1])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[1])):
								group_ini=group2_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			#print(hop_count,hop_count_entry)
			if(initiator1!=0 or initiator2!=0  or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
					total=float(initiator1+initiator2+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2)
					intrusion_factor=intrusion/total
					if(group_ini==group1_initiator):
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy_s[m].append(int(columns[1]))
							m=m+1
						if(relaibility >=0.9):
							#print relaibility,count_100per_rel;
							count_100per_rel_s.append(int(columns[1]))
							for i in sorted(victim_nodes):
								if (int(columns[1]) == i):
									recovered_nodes_s.append(int(columns[1]))
						else:
							victim_nodes_s.append(int(columns[1]))
							flag_present=0
							for i in sorted(victim_nodes):
								if (int(columns[1]) == i):
									flag_present=1
							if(flag_present==0):
								extra_nodes_s.append(int(columns[1]))
						relaibility_d=(avg_relaibility_S-relaibility)**2
						damage_d=(avg_damage_S-damage_factor)**2
						intrusion_d=(avg_intrusion_S-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						count=count+1
						print count,"\t",columns[1],"\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator1,"\t\t",initiator2,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3),"\t"#,round(relaibility_sum,3),"\t",round(damage_sum,3),"\t",round(intrusion_sum,3)initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	f2 = []
	for (dirpath, dirnames, filenames2) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/S_2_3'):
		f2.extend(filenames2)
		for name in f2:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/S_2_3',name)
			node_list = []
#			node_neighbor_list = []
			f2 = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f2:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
						#print(columns)
						#print(columns[1],columns[22])
	 					if int(columns[12])==group1_initiator:
	                                                initiator1=initiator1+1;
						if int(columns[12])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[12])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[1])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[1])):
								group_ini=group2_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(hop_count,hop_count_entry)
			if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
					total2=initiator1+initiator2+no_rcv_cnt
					total=float(initiator1+initiator2+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1)
					intrusion_factor=intrusion/total
					if(group_ini==group2_initiator):
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy_s[m].append(int(columns[1]))
							m=m+1
						if(relaibility >=0.9):
							#print relaibility,count_100per_rel;
							count_100per_rel_s.append(int(columns[1]))
							for i in sorted(victim_nodes):
								if (int(columns[1]) == i):
									recovered_nodes_s.append(int(columns[1]))
						else:
							#print columns		
							victim_nodes_s.append(int(columns[1]))	
							flag_present=0
							for i in sorted(victim_nodes):
								if (int(columns[1]) == i):
									flag_present=1
							if(flag_present==0):
								extra_nodes_s.append(int(columns[1]))
						#avg_hop_count= hop_count/hop_count_entry
						count=count+1
						relaibility_d=(avg_relaibility_S-relaibility)**2
						damage_d=(avg_damage_S-damage_factor)**2
						intrusion_d=(avg_intrusion_S-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						print count,"\t",columns[1],"\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator2,"\t\t",initiator1,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3),"\t"#,"\t",round(relaibility_sum,3),"\t",round(damage_sum,3),"\t",round(intrusion_sum,3)
                	initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	relaibility_sd_S=math.sqrt(relaibility_sum/(count-1))
	damage_sd_S=math.sqrt(damage_sum/(count-1))
	intrusion_sd_S=math.sqrt(intrusion_sum/(count-1))
	per_recovered_s=(float(len(recovered_nodes_s))/float(len(victim_nodes)))*100
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#print round(relaibility_total,3),round(damage_total,3),round(intrusion_total,3),count
	print "Average----------------------------------------------------------------------------------------------------------------------->\t",round(avg_relaibility_S,3),"\t\t",round(avg_damage_S,3),"\t\t",round(avg_intrusion_S,3)
	print "Standard Deviation------------------------------------------------------------------------------------------------------------>\t",round(relaibility_sd_S,3),"\t\t",round(damage_sd_S,3),"\t\t",round(intrusion_sd_S,3)
	print "No of nodes with 100% relaibilty---------------------------------------------------------------------------------------------->\t",len(count_100per_rel_s)
	print "Non-Victim nodes :",count_100per_rel_s
	print "No of nodes with damage ------------------------------------------------------------------------------------------------------>\t",len(victim_nodes_s)
	print "Victim nodes :",victim_nodes_s
	print "No of nodes recovered ------------------------------------------------------------------------------------------------------>\t",len(recovered_nodes_s)
	print "Recovered nodes :",recovered_nodes_s
	print "No of nodes extra became victim ---------------------------------------------------------------------------------------------->\t",len(extra_nodes_s)
	print "Extra nodes became victim :",extra_nodes_s
	print "Percent recovered with SFD Change :",round(per_recovered_s,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	m=0
	for target in cdf:
		print cdf[m],"\t|\t",len(listy_s[m]),"\t|\t",listy_s[m]
		m=m+1
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"



	increase_relaibilty_S = (avg_relaibility_S-avg_relaibility_NC)
	increase_damage_S=avg_damage_S-avg_damage_NC
	decrease_intrusion_S=avg_intrusion_NC-avg_intrusion_S
	improvement_relaibilty_S=(increase_relaibilty_S/avg_relaibility_NC)*100
	improvement_intrusion_S=((decrease_intrusion_S)/avg_intrusion_NC)*100
	improvement_damage_S=((increase_damage_S)/avg_damage_NC)*100
	print "SFD Change Improvement Details"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "Relaibility Inceases: ",round(improvement_relaibilty_S,3)
	print "Damage Increases: ",round(improvement_damage_S,3)
	print "Intrusion Decreases: ",round(improvement_intrusion_S,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "Overall Result: "	
	print "Group Selected as follows:"
	print "Group 1:",group1
	print "Group 2:",group2
	
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	print "Channel :: ", channel
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	print "\tExp_No\t|" "\tType        \t|""\t\t\t\tAverage Value\t\t\t|""\t\t\tImprovement Statistics"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "\t      \t|" "\t            \t|""\tReliability\t|""\tDamage\t|""\tIntrusion\t|"" Vic-Nodes |"" Non-Vic-nodes |"" Rec-nodes |"" Ext-nodes-vic |""% Rec  |""Inc_Rel\t|"" Inc_Dam |""Inc_Intr|"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "\tW_2_3 |" "\tNo change   \t|""\t",round(avg_relaibility_NC,3),"\t\t|""\t",round(avg_damage_NC,3),"\t|""\t",round(avg_intrusion_NC,3),"\t\t|   ",len(victim_nodes),"    |","    ", len(count_100per_rel),"\t    |""\t---- \t|""\t-----\t|------ |""---\t\t|""---      |"" ---    |"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "\tW_2_3 |" "\tSFD change  \t|""\t",round(avg_relaibility_S,3),"\t\t|""\t",round(avg_damage_S,3),"\t|""\t",round(avg_intrusion_S,3),"\t\t|   ",len(victim_nodes_s),"    |","    ", len(count_100per_rel_s),"\t    |""\t",len(recovered_nodes_s),"\t|""\t",len(extra_nodes_s),"\t|",round(per_recovered_s,3),"|",round(improvement_relaibilty_S,2),"\t|",round(improvement_damage_S,2)    ,"|", round(improvement_intrusion_S,2)     ,"|"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	


	print "---------------------------------------------------------"
	print "%","\t|\t","No change","\t|\t","SFD change","\t|\t"
	print "---------------------------------------------------------"
	m=0
	for target in cdf:
		print cdf[m],"\t|\t\t",len(listy[m]),"\t|\t\t",len(listy_s[m]),"\t|\t"
		m=m+1
	print "---------------------------------------------------------"	
	print sorted(group)

def main():

    if len(sys.argv) != 1:
        print('...')
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()

