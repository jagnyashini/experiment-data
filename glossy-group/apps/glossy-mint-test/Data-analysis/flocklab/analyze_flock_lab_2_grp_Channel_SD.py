import sys
import os
import csv
import math  
from os import walk
from fileinput import close

def analyze():

	node_count=0
	parent_list = []
	#group1=[11,13,17,24,20,19,18,6,22,3,26,27]
	#group2=[2,1,8,15,4,33,32,16,31,10,23,28,25]
	#group1=[24,13,17,11,20,19,18,6,22,25,28,27]
	#group2=[23,1,8,2,4,33,32,16,31,10,15,26,3]
	#group1=[24,13,17,11,20,19,18,6,22,3,26,27]
	#group2=[10,1,8,15,4,33,32,16,31,2,23,28,25]
	#group1=[24,13,17,11,20,19,18,6,22,25,28,27]
	#group2=[15,1,8,2,4,33,32,16,31,10,23,26,3]
	#group1=[13,11,17,24,20,19,18,6,22,10,26,27]
	#group2=[8,1,2,15,4,33,32,16,31,3,23,28,25]
	#group1=[22,11,17,24,20,19,18,6,13,10,26,27]
	#group2=[28,1,2,15,4,33,32,16,31,3,23,8,25]
	#group1=[28,13,17,11,20,19,18,24,22,25,6,27]
	#group2=[31,1,8,2,4,33,32,16,23,10,15,26,3]
	#group1=[6,13,17,11,20,19,18,24,22,3,26,27]
	#group2=[16,1,8,15,4,33,32,10,31,2,23,28,25]
	#group1=[13,6,17,11,20,19,18,24,22,3,26,27]
	#group2=[25,1,8,15,4,33,32,10,31,2,23,28,16]
	#group1=[6,11,17,24,20,19,18,22,13,10,26,27]
	#group2=[3,1,2,15,4,33,32,16,31,28,23,8,25]
	#group1=[24,6,17,11,20,19,18,13,22,3,26,27]
	#group2=[2,1,8,15,4,33,32,10,31,25,23,28,16]
	#group4=[10,26]
	#group5=[24,23]
	#group6=[18,27]
	#group7=[22,28]
	#group8=[16,6]
	#group9=[31,32]
	#group10=[3,33]
	#group11=[15,8]
	#group12=[2,1,4]
	node_set =['4','1','2','8','15','33','3','31','32','10','6','16','20','26','19','25','11','17','23','24','27','18','28','22','13']
	#group1=[27,16,28,24,22,23,6,18,10]
	#group2=[25,11,14,7,13,20,26,19,17]	
	#group3=[31,2,4,1,15,33,32,3,8]		
	#group1=[25,26,10,11,13,23,32,31]
	#group2=[6,2,1,15,3,33,8,4]	
	#group3=[20,24,19,17,27,28,18,16,22]	
	#group1=[22,6,18,28,20,23,31,32,10]
	#group2=[11,26,25,19,13,17,24,27]	
	#group3=[33,1,2,15,03,4,8,16]	
	#group1=[31,6,18,22,20,23,28,32,10]
	#group2=[17,26,25,11,13,19,24,27]	
	#group3=[8,1,2,15,03,33,4,16]
	#group1=[11,13,17,25,19,24,10,26]
	#group2=[31,20,23,27,32,15,3,6,28]	
	#group3=[4,1,2,8,33,16,22,18]


#	group1=[23,15,6,8,10,11,17,18]
#	group2=[28,26,25,20,24,16,3,4,2]	
#	group3=[19,13,27,22,33,31,32,1]

	group1=[6,15,8,23,10,11,17,18]
	group2=[24,26,25,20,28,16,3,4,2]	
	group3=[31,19,27,22,33,13,32,1]
	group2_initiator=group2[0]
	group1_initiator=group1[0]
	group3_initiator=group3[0]

	#group4_initiator=group4[0]
	#group5_initiator=group5[0]
	#group6_initiator=group6[0]
	#group7_initiator=group7[0]
	#group8_initiator=group8[0]
	#group9_initiator=group9[0]
	#group10_initiator=group10[0]
	#group11_initiator=group11[0]
	#group12_initiator=group12[0]
#Calculate the average value
	f = []
	avg_relaibility_cal=0.0
	avg_damage_cal=0.0
	avg_intrusion_cal=0.0
	radio_on_time_cal = 0.0
	latency_final_cal=0.0
	relay_Count_final_cal=0.0
	count=0
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-mint-test/Data/Flocklab/10thNov/Mixed/3_Groups/6_24_31/C_6_24_31'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-mint-test/Data/Flocklab/10thNov/Mixed/3_Groups/6_24_31/C_6_24_31',name)
			f = open(fullName, 'r')
			relaibility_total=0.0
			damage_total=0.0
			intrusion_total=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
			relaibility_sd_NC=0.0
			damage_sd_NC=0.0
			intrusion_sd_NC=0.0
			relaibility_sum=0.0
			damage_sum=0.0
			intrusion_sum=0.0	
			relay_count_total=0.0
			count_radio_on=0.0
			count_latency=0.0
			average_latency_total=0.0
			average_radio_on_time_total=0.0
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				self_initiator=0
				other_initiator=0
				no_rcv_cnt=0
				percent_self_initiator=0.0
				percent_other_initiator=0.0
				percent_initiator_nt_rcv=0.0
				relaibility=0.0
				damage_factor=0.0
				intrusion=0.0
				intrusion_factor=0.0
				average_radio_on_time=0.0
				average_relay_count=0.0
				average_latency=0.0
				relay_count=0
				one=0
				two=0
				three=0
				four=0
				five=0
				zero=0
				number_of_iteration=0
				latency_iteration=0
				radio_on_time=0.0
				latency=0.0
				for line in f:
					line = line.strip()
					columns = line.split()
					
					try:
						if (columns[2]) == i:
							node_id=int(columns[2])
							#print columns[2]
					 		if int(columns[14])!=0:							 
                                        	 	        self_initiator=self_initiator+1;
							elif int(columns[14])==0 and int(columns[15])!=0 and int(columns[16])!=0:
								other_initiator=other_initiator+1;
							elif (int(columns[14])==0 and int(columns[15])==0) or  int(columns[16])==0:
								no_rcv_cnt=no_rcv_cnt+1;
							for j in sorted(group1):
								if(j==int(columns[2])):
									group_ini=group1_initiator
							for j in sorted(group2):
								if(j==int(columns[2])):
									group_ini=group2_initiator 
							for j in sorted(group3):
								if(j==int(columns[2])):
									group_ini=group3_initiator
							"""
							for j in sorted(group4):
								if(j==int(columns[2])):
									group_ini=group4_initiator
							for j in sorted(group5):
								if(j==int(columns[2])):
									group_ini=group5_initiator
							for j in sorted(group6):
								if(j==int(columns[2])):
									group_ini=group6_initiator
							for j in sorted(group7):
								if(j==int(columns[2])):
									group_ini=group7_initiator
							for j in sorted(group8):
								if(j==int(columns[2])):
									group_ini=group8_initiator
							for j in sorted(group9):
								if(j==int(columns[2])):
									group_ini=group9_initiator
							for j in sorted(group10):
								if(j==int(columns[2])):
									group_ini=group10_initiator
							for j in sorted(group11):
								if(j==int(columns[2])):
									group_ini=group11_initiator
							for j in sorted(group12):
								if(j==int(columns[2])):
									group_ini=group12_initiator"""
#Calculate relay count and latency only if it recieves packet from its own initiator
							if int(columns[14])!=0:	
								radio_on_time = radio_on_time + float(columns[22])
								relay_count=relay_count+float(columns[23])
								number_of_iteration=number_of_iteration+1
							if(float(columns[12])!=0.0):
								latency=latency+float(columns[12])
								latency_iteration=latency_iteration+1
							if(int(columns[2])==group_ini):
								latency=0
					except (IndexError,ValueError):
							gotdata = 'null'
				if(self_initiator!=0 or other_initiator!=0 or no_rcv_cnt!=0):
					count=count+1
					total=float(self_initiator+other_initiator+no_rcv_cnt)
					percent_self_initiator=float((self_initiator/total)*100)
        	               	 	percent_other_initiator=float((other_initiator/total)*100)
       		                	percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=self_initiator/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(other_initiator)
					intrusion_factor=intrusion/total
					relaibility_total=relaibility_total+relaibility
					damage_total=damage_total+damage_factor
					intrusion_total=intrusion_total+intrusion_factor
					if(number_of_iteration!=0):
						#print average_radio_on_time_total
						average_radio_on_time=radio_on_time/number_of_iteration
						average_relay_count=relay_count/number_of_iteration
						average_radio_on_time_total = average_radio_on_time_total+average_radio_on_time
						relay_count_total = relay_count_total+average_relay_count
						count_radio_on=count_radio_on+1
					if(latency_iteration!=0):
						average_latency=latency/latency_iteration
						average_latency_total = average_latency_total+average_latency
						count_latency=count_latency+1
				self_initiator=0
				other_initiator=0
				no_rcv_cnt=0
				percent_self_initiator=0.0
				percent_other_initiator=0.0
				percent_initiator_nt_rcv=0.0
				relaibility=0.0
				damage_factor=0.0
				intrusion=0.0
				intrusion_factor=0.0
				number_of_iteration=0
				latency_iteration=0
		avg_relaibility_cal=relaibility_total/count
		avg_damage_cal=damage_total/count
		avg_intrusion_cal=intrusion_total/count
		radio_on_time_cal = average_radio_on_time_total/count_radio_on
		latency_final_cal=average_latency_total/count_latency
		relay_Count_final_cal=relay_count_total/count_radio_on
		print avg_relaibility_cal,avg_damage_cal,avg_intrusion_cal,radio_on_time_cal,latency_final_cal,relay_Count_final_cal
	count=0
	f = []	
	count_100per_rel=[]
	victim_nodes=[]
	cdf=[0.999,0.990,0.985,0.980,0.975,0.900,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0.05,0]
	len_cdf=len(cdf)
	listy = [[] for i in range(30)]
	print "With Channel change"		
	print"------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "SNo\t" "Node:\t""Init_Id\t""R_Self_init\t""R_other1\t""Nt recv\t""Num_Self_init\t""Num_other1\t""No_pkt_recv""\t""Reliability""\t""Damage""\t""Intrusion""\t""Radio-on""\t""Relay-count""\t""Latency"
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-mint-test/Data/Flocklab/10thNov/Mixed/3_Groups/6_24_31/C_6_24_31'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-mint-test/Data/Flocklab/10thNov/Mixed/3_Groups/6_24_31/C_6_24_31',name)
			f = open(fullName, 'r')
			relaibility_total=0.0
			damage_total=0.0
			intrusion_total=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
			relaibility_sd_NC=0.0
			damage_sd_NC=0.0
			intrusion_sd_NC=0.0
			latency_d_sum=0.0
			relay_d_sum=0.0
			radio_on_d_sum=0.0
			relaibility_sum=0.0
			damage_sum=0.0
			intrusion_sum=0.0	
			relay_count_total=0.0
			count_radio_on=0.0
			count_latency=0.0
			average_latency_total=0.0
			average_radio_on_time_total=0.0
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				self_initiator=0
				other_initiator=0
				no_rcv_cnt=0
				percent_self_initiator=0.0
				percent_other_initiator=0.0
				percent_initiator_nt_rcv=0.0
				relaibility=0.0
				damage_factor=0.0
				intrusion=0.0
				intrusion_factor=0.0
				average_radio_on_time=0.0
				average_relay_count=0.0
				average_latency=0.0
				relay_count=0
				one=0
				two=0
				three=0
				four=0
				five=0
				zero=0
				number_of_iteration=0
				latency_iteration=0
				radio_on_time=0.0
				latency=0.0
				relaibility_d=0.0
				damage_d=0.0
				intrusion_d=0.0
				radio_on_d=0.0
				relay_d=0.0
				latency_d=0.0
				for line in f:
					line = line.strip()
					columns = line.split()
					
					try:
						if (columns[2]) == i:
							node_id=int(columns[2])
							#print columns[2]
					 		if int(columns[14])!=0:							 
                                        	 	        self_initiator=self_initiator+1;
								if int(columns[14])==1:
									one=one+1;
								if int(columns[14])==2:
									two=two+1;
								if int(columns[14])==3:
									three=three+1;
								if int(columns[14])==4:
									four=four+1;
								if int(columns[14])==5:
									five=five+1;
							elif int(columns[14])==0 and int(columns[15])!=0 and int(columns[16])!=0:
								other_initiator=other_initiator+1;
								#print columns
								zero=zero+1;	
							elif (int(columns[14])==0 and int(columns[15])==0) or  int(columns[16])==0:
								no_rcv_cnt=no_rcv_cnt+1;
								zero=zero+1;	
							for j in sorted(group1):
								if(j==int(columns[2])):
									group_ini=group1_initiator
							for j in sorted(group2):
								if(j==int(columns[2])):
									group_ini=group2_initiator 
							for j in sorted(group3):
								if(j==int(columns[2])):
									group_ini=group3_initiator
							"""
							for j in sorted(group4):
								if(j==int(columns[2])):
									group_ini=group4_initiator
							for j in sorted(group5):
								if(j==int(columns[2])):
									group_ini=group5_initiator
							for j in sorted(group6):
								if(j==int(columns[2])):
									group_ini=group6_initiator
							for j in sorted(group7):
								if(j==int(columns[2])):
									group_ini=group7_initiator
							for j in sorted(group8):
								if(j==int(columns[2])):
									group_ini=group8_initiator
							for j in sorted(group9):
								if(j==int(columns[2])):
									group_ini=group9_initiator
							for j in sorted(group10):
								if(j==int(columns[2])):
									group_ini=group10_initiator
							for j in sorted(group11):
								if(j==int(columns[2])):
									group_ini=group11_initiator
							for j in sorted(group12):
								if(j==int(columns[2])):
									group_ini=group12_initiator"""
#Calculate relay count and latency only if it recieves packet from its own initiator
							if int(columns[14])!=0:	
								radio_on_time = radio_on_time + float(columns[22])
								relay_count=relay_count+float(columns[23])
								number_of_iteration=number_of_iteration+1
							if(float(columns[12])!=0.0):
								latency=latency+float(columns[12])
								latency_iteration=latency_iteration+1
							if(int(columns[2])==group_ini):
								latency=0
					except (IndexError,ValueError):
							gotdata = 'null'
				if(self_initiator!=0 or other_initiator!=0 or no_rcv_cnt!=0):
					count=count+1
					total=float(self_initiator+other_initiator+no_rcv_cnt)
					percent_self_initiator=float((self_initiator/total)*100)
        	               	 	percent_other_initiator=float((other_initiator/total)*100)
       		                	percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=self_initiator/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(other_initiator)
					intrusion_factor=intrusion/total
					relaibility_total=relaibility_total+relaibility
					damage_total=damage_total+damage_factor
					intrusion_total=intrusion_total+intrusion_factor
					relaibility_d=(avg_relaibility_cal-relaibility)**2
					damage_d=(avg_damage_cal-damage_factor)**2
					intrusion_d=(avg_intrusion_cal-intrusion_factor)**2
					relaibility_sum=relaibility_sum+relaibility_d
					damage_sum=damage_sum+damage_d
					intrusion_sum=intrusion_sum+intrusion_d
					if(number_of_iteration!=0):
						#print average_radio_on_time_total
						average_radio_on_time=radio_on_time/number_of_iteration
						average_radio_on_time_total = average_radio_on_time_total+average_radio_on_time
						#for standard deviation calculation
						radio_on_d=(radio_on_time_cal-average_radio_on_time)**2
						radio_on_d_sum=radio_on_d_sum+radio_on_d
						#replay count start
						average_relay_count=relay_count/number_of_iteration
						relay_count_total = relay_count_total+average_relay_count
						#for standard_deviation calculation
						relay_d=(relay_Count_final_cal-average_relay_count)**2
						relay_d_sum=relay_d_sum+relay_d
						count_radio_on=count_radio_on+1
					if(latency_iteration!=0):
						average_latency=latency/latency_iteration
						average_latency_total = average_latency_total+average_latency
						latency_d=(latency_final_cal-average_latency)**2
						latency_d_sum=latency_d_sum+latency_d
						count_latency=count_latency+1
					m=0
					for target in cdf:
						if(relaibility >= target):
							listy[m].append(node_id)
						m=m+1
					if(relaibility >=0.9):
						count_100per_rel.append(node_id)
					else:
						victim_nodes.append(node_id)
					print count,"\t",node_id,"\t",group_ini,"\t",round(percent_self_initiator,2),"\t\t",round(percent_other_initiator,2),"\t\t",round(percent_initiator_nt_rcv,2),"\t",self_initiator,"\t\t",other_initiator,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,4),"\t\t",round(damage_factor,4),"\t",round(intrusion_factor,4),"\t\t",round(average_radio_on_time,3),"\t\t",round(average_relay_count,3),"\t\t",round(average_latency,3)
				self_initiator=0
				other_initiator=0
				no_rcv_cnt=0
				percent_self_initiator=0.0
				percent_other_initiator=0.0
				percent_initiator_nt_rcv=0.0
				relaibility=0.0
				damage_factor=0.0
				intrusion=0.0
				intrusion_factor=0.0
				number_of_iteration=0
				latency_iteration=0
				one=0
				two=0
				three=0
				four=0
				five=0
				zero=0
		avg_relaibility_NC=relaibility_total/count
		avg_damage_NC=damage_total/count
		avg_intrusion_NC=intrusion_total/count
		radio_on_time = average_radio_on_time_total/count_radio_on
		latency_final=average_latency_total/count_latency
		relay_Count_final=relay_count_total/count_radio_on
		relaibility_sd_H=math.sqrt(relaibility_sum/(count))
		damage_sd_H=math.sqrt(damage_sum/(count))
		intrusion_sd_H=math.sqrt(intrusion_sum/(count))
		radio_on_sd_H=math.sqrt(radio_on_d_sum/(count_radio_on))
		latency_sd_H=math.sqrt(latency_d_sum/(count_latency))
		relay_sd_H=math.sqrt(relay_d_sum/(count_radio_on))


		print average_latency_total,average_radio_on_time_total
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#print round(relaibility_total,3),round(damage_total,3),round(intrusion_total,3),count
	print "Average------------------------------------------------------------------------------------------------------->\t",round(avg_relaibility_NC,2),"\t\t",round(avg_damage_NC,2),"\t",round(avg_intrusion_NC,2),"\t\t",round(radio_on_time,2),"\t\t",round(relay_Count_final,2),"\t\t",round(latency_final,2)
	print "Standard Deviation-------------------------------------------------------------------------------------------->",round(relaibility_sd_H,2),"\t\t",round(damage_sd_H,2),"\t",round(intrusion_sd_H,2),"\t\t",round(radio_on_sd_H,2),"\t\t",round(relay_sd_H,2),"\t\t",round(latency_sd_H,2)
	print "No of nodes with 100% relaibilty---------------------------------------------------------------------------------------------->\t",len(count_100per_rel)
	print "Non-Victim nodes :",count_100per_rel
	print "No of victim nodes ----------------------------------------------------------------------------------------------------------->\t",len(victim_nodes)
	print "Victim nodes :",victim_nodes
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "CDF TABLE-NO CHANGE "
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	m=0
	for target in cdf:
		print cdf[m],"\t|\t",len(listy[m]),"\t|\t",listy[m]
		m=m+1	
	

def main():

    if len(sys.argv) != 1:
        print '...'
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()
	
