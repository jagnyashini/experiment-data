import sys
import os
import csv
import math  
from os import walk
from fileinput import close

def analyze():

	node_count=0
	parent_list = []
	
	#group1=[2,1,15,32,28,22,16,27,10,26,19,11,17,7]
	#group2=[4,8,33,3,31,6,18,24,23,20,25,14,13]
	#node_set = ['2','1','15','32','28','22','16','27','10','26','19','11','17','7','4','8','33','3','31','6','18','24','23','20','25','14','13']

#	group1=[20,17,25,11,7,24,27,18,22,6,33,4,8]
#	group2=[19,26,13,14,23,10,28,16,3,15,2,1]
#	node_set = ['20','17','25','11','7','24','27','18','22','6','33','4','8',19,'26','13','14','23','10','28','16','3','15','2','1']
	
	#group1=[17,25,20,14,7,27,23,22,28,16,33,8,2]
	#group2=[13,19,26,11,24,10,18,31,32,6,3,15,4,1]
	#node_set = ['17','25','20','14','7','27','23','22','28','16','33','8','2','13','19','26','11','24','10','18','31','32','6','3','15','4','1']

	#group1=[6,22,31,33,8,2,1,27,23,26,19,25,14,11]
	#group2=[16,28,3,32,15,4,18,24,10,20,17,13,7]
	#node_set = ['6','22','31','33','8','2','1','27','23','26','19','25','14','11','16','28','3','32','15','4','18','24','10','20','17','13','7']

	#group1=[17,25,14,7,10,24,18,28,6,32,33,4,1]
	#group2=[26,13,11,23,27,31,22,16,3,15,2,8]
	#node_set = ['17','25','14','7','10','24','18','28','6','32','33','4','1','26','13','11','23','27','31','22','16','3','15','2','8']


#	group2=[11,14,13,19,20,10,27,28,6,16,33,15,2]
#	group1=[7,25,17,26,23,24,18,31,22,3,8,4,1]
#	node_set = ['11','14','13','19','20','10','27','28','6','16','33','15','2','7','25','17','26','23','24','18','31','22','3','8','4','1']

	#group1=[2,1,3,4,6,8,15,18,22,27,28,31,32,33]
	#group2=[7,11,10,13,14,17,19,20,23,24,25,26]
	#group1=[8,2,4,1,15,33,32,3,31]
	#group2=[18,16,27,24,22,28,6,23,10]
	#group3=[7,11,14,26,25,20,13,19,17]
	#group1=[31,2,4,1,15,33,32,3,8]
	#group2=[23,16,27,24,22,28,6,18,10]
	#group3=[26,11,14,7,25,20,13,19,17]
	#group1=[4,1,2,8,15,33,3,31,32,10,6,16]
	#group2=[20,26,19,25,13,11,17,23,24,27,18,28,22]
	#group1=[2,1,4,8,15,33,32,3,31,6,28,22,16]
	#group2=[11,25,13,17,19,20,10,23,24,27,26,18]
	#group1=[3,6,31,4,8,33,16,22,26,20,24,18,13]
	#group2=[23,10,32,15,1,2,28,25,19,27,11,17]
	#group1=[31,1,4,8,15,33,32,3,2,6,28,22,16,18,26]
	#group2=[10,11,25,13,17,19,20,23,24,27]
	group1=[2,1,10,8,15,33,3,31,32,4,6,16]
	group2=[13,20,19,25,26,11,17,23,24,27,18,28,22]
	node_set =['4','1','2','8','15','33','3','31','32','10','6','16','20','26','19','25','13','11','17','23','24','27','18','28','22']
	group2_initiator=group2[0]
	group1_initiator=group1[0]
	initiator2=group2[0]
	initiator1=group1[0]
	cdf=[1,0.999,0.990,0.985,0.980,0.975,0.900,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0.05,0]
	len_cdf=len(cdf)
	listy_c = [[] for i in range(20)]	

	f = []	

	print "With Channel change"		
	print"------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "SNo\t" "Node:\t""Init_Id\t\t""R_Self_init\t""R_other1\t""Nt recv\t\t""Num_Self_init\t""Num_other1\t""No_pkt_recv""\t""Reliability""\t""Damage""\t\t""Intrusion""\t""Hopcount"
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Flocklab/Hop_count/2_Group_clustering/C_1_2_13'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Flocklab/Hop_count/2_Group_clustering/C_1_2_13',name)
			f = open(fullName, 'r')
			total2=0
			count=0
			relaibility_total=0.0
			damage_total=0.0
			intrusion_total=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
			relaibility_sd_S=0.0
			damage_sd_S=0.0
			intrusion_sd_S=0.0
			relaibility_sum=0.0
			damage_sum=0.0
			intrusion_sum=0.0
			avg_relaibility_C=0.0
			avg_damage_C=0.0
			avg_intrusion_C=0.0
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				count_initiator1=0
				count_initiator2=0
				count_nt_rcv=0
				percent_initiator1=0.0
				percent_initiator2=0.0
				flag=0
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
					 	if (columns[2]) == i:
							#print("here")
							flag=flag+1
							#print(columns[1],columns[8])
							if(int(columns[14])==initiator1):
								count_initiator1=count_initiator1+1
							if(int(columns[14])==initiator2):
								count_initiator2=count_initiator2+1
							if(int(columns[14])==0):
								count_nt_rcv=count_nt_rcv+1
							group_ini=0
							for j in sorted(group1):
								if(j==int(i)):
									group_ini=group1_initiator
							for k in sorted(group2):
								if(k==int(i)):
									group_ini=group2_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				if(count_initiator1!=0 or count_initiator2!=0 or count_nt_rcv!=0):
					total=float(count_initiator1+count_initiator2+count_nt_rcv)
					percent_initiator1=float((count_initiator1/total)*100)
        			        percent_initiator2=float((count_initiator2/total)*100)
					percent_nt_rcv=float((count_nt_rcv/total)*100)
					if(group_ini==group1_initiator):
						count=count+1
						relaibility=count_initiator1/total;
						damage_factor=count_nt_rcv/total;
						intrusion=float(count_initiator2)
						intrusion_factor=intrusion/total
						relaibility_total=relaibility_total+relaibility
						damage_total=damage_total+damage_factor
						intrusion_total=intrusion_total+intrusion_factor
					if(group_ini==group2_initiator):
						count=count+1
						relaibility=count_initiator2/total;
						damage_factor=count_nt_rcv/total;
						intrusion=float(count_initiator1)
						intrusion_factor=intrusion/total
						relaibility_total=relaibility_total+relaibility
						damage_total=damage_total+damage_factor
						intrusion_total=intrusion_total+intrusion_factor
			count_initiator1=0
			count_initiator2=0
			count_nt_rcv=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
		#print relaibility_total,damage_total,intrusion_total,count
		avg_relaibility_C=relaibility_total/count
		avg_damage_C=damage_total/count
		avg_intrusion_C=intrusion_total/count
		#print avg_relaibility_W,avg_damage_W,avg_intrusion_W,count

	
		relaibility_sd_S=0.0
		damage_sd_S=0.0
		intrusion_sd_S=0.0
		f = []
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Flocklab/Hop_count/2_Group_clustering/C_1_2_13',name)
			f = open(fullName, 'r')
			count=0
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				count_initiator1=0
				count_initiator2=0
				count_nt_rcv=0
				percent_initiator1=0.0
				percent_initiator2=0.0
				relaibility=0.0
				damage_factor=0.0
				intrusion=0.0
				intrusion_factor=0.0
				relaibility_d=0.0
				damage_d=0.0
				intrusion_d=0.0
				relaibility_sum=0.0
				damage_sum=0.0
				intrusion_sum=0.0
				flag=0
				avg_hop_count=0
				hop_count=0
				hop_count_entry=0
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
					 	if (columns[2]) == i:
							#print("here")
							flag=flag+1
							#print(columns[1],columns[8])
							if(int(columns[14])==initiator1):
								count_initiator1=count_initiator1+1
								hop_count=hop_count+int(columns[18])
								hop_count_entry =hop_count_entry+1
							if(int(columns[14])==initiator2):
								count_initiator2=count_initiator2+1
							if(int(columns[14])==0):
								count_nt_rcv=count_nt_rcv+1
							group_ini=0
							for j in sorted(group1):
								if(j==int(i)):
									group_ini=group1_initiator
							for k in sorted(group2):
								if(k==int(i)):
									group_ini=group2_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				if(count_initiator1!=0 or count_initiator2!=0 or count_nt_rcv!=0):
					total=float(count_initiator1+count_initiator2+count_nt_rcv)
					percent_initiator1=float((count_initiator1/total)*100)
        			   	percent_initiator2=float((count_initiator2/total)*100)
					percent_nt_rcv=float((count_nt_rcv/total)*100)	
					if(group_ini==group1_initiator and flag>0):
						#avg_hop_count= hop_count/hop_count_entry
						relaibility=count_initiator1/total;
						damage_factor=count_nt_rcv/total;
						intrusion=float(count_initiator2)
						intrusion_factor=intrusion/total
						count=count+1
						m=0	
						for target in cdf:
							if(relaibility >= target):
								listy_c[m].append(i)
							m=m+1
						relaibility_d=(avg_relaibility_C-relaibility)**2
						damage_d=(avg_damage_C-damage_factor)**2
						intrusion_d=(avg_intrusion_C-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						print count,"\t",i,"\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_nt_rcv,1),"\t\t",count_initiator1,"\t\t",count_initiator2,"\t\t",count_nt_rcv,"\t\t",round(relaibility,4),"\t\t",round(damage_factor,4),"\t\t",round(intrusion_factor,4),"\t\t",avg_hop_count
					if (group_ini==group1_initiator and flag==0):
						print count,"\t",i,"\t",group_ini,"\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------"
		
		f = []
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Flocklab/Hop_count/2_Group_clustering/C_1_2_13',name)
			f = open(fullName, 'r')
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				count_initiator1=0
				count_initiator2=0
				count_nt_rcv=0
				percent_initiator1=0.0
				percent_initiator2=0.0
				relaibility=0.0
				damage_factor=0.0
				intrusion=0.0
				intrusion_factor=0.0
				relaibility_d=0.0
				damage_d=0.0
				intrusion_d=0.0
				flag=0
				avg_hop_count=0
				hop_count=0
				hop_count_entry=0
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
					 	if (columns[2]) == i:
							#print("here")
							flag=flag+1
							#print(columns[1],columns[8])
							if(int(columns[14])==initiator1):
								count_initiator1=count_initiator1+1
							if(int(columns[14])==initiator2):
								count_initiator2=count_initiator2+1
								hop_count=hop_count+int(columns[18])
								hop_count_entry =hop_count_entry+1
							if(int(columns[14])==0):
								count_nt_rcv=count_nt_rcv+1
							group_ini=0
							for j in sorted(group1):
								if(j==int(i)):
									group_ini=group1_initiator
							for k in sorted(group2):
								if(k==int(i)):
									group_ini=group2_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				if(count_initiator1!=0 or count_initiator2!=0  or count_nt_rcv!=0):
					total=float(count_initiator1+count_initiator2+count_nt_rcv)
					percent_initiator1=float((count_initiator1/total)*100)
        			   	percent_initiator2=float((count_initiator2/total)*100)
					percent_nt_rcv=float((count_nt_rcv/total)*100)	
					if(group_ini==group2_initiator and flag>0):
						#avg_hop_count= hop_count/hop_count_entry
						relaibility=count_initiator2/total;
						damage_factor=count_nt_rcv/total;
						intrusion=float(count_initiator1)
						intrusion_factor=intrusion/total
						count=count+1
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy_c[m].append(i)
							m=m+1
						relaibility_d=(avg_relaibility_C-relaibility)**2
						damage_d=(avg_damage_C-damage_factor)**2
						intrusion_d=(avg_intrusion_C-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						print count,"\t",i,"\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_nt_rcv,1),"\t\t",count_initiator2,"\t\t",count_initiator1,"\t\t",count_nt_rcv,"\t\t",round(relaibility,4),"\t\t",round(damage_factor,4),"\t\t",round(intrusion_factor,4),"\t\t",avg_hop_count
					if (group_ini==group1_initiator and flag==0):
						print count,"\t",i,"\t",group_ini,"\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------"
		
		relaibility_sd_C=math.sqrt(relaibility_sum/(count-1))
		damage_sd_C=math.sqrt(damage_sum/(count-1))
		intrusion_sd_C=math.sqrt(intrusion_sum/(count-1))

		print"------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
		print "\t\t\t\t\t\t\t\t\t\t\t\t\t\tAverage------->",round(avg_relaibility_C,4),"\t\t",round(avg_damage_C,4),"\t\t",round(avg_intrusion_C,4)
		print "\t\t\t\t\t\t\t\t\t\t\t\t\tStandard Deviation---->",round(relaibility_sd_C,4),"\t\t",round(damage_sd_C,4),"\t\t",round(intrusion_sd_C,4)	
		m=0
	
		for target in cdf:
			print cdf[m],"\t|\t",len(listy_c[m]),"\t|\t"
			m=m+1
	
	f = []	
	listy = [[] for i in range(20)]
	hop_cnt_c = [[] for i in range(42)]
	print"------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "Without change"
	print"------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "SNo\t" "Node:\t""Init_Id\t\t""R_Self_init\t""R_other1\t""Nt recv\t\t""Num_Self_init\t""Num_other1""\t""No_pkt_recv""\t""Reliability""\t""Damage""\t\t""Intrusion"
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Flocklab/Hop_count/2_Group_clustering/W_1_2_13'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Flocklab/Hop_count/2_Group_clustering/W_1_2_13',name)
			f = open(fullName, 'r')
			total2=0
			count=0
			relaibility_total=0.0
			damage_total=0.0
			intrusion_total=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
			relaibility_sd_W=0.0
			damage_sd_W=0.0
			intrusion_sd_W=0.0
			relaibility_sum=0.0
			damage_sum=0.0
			intrusion_sum=0.0
			avg_relaibility_W=0.0
			avg_damage_S=0.0
			avg_intrusion_W=0.0
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				count_initiator1=0
				count_initiator2=0
				count_nt_rcv=0
				percent_initiator1=0.0
				percent_initiator2=0.0
				flag=0
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
					 	if (columns[2]) == i:
							#print("here")
							flag=flag+1
							#print(columns[1],columns[8])
							if(int(columns[13])==initiator1):
								count_initiator1=count_initiator1+1
							if(int(columns[13])==initiator2):
								count_initiator2=count_initiator2+1
							if(int(columns[13])==0):
								count_nt_rcv=count_nt_rcv+1
							group_ini=0
							for j in sorted(group1):
								if(j==int(i)):
									group_ini=group1_initiator
							for k in sorted(group2):
								if(k==int(i)):
									group_ini=group2_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				if(count_initiator1!=0 or count_initiator2!=0  or count_nt_rcv!=0):
					total=float(count_initiator1+count_initiator2+count_nt_rcv)
					percent_initiator1=float((count_initiator1/total)*100)
        			        percent_initiator2=float((count_initiator2/total)*100)
					percent_nt_rcv=float((count_nt_rcv/total)*100)
					if(group_ini==group1_initiator):
						count=count+1
						relaibility=count_initiator1/total;
						damage_factor=count_nt_rcv/total;
						intrusion=float(count_initiator2)
						intrusion_factor=intrusion/total
						relaibility_total=relaibility_total+relaibility
						damage_total=damage_total+damage_factor
						intrusion_total=intrusion_total+intrusion_factor
					if(group_ini==group2_initiator):
						count=count+1
						relaibility=count_initiator2/total;
						damage_factor=count_nt_rcv/total;
						intrusion=float(count_initiator1)
						intrusion_factor=intrusion/total
						relaibility_total=relaibility_total+relaibility
						damage_total=damage_total+damage_factor
						intrusion_total=intrusion_total+intrusion_factor
			count_initiator1=0
			count_initiator2=0
			count_nt_rcv=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
		#print relaibility_total,damage_total,intrusion_total,count
		avg_relaibility_W=relaibility_total/count
		avg_damage_W=damage_total/count
		avg_intrusion_W=intrusion_total/count
		#print avg_relaibility_W,avg_damage_W,avg_intrusion_W,count

		
		relaibility_sd_W=0.0
		damage_sd_W=0.0
		intrusion_sd_W=0.0
		f = []
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Flocklab/Hop_count/2_Group_clustering/W_1_2_13',name)
			f = open(fullName, 'r')
			count=0
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				count_initiator1=0
				count_initiator2=0
				count_nt_rcv=0
				percent_initiator1=0.0
				percent_initiator2=0.0
				relaibility=0.0
				damage_factor=0.0
				intrusion=0.0
				intrusion_factor=0.0
				relaibility_d=0.0
				damage_d=0.0
				intrusion_d=0.0
				relaibility_sum=0.0
				damage_sum=0.0
				intrusion_sum=0.0
				flag=0
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
					 	if (columns[2]) == i:
							#print("here")
							flag=flag+1
							#print(columns[1],columns[8])
							if(int(columns[13])==initiator1):
								count_initiator1=count_initiator1+1
							if(int(columns[13])==initiator2):
								count_initiator2=count_initiator2+1
							if(int(columns[13])==0):
								count_nt_rcv=count_nt_rcv+1
							group_ini=0
							for j in sorted(group1):
								if(j==int(i)):
									group_ini=group1_initiator
							for k in sorted(group2):
								if(k==int(i)):
									group_ini=group2_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				if(count_initiator1!=0 or count_initiator2!=0 or count_nt_rcv!=0):
					total=float(count_initiator1+count_initiator2+count_nt_rcv)
					percent_initiator1=float((count_initiator1/total)*100)
        			   	percent_initiator2=float((count_initiator2/total)*100)
					percent_nt_rcv=float((count_nt_rcv/total)*100)	
					if(group_ini==group1_initiator and flag>0):
						relaibility=count_initiator1/total;
						damage_factor=count_nt_rcv/total;
						intrusion=float(count_initiator2)
						intrusion_factor=intrusion/total
						count=count+1
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy[m].append(i)
							m=m+1
						relaibility_d=(avg_relaibility_W-relaibility)**2
						damage_d=(avg_damage_W-damage_factor)**2
						intrusion_d=(avg_intrusion_W-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						print count,"\t",i,"\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_nt_rcv,1),"\t\t",count_initiator1,"\t\t",count_initiator2,"\t\t",count_nt_rcv,"\t\t",round(relaibility,4),"\t\t",round(damage_factor,4),"\t\t",round(intrusion_factor,4)
					if (group_ini==group1_initiator and flag==0):
						print count,"\t",i,"\t",group_ini,"\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------"
		
		f = []
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Flocklab/Hop_count/2_Group_clustering/W_1_2_13',name)
			f = open(fullName, 'r')
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				count_initiator1=0
				count_initiator2=0
				count_nt_rcv=0
				percent_initiator1=0.0
				percent_initiator2=0.0
				relaibility=0.0
				damage_factor=0.0
				intrusion=0.0
				intrusion_factor=0.0
				relaibility_d=0.0
				damage_d=0.0
				intrusion_d=0.0
				flag=0
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
					 	if (columns[2]) == i:
							#print("here")
							flag=flag+1
							#print(columns[1],columns[8])
							if(int(columns[13])==initiator1):
								count_initiator1=count_initiator1+1
							if(int(columns[13])==initiator2):
								count_initiator2=count_initiator2+1
							if(int(columns[13])==0):
								count_nt_rcv=count_nt_rcv+1
							group_ini=0
							for j in sorted(group1):
								if(j==int(i)):
									group_ini=group1_initiator
							for k in sorted(group2):
								if(k==int(i)):
									group_ini=group2_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				if(count_initiator1!=0 or count_initiator2!=0  or count_nt_rcv!=0):
					total=float(count_initiator1+count_initiator2+count_nt_rcv)
					percent_initiator1=float((count_initiator1/total)*100)
        			   	percent_initiator2=float((count_initiator2/total)*100)
					percent_nt_rcv=float((count_nt_rcv/total)*100)	
					if(group_ini==group2_initiator and flag>0):
						relaibility=count_initiator2/total;
						damage_factor=count_nt_rcv/total;
						intrusion=float(count_initiator1)
						intrusion_factor=intrusion/total
						count=count+1
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy[m].append(i)
							m=m+1
						relaibility_d=(avg_relaibility_W-relaibility)**2
						damage_d=(avg_damage_W-damage_factor)**2
						intrusion_d=(avg_intrusion_W-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						print count,"\t",i,"\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_nt_rcv,1),"\t\t",count_initiator2,"\t\t",count_initiator1,"\t\t",count_nt_rcv,"\t\t",round(relaibility,4),"\t\t",round(damage_factor,4),"\t\t",round(intrusion_factor,4)
					if (group_ini==group1_initiator and flag==0):
						print count,"\t",i,"\t",group_ini,"\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------"
	
		relaibility_sd_W=math.sqrt(relaibility_sum/(count-1))
		damage_sd_W=math.sqrt(damage_sum/(count-1))
		intrusion_sd_W=math.sqrt(intrusion_sum/(count-1))			

		
	print"----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "\t\t\t\t\t\t\t\t\t\t\t\t\t\tAverage------->",round(avg_relaibility_W,4),"\t\t",round(avg_damage_W,4),"\t\t",round(avg_intrusion_W,4)
	print "\t\t\t\t\t\t\t\t\t\t\t\t\tStandard Deviation---->",round(relaibility_sd_W,4),"\t\t",round(damage_sd_W,4),"\t\t",round(intrusion_sd_W,4)
	m=0
	for target in cdf:
		print cdf[m],"\t|\t",len(listy[m]),"\t|\t"
		m=m+1
	f = []	
	listy_s = [[] for i in range(20)]
	hop_cnt_c = [[] for i in range(42)]	
	print"---------------------------------------------------------------------------------------------------"
	print "With SFD change"		
	print"---------------------------------------------------------------------------------------------------"
	print "SNo\t" "Node:\t""Init_Id\t\t""R_Self_init\t""R_other1\t""Nt recv\t\t""Num_Self_init\t""Num_other1""\t""No_pkt_recv""\t""Reliability""\t""Damage""\t\t""Intrusion"
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Flocklab/Hop_count/2_Group_clustering/S_1_2_13'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Flocklab/Hop_count/2_Group_clustering/S_1_2_13',name)
			f = open(fullName, 'r')
			total2=0
			count=0
			relaibility_total=0.0
			damage_total=0.0
			intrusion_total=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
			relaibility_sd_S=0.0
			damage_sd_S=0.0
			intrusion_sd_S=0.0
			relaibility_sum=0.0
			damage_sum=0.0
			intrusion_sum=0.0
			avg_relaibility_S=0.0
			avg_damage_S=0.0
			avg_intrusion_S=0.0
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				count_initiator1=0
				count_initiator2=0
				count_nt_rcv=0
				percent_initiator1=0.0
				percent_initiator2=0.0
				flag=0
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
					 	if (columns[2]) == i:
							#print("here")
							flag=flag+1
							#print(columns[1],columns[8])
							if(int(columns[13])==initiator1):
								count_initiator1=count_initiator1+1
							if(int(columns[13])==initiator2):
								count_initiator2=count_initiator2+1
							if(int(columns[13])==0):
								count_nt_rcv=count_nt_rcv+1
							group_ini=0
							for j in sorted(group1):
								if(j==int(i)):
									group_ini=group1_initiator
							for k in sorted(group2):
								if(k==int(i)):
									group_ini=group2_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				if(count_initiator1!=0 or count_initiator2!=0 or count_nt_rcv!=0):
					total=float(count_initiator1+count_initiator2+count_nt_rcv)
					percent_initiator1=float((count_initiator1/total)*100)
        			        percent_initiator2=float((count_initiator2/total)*100)
					percent_nt_rcv=float((count_nt_rcv/total)*100)
					if(group_ini==group1_initiator):
						count=count+1
						relaibility=count_initiator1/total;
						damage_factor=count_nt_rcv/total;
						intrusion=float(count_initiator2)
						intrusion_factor=intrusion/total
						relaibility_total=relaibility_total+relaibility
						damage_total=damage_total+damage_factor
						intrusion_total=intrusion_total+intrusion_factor
					if(group_ini==group2_initiator):
						count=count+1
						relaibility=count_initiator2/total;
						damage_factor=count_nt_rcv/total;
						intrusion=float(count_initiator1)
						intrusion_factor=intrusion/total
						relaibility_total=relaibility_total+relaibility
						damage_total=damage_total+damage_factor
						intrusion_total=intrusion_total+intrusion_factor
			count_initiator1=0
			count_initiator2=0
			count_nt_rcv=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
		#print relaibility_total,damage_total,intrusion_total,count
		avg_relaibility_S=relaibility_total/count
		avg_damage_S=damage_total/count
		avg_intrusion_S=intrusion_total/count
		#print avg_relaibility_W,avg_damage_W,avg_intrusion_W,count

		
		relaibility_sd_S=0.0
		damage_sd_S=0.0
		intrusion_sd_S=0.0
		f = []
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Flocklab/Hop_count/2_Group_clustering/S_1_2_13',name)
			f = open(fullName, 'r')
			count=0
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				count_initiator1=0
				count_initiator2=0
				count_nt_rcv=0
				percent_initiator1=0.0
				percent_initiator2=0.0
				relaibility=0.0
				damage_factor=0.0
				intrusion=0.0
				intrusion_factor=0.0
				relaibility_d=0.0
				damage_d=0.0
				intrusion_d=0.0
				relaibility_sum=0.0
				damage_sum=0.0
				intrusion_sum=0.0
				flag=0
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
					 	if (columns[2]) == i:
							#print("here")
							flag=flag+1
							#print(columns[1],columns[8])
							if(int(columns[13])==initiator1):
								count_initiator1=count_initiator1+1
							if(int(columns[13])==initiator2):
								count_initiator2=count_initiator2+1
							if(int(columns[13])==0):
								count_nt_rcv=count_nt_rcv+1
							group_ini=0
							for j in sorted(group1):
								if(j==int(i)):
									group_ini=group1_initiator
							for k in sorted(group2):
								if(k==int(i)):
									group_ini=group2_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				if(count_initiator1!=0 or count_initiator2!=0 or count_nt_rcv!=0):
					total=float(count_initiator1+count_initiator2+count_nt_rcv)
					percent_initiator1=float((count_initiator1/total)*100)
        			   	percent_initiator2=float((count_initiator2/total)*100)
					percent_nt_rcv=float((count_nt_rcv/total)*100)	
					if(group_ini==group1_initiator and flag>0):
						relaibility=count_initiator1/total;
						damage_factor=count_nt_rcv/total;
						intrusion=float(count_initiator2)
						intrusion_factor=intrusion/total
						count=count+1
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy_s[m].append(i)
							m=m+1
						relaibility_d=(avg_relaibility_S-relaibility)**2
						damage_d=(avg_damage_S-damage_factor)**2
						intrusion_d=(avg_intrusion_S-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						print count,"\t",i,"\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_nt_rcv,1),"\t\t",count_initiator1,"\t\t",count_initiator2,"\t\t",count_nt_rcv,"\t\t",round(relaibility,4),"\t\t",round(damage_factor,4),"\t\t",round(intrusion_factor,4)
					if (group_ini==group1_initiator and flag==0):
						print count,"\t",i,"\t",group_ini,"\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------"
		
		f = []
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Flocklab/Hop_count/2_Group_clustering/S_1_2_13',name)
			f = open(fullName, 'r')
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				count_initiator1=0
				count_initiator2=0
				count_nt_rcv=0
				percent_initiator1=0.0
				percent_initiator2=0.0
				relaibility=0.0
				damage_factor=0.0
				intrusion=0.0
				intrusion_factor=0.0
				relaibility_d=0.0
				damage_d=0.0
				intrusion_d=0.0
				flag=0
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
					 	if (columns[2]) == i:
							#print("here")
							flag=flag+1
							#print(columns[1],columns[8])
							if(int(columns[13])==initiator1):
								count_initiator1=count_initiator1+1
							if(int(columns[13])==initiator2):
								count_initiator2=count_initiator2+1
							if(int(columns[13])==0):
								count_nt_rcv=count_nt_rcv+1
							group_ini=0
							for j in sorted(group1):
								if(j==int(i)):
									group_ini=group1_initiator
							for k in sorted(group2):
								if(k==int(i)):
									group_ini=group2_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				if(count_initiator1!=0 or count_initiator2!=0  or count_nt_rcv!=0):
					total=float(count_initiator1+count_initiator2+count_nt_rcv)
					percent_initiator1=float((count_initiator1/total)*100)
        			   	percent_initiator2=float((count_initiator2/total)*100)
					percent_nt_rcv=float((count_nt_rcv/total)*100)	
					if(group_ini==group2_initiator and flag>0):
						relaibility=count_initiator2/total;
						damage_factor=count_nt_rcv/total;
						intrusion=float(count_initiator1)
						intrusion_factor=intrusion/total
						count=count+1
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy_s[m].append(i)
							m=m+1
						relaibility_d=(avg_relaibility_S-relaibility)**2
						damage_d=(avg_damage_S-damage_factor)**2
						intrusion_d=(avg_intrusion_S-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						print count,"\t",i,"\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_nt_rcv,1),"\t\t",count_initiator2,"\t\t",count_initiator1,"\t\t",count_nt_rcv,"\t\t",round(relaibility,4),"\t\t",round(damage_factor,4),"\t\t",round(intrusion_factor,4)
					if (group_ini==group1_initiator and flag==0):
						print count,"\t",i,"\t",group_ini,"\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------"
		
		
		relaibility_sd_S=math.sqrt(relaibility_sum/(count-1))
		damage_sd_S=math.sqrt(damage_sum/(count-1))
		intrusion_sd_S=math.sqrt(intrusion_sum/(count-1))

		print"------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
		print "\t\t\t\t\t\t\t\t\t\t\t\t\t\tAverage------->",round(avg_relaibility_S,4),"\t\t",round(avg_damage_S,4),"\t\t",round(avg_intrusion_S,4)
		print "\t\t\t\t\t\t\t\t\t\t\t\t\tStandard Deviation---->",round(relaibility_sd_S,4),"\t\t",round(damage_sd_S,4),"\t\t",round(intrusion_sd_S,4)
		print"------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

	m=0
	for target in cdf:
		print cdf[m],"\t|\t",len(listy_s[m]),"\t|\t"
		m=m+1
	listy_h = [[] for i in range(20)]
	hop_cnt_c = [[] for i in range(42)]
	f = []	
	print"----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "With Header change"		
	print"----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "SNo\t" "Node:\t""Init_Id\t\t""R_Self_init\t""R_other1\t""Nt recv\t\t""Num_Self_init\t""Num_other1""\t""No_pkt_recv""\t""Reliability""\t""Damage""\t\t""Intrusion"
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Flocklab/Hop_count/2_Group_clustering/H_1_2_13'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Flocklab/Hop_count/2_Group_clustering/H_1_2_13',name)
			f = open(fullName, 'r')
			total2=0
			count=0
			relaibility_total=0.0
			damage_total=0.0
			intrusion_total=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
			relaibility_sd_H=0.0
			damage_sd_H=0.0
			intrusion_sd_H=0.0
			relaibility_sum=0.0
			damage_sum=0.0
			intrusion_sum=0.0
			avg_relaibility_H=0.0
			avg_damage_H=0.0
			avg_intrusion_H=0.0
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				count_initiator1=0
				count_initiator2=0
				count_nt_rcv=0
				percent_initiator1=0.0
				percent_initiator2=0.0
				flag=0
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
					 	if (columns[2]) == i:
							#print("here")
							flag=flag+1
							#print(columns[1],columns[8])
							if(int(columns[13])==initiator1):
								count_initiator1=count_initiator1+1
							if(int(columns[13])==initiator2):
								count_initiator2=count_initiator2+1
							if(int(columns[13])==0):
								count_nt_rcv=count_nt_rcv+1
							group_ini=0
							for j in sorted(group1):
								if(j==int(i)):
									group_ini=group1_initiator
							for k in sorted(group2):
								if(k==int(i)):
									group_ini=group2_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				if(count_initiator1!=0 or count_initiator2!=0 or count_nt_rcv!=0):
					total=float(count_initiator1+count_initiator2+count_nt_rcv)
					percent_initiator1=float((count_initiator1/total)*100)
        			        percent_initiator2=float((count_initiator2/total)*100)
					percent_nt_rcv=float((count_nt_rcv/total)*100)
					if(group_ini==group1_initiator):
						count=count+1
						relaibility=count_initiator1/total;
						damage_factor=count_nt_rcv/total;
						intrusion=float(count_initiator2)
						intrusion_factor=intrusion/total
						relaibility_total=relaibility_total+relaibility
						damage_total=damage_total+damage_factor
						intrusion_total=intrusion_total+intrusion_factor
					if(group_ini==group2_initiator):
						count=count+1
						relaibility=count_initiator2/total;
						damage_factor=count_nt_rcv/total;
						intrusion=float(count_initiator1)
						intrusion_factor=intrusion/total
						relaibility_total=relaibility_total+relaibility
						damage_total=damage_total+damage_factor
						intrusion_total=intrusion_total+intrusion_factor
			count_initiator1=0
			count_initiator2=0
			count_nt_rcv=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
		#print relaibility_total,damage_total,intrusion_total,count
		avg_relaibility_H=relaibility_total/count
		avg_damage_H=damage_total/count
		avg_intrusion_H=intrusion_total/count
		#print avg_relaibility_W,avg_damage_W,avg_intrusion_W,count

		
		relaibility_sd_H=0.0
		damage_sd_H=0.0
		intrusion_sd_H=0.0
		f = []
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Flocklab/Hop_count/2_Group_clustering/H_1_2_13',name)
			f = open(fullName, 'r')
			count=0
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				count_initiator1=0
				count_initiator2=0
				count_nt_rcv=0
				percent_initiator1=0.0
				percent_initiator2=0.0
				relaibility=0.0
				damage_factor=0.0
				intrusion=0.0
				intrusion_factor=0.0
				relaibility_d=0.0
				damage_d=0.0
				intrusion_d=0.0
				relaibility_sum=0.0
				damage_sum=0.0
				intrusion_sum=0.0
				flag=0
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
					 	if (columns[2]) == i:
							#print("here")
							flag=flag+1
							#print(columns[1],columns[8])
							if(int(columns[13])==initiator1):
								count_initiator1=count_initiator1+1
							if(int(columns[13])==initiator2):
								count_initiator2=count_initiator2+1
							if(int(columns[13])==0):
								count_nt_rcv=count_nt_rcv+1
							group_ini=0
							for j in sorted(group1):
								if(j==int(i)):
									group_ini=group1_initiator
							for k in sorted(group2):
								if(k==int(i)):
									group_ini=group2_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				if(count_initiator1!=0 or count_initiator2!=0 or count_nt_rcv!=0):
					total=float(count_initiator1+count_initiator2+count_nt_rcv)
					percent_initiator1=float((count_initiator1/total)*100)
        			   	percent_initiator2=float((count_initiator2/total)*100)
					percent_nt_rcv=float((count_nt_rcv/total)*100)	
					if(group_ini==group1_initiator and flag>0):
						relaibility=count_initiator1/total;
						damage_factor=count_nt_rcv/total;
						intrusion=float(count_initiator2)
						intrusion_factor=intrusion/total
						count=count+1
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy_h[m].append(i)
							m=m+1
						relaibility_d=(avg_relaibility_S-relaibility)**2
						damage_d=(avg_damage_S-damage_factor)**2
						intrusion_d=(avg_intrusion_S-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						print count,"\t",i,"\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_nt_rcv,1),"\t\t",count_initiator1,"\t\t",count_initiator2,"\t\t",count_nt_rcv,"\t\t",round(relaibility,4),"\t\t",round(damage_factor,4),"\t\t",round(intrusion_factor,4)
					if (group_ini==group1_initiator and flag==0):
						print count,"\t",i,"\t",group_ini,"\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------"
		
		f = []
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-test/Data/Flocklab/Hop_count/2_Group_clustering/H_1_2_13',name)
			f = open(fullName, 'r')
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				count_initiator1=0
				count_initiator2=0
				count_nt_rcv=0
				percent_initiator1=0.0
				percent_initiator2=0.0
				relaibility=0.0
				damage_factor=0.0
				intrusion=0.0
				intrusion_factor=0.0
				relaibility_d=0.0
				damage_d=0.0
				intrusion_d=0.0
				flag=0
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
					 	if (columns[2]) == i:
							#print("here")
							flag=flag+1
							#print(columns[1],columns[8])
							if(int(columns[13])==initiator1):
								count_initiator1=count_initiator1+1
							if(int(columns[13])==initiator2):
								count_initiator2=count_initiator2+1
							if(int(columns[13])==0):
								count_nt_rcv=count_nt_rcv+1
							group_ini=0
							for j in sorted(group1):
								if(j==int(i)):
									group_ini=group1_initiator
							for k in sorted(group2):
								if(k==int(i)):
									group_ini=group2_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				if(count_initiator1!=0 or count_initiator2!=0  or count_nt_rcv!=0):
					total=float(count_initiator1+count_initiator2+count_nt_rcv)
					percent_initiator1=float((count_initiator1/total)*100)
        			   	percent_initiator2=float((count_initiator2/total)*100)
					percent_nt_rcv=float((count_nt_rcv/total)*100)	
					if(group_ini==group2_initiator and flag>0):
						relaibility=count_initiator2/total;
						damage_factor=count_nt_rcv/total;
						intrusion=float(count_initiator1)
						intrusion_factor=intrusion/total
						count=count+1
						m=0
						for target in cdf:
							if(relaibility >= target):
							#print relaibility,count_100per_rel;
								listy_h[m].append(i)
							m=m+1
						relaibility_d=(avg_relaibility_S-relaibility)**2
						damage_d=(avg_damage_S-damage_factor)**2
						intrusion_d=(avg_intrusion_S-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						print count,"\t",i,"\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_nt_rcv,1),"\t\t",count_initiator2,"\t\t",count_initiator1,"\t\t",count_nt_rcv,"\t\t",round(relaibility,4),"\t\t",round(damage_factor,4),"\t\t",round(intrusion_factor,4)
					if (group_ini==group1_initiator and flag==0):
						print count,"\t",i,"\t",group_ini,"\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------","\t\t","-------"
		
		
		relaibility_sd_H=math.sqrt(relaibility_sum/(count-1))
		damage_sd_H=math.sqrt(damage_sum/(count-1))
		intrusion_sd_H=math.sqrt(intrusion_sum/(count-1))

		print"------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
		print "\t\t\t\t\t\t\t\t\t\t\t\t\t\tAverage------->",round(avg_relaibility_H,4),"\t\t",round(avg_damage_H,4),"\t\t",round(avg_intrusion_H,4)
		print "\t\t\t\t\t\t\t\t\t\t\t\t\tStandard Deviation---->",round(relaibility_sd_H,4),"\t\t",round(damage_sd_H,4),"\t\t",round(intrusion_sd_H,4)
		print"------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	m=0
	for target in cdf:
		print cdf[m],"\t|\t",len(listy_h[m]),"\t|\t"
		m=m+1














	increase_relaibilty_S = (avg_relaibility_S-avg_relaibility_W)
	increase_damage_S=avg_damage_S-avg_damage_W
	decrease_intrusion_S=avg_intrusion_W-avg_intrusion_S
	improvement_relaibilty_S=(increase_relaibilty_S/avg_relaibility_W)*100
	improvement_intrusion_S=((decrease_intrusion_S)/avg_intrusion_W)*100
	improvement_damage_S=((increase_damage_S)/avg_damage_W)*100
	print "SFD Change Improvement Details"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "Relaibility Inceases: ",round(improvement_relaibilty_S,3)
	print "Damage Increases: ",round(improvement_damage_S,3)
	print "Intrusion Decreases: ",round(improvement_intrusion_S,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	increase_relaibilty_C = (avg_relaibility_C-avg_relaibility_W)
	increase_damage_C=avg_damage_C-avg_damage_W
	decrease_intrusion_C=avg_intrusion_W-avg_intrusion_C
	improvement_relaibilty_C=(increase_relaibilty_C/avg_relaibility_W)*100
	improvement_intrusion_C=((decrease_intrusion_C)/avg_intrusion_W)*100
	improvement_damage_C=((increase_damage_C)/avg_damage_W)*100
	print "Channel Change Improvement Details"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "Relaibility Inceases: ",round(improvement_relaibilty_C,3)
	print "Damage Increases: ",round(improvement_damage_C,3)
	print "Intrusion Decreases: ",round(improvement_intrusion_C,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	print "--------------------------------------------------------------------------------------------------------"
	print "%","\t|\t","No change","\t|\t","Channel change","\t|\t","SFD change","\t|\t","Header change","\t|\t"
	print "--------------------------------------------------------------------------------------------------------"
	m=0
	for target in cdf:
		print cdf[m],"\t|\t\t",len(listy[m]),"\t|\t\t",len(listy_c[m]),"\t|\t\t",len(listy_s[m]),"\t|\t\t",len(listy_h[m]),"\t|\t"
		m=m+1
	print "--------------------------------------------------------------------------------------------------------"		
def main():

    if len(sys.argv) != 1:
        print '...'
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()
	
