import sys
import os
import csv
import math  
from os import walk
from fileinput import close

def analyze():

	node_count=0
	parent_list = []
	group1=[4,6,31,3,8,33,16,22,26,20,24,18,13]
	group2=[15,10,32,23,1,2,28,25,19,27,11,17]
	#group1=[20,19]
	#group2=[25,11]
	#group3=[17,13]
	#group4=[10,26]
	#group5=[24,23]
	#group6=[18,27]
	#group7=[22,28]
	#group8=[16,6]
	#group9=[31,32]
	#group10=[3,33]
	#group11=[15,8]
	#group12=[2,1,4]
	node_set =['4','1','2','8','15','33','3','31','32','10','6','16','20','26','19','25','11','17','23','24','27','18','28','22','13']
	group2_initiator=group2[0]
	group1_initiator=group1[0]
	#group3_initiator=group3[0]
	#group4_initiator=group4[0]
	#group5_initiator=group5[0]
	#group6_initiator=group6[0]
	#group7_initiator=group7[0]
	#group8_initiator=group8[0]
	#group9_initiator=group9[0]
	#group10_initiator=group10[0]
	#group11_initiator=group11[0]
	#group12_initiator=group12[0]
	count=0
	f = []	
	count_100per_rel=[]
	victim_nodes=[]
	cdf=[0.999,0.990,0.985,0.980,0.975,0.900,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0.05,0]
	len_cdf=len(cdf)
	listy = [[] for i in range(30)]
	print "With Channel change"		
	print"------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "SNo\t" "Node:\t""Init_Id\t""R_Self_init\t""R_other1\t""Nt recv\t""Num_Self_init\t""Num_other1\t""No_pkt_recv""\t""Reliability""\t""Damage""\t""Intrusion""\t""Radio-on""\t""Relay-count""\t""Latency"
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-mint-test/Data/Flocklab/10thNov/Mixed/2_Groups/4_15/C_4_15'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-mint-test/Data/Flocklab/10thNov/Mixed/2_Groups/4_15/C_4_15',name)
			f = open(fullName, 'r')
			relaibility_total=0.0
			damage_total=0.0
			intrusion_total=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
			relaibility_sd_NC=0.0
			damage_sd_NC=0.0
			intrusion_sd_NC=0.0
			relaibility_sum=0.0
			damage_sum=0.0
			intrusion_sum=0.0	
			relay_count_total=0.0
			count_radio_on=0.0
			count_latency=0.0
			average_latency_total=0.0
			average_radio_on_time_total=0.0
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				self_initiator=0
				other_initiator=0
				no_rcv_cnt=0
				percent_self_initiator=0.0
				percent_other_initiator=0.0
				percent_initiator_nt_rcv=0.0
				relaibility=0.0
				damage_factor=0.0
				intrusion=0.0
				intrusion_factor=0.0
				average_radio_on_time=0.0
				average_relay_count=0.0
				average_latency=0.0
				relay_count=0
				one=0
				two=0
				three=0
				four=0
				five=0
				zero=0
				number_of_iteration=0
				latency_iteration=0
				radio_on_time=0.0
				latency=0.0
				for line in f:
					line = line.strip()
					columns = line.split()
					
					try:
						if (columns[2]) == i:
							node_id=int(columns[2])
							#print columns
					 		if int(columns[14])!=0:							 
                                        	 	        self_initiator=self_initiator+1;
								if int(columns[14])==1:
									one=one+1;
								if int(columns[14])==2:
									two=two+1;
								if int(columns[14])==3:
									three=three+1;
								if int(columns[14])==4:
									four=four+1;
								if int(columns[14])==5:
									five=five+1;
							elif int(columns[14])==0 and int(columns[15])!=0 and int(columns[16])!=0:
								other_initiator=other_initiator+1;
								zero=zero+1;	
							elif int(columns[14])==0 and int(columns[15])==0 or  int(columns[16])==0:
								no_rcv_cnt=no_rcv_cnt+1;
								zero=zero+1;	
							for j in sorted(group1):
								if(j==int(columns[2])):
									group_ini=group1_initiator
							for j in sorted(group2):
								if(j==int(columns[2])):
									group_ini=group2_initiator
							"""
							for j in sorted(group3):
								if(j==int(columns[2])):
									group_ini=group3_initiator
							for j in sorted(group4):
								if(j==int(columns[2])):
									group_ini=group4_initiator
							for j in sorted(group5):
								if(j==int(columns[2])):
									group_ini=group5_initiator
							for j in sorted(group6):
								if(j==int(columns[2])):
									group_ini=group6_initiator
							for j in sorted(group7):
								if(j==int(columns[2])):
									group_ini=group7_initiator
							for j in sorted(group8):
								if(j==int(columns[2])):
									group_ini=group8_initiator
							for j in sorted(group9):
								if(j==int(columns[2])):
									group_ini=group9_initiator
							for j in sorted(group10):
								if(j==int(columns[2])):
									group_ini=group10_initiator
							for j in sorted(group11):
								if(j==int(columns[2])):
									group_ini=group11_initiator
							for j in sorted(group12):
								if(j==int(columns[2])):
									group_ini=group12_initiator"""
#Calculate relay count and latency only if it recieves packet from its own initiator
							if int(columns[14])!=0:	
								radio_on_time = radio_on_time + float(columns[22])
								relay_count=relay_count+float(columns[23])
								number_of_iteration=number_of_iteration+1
							if(float(columns[12])!=0.0):
								latency=latency+float(columns[12])
								latency_iteration=latency_iteration+1
							if(int(columns[2])==group_ini):
								latency=0
					except (IndexError,ValueError):
							gotdata = 'null'
				if(self_initiator!=0 or other_initiator!=0 or no_rcv_cnt!=0):
					count=count+1
					total=float(self_initiator+other_initiator+no_rcv_cnt)
					percent_self_initiator=float((self_initiator/total)*100)
        	               	 	percent_other_initiator=float((other_initiator/total)*100)
       		                	percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=self_initiator/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(other_initiator)
					intrusion_factor=intrusion/total
					relaibility_total=relaibility_total+relaibility
					damage_total=damage_total+damage_factor
					intrusion_total=intrusion_total+intrusion_factor
					if(number_of_iteration!=0):
						#print average_radio_on_time_total
						average_radio_on_time=radio_on_time/number_of_iteration
						average_relay_count=relay_count/number_of_iteration
						average_radio_on_time_total = average_radio_on_time_total+average_radio_on_time
						relay_count_total = relay_count_total+average_relay_count
						count_radio_on=count_radio_on+1
					if(latency_iteration!=0):
						average_latency=latency/latency_iteration
						average_latency_total = average_latency_total+average_latency
						count_latency=count_latency+1
					m=0
					for target in cdf:
						if(relaibility >= target):
							listy[m].append(node_id)
						m=m+1
					if(relaibility >=0.9):
						count_100per_rel.append(node_id)
					else:
						victim_nodes.append(node_id)
					print count,"\t",node_id,"\t",group_ini,"\t",round(percent_self_initiator,2),"\t\t",round(percent_other_initiator,2),"\t\t",round(percent_initiator_nt_rcv,2),"\t",self_initiator,"\t\t",other_initiator,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,4),"\t\t",round(damage_factor,4),"\t",round(intrusion_factor,4),"\t\t",round(average_radio_on_time,3),"\t\t",round(average_relay_count,3),"\t\t",round(average_latency,3)
				self_initiator=0
				other_initiator=0
				no_rcv_cnt=0
				percent_self_initiator=0.0
				percent_other_initiator=0.0
				percent_initiator_nt_rcv=0.0
				relaibility=0.0
				damage_factor=0.0
				intrusion=0.0
				intrusion_factor=0.0
				number_of_iteration=0
				latency_iteration=0
				one=0
				two=0
				three=0
				four=0
				five=0
				zero=0
		avg_relaibility_NC=relaibility_total/count
		avg_damage_NC=damage_total/count
		avg_intrusion_NC=intrusion_total/count
		radio_on_time = average_radio_on_time_total/count_radio_on
		latency_final=average_latency_total/count_latency
		relay_Count_final=relay_count_total/count_radio_on
		print average_latency_total,average_radio_on_time_total
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#print round(relaibility_total,3),round(damage_total,3),round(intrusion_total,3),count
	print "Average------------------------------------------------------------------------------------------------------->\t",round(avg_relaibility_NC,3),"\t\t",round(avg_damage_NC,3),"\t",round(avg_intrusion_NC,3),"\t\t",round(radio_on_time,3),"\t\t",round(relay_Count_final,3),"\t\t",round(latency_final,3)
	print "No of nodes with 100% relaibilty---------------------------------------------------------------------------------------------->\t",len(count_100per_rel)
	print "Non-Victim nodes :",count_100per_rel
	print "No of victim nodes ----------------------------------------------------------------------------------------------------------->\t",len(victim_nodes)
	print "Victim nodes :",victim_nodes
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "CDF TABLE-NO CHANGE "
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	m=0
	for target in cdf:
		print cdf[m],"\t|\t",len(listy[m]),"\t|\t",listy[m]
		m=m+1	
	

def main():

    if len(sys.argv) != 1:
        print '...'
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()
	
