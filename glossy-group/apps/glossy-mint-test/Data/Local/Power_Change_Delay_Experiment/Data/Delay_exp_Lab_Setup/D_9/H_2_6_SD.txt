------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
With No Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SNo	Node:	Init_Id		R_Self_init	R_other1	Nt recv		Num_Self_init	Num_other1	No_pkt_recv	Reliability	Damage		Intrusion
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
0.456108385302 0.543891614698 0.0
1 	4 	2 		46.31 		0.0 		53.69 		69 		0 		80 		0.4631 		0.5369 		0.0 		
2 	2 	2 		48.59 		0.0 		51.41 		69 		0 		73 		0.4859 		0.5141 		0.0 		
3 	1 	5 		43.62 		0.0 		56.38 		65 		0 		84 		0.4362 		0.5638 		0.0 		
4 	5 	5 		43.92 		0.0 		56.08 		65 		0 		83 		0.4392 		0.5608 		0.0 		
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Average----------------------------------------------------------------------------------------------------------------------->	0.456 		0.544 		0.0
Average----------------------------------------------------------------------------------------------------------------------->	0.023 		0.023 		0.0
No of nodes with 100% relaibilty---------------------------------------------------------------------------------------------->	0
Non-Victim nodes : []
No of victim nodes ----------------------------------------------------------------------------------------------------------->	4
Victim nodes : [4, 2, 1, 5]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CDF TABLE-NO CHANGE 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
0.999 	|	0 	|	[]
0.99 	|	0 	|	[]
0.985 	|	0 	|	[]
0.98 	|	0 	|	[]
0.975 	|	0 	|	[]
0.9 	|	0 	|	[]
0.8 	|	0 	|	[]
0.7 	|	0 	|	[]
0.6 	|	0 	|	[]
0.5 	|	0 	|	[]
0.4 	|	4 	|	[4, 2, 1, 5]
0.3 	|	4 	|	[4, 2, 1, 5]
0.2 	|	4 	|	[4, 2, 1, 5]
0.1 	|	4 	|	[4, 2, 1, 5]
0.05 	|	4 	|	[4, 2, 1, 5]
0 	|	4 	|	[4, 2, 1, 5]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Node:	ZERO	ONE	TWO	THREE	FOUR	FIVE	
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4 	80 	69 	0 	0 	0 	0
2 	73 	69 	0 	0 	0 	0
1 	84 	65 	0 	0 	0 	0
5 	83 	65 	0 	0 	0 	0
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
0.55 
1.0 
1.0 
1.0 
1.0 
1.0
