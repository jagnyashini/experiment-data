------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
With No Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SNo	Node:	Init_Id		R_Self_init	R_other1	Nt recv		Num_Self_init	Num_other1	No_pkt_recv	Reliability	Damage		Intrusion
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
0.958354944675 0.041645055325 0.0
1 	4 	2 		100.0 		0.0 		0.0 		240 		0 		0 		1.0 		0.0 		0.0 		
2 	2 	2 		97.93 		0.0 		2.07 		236 		0 		5 		0.9793 		0.0207 		0.0 		
3 	1 	5 		100.0 		0.0 		0.0 		241 		0 		0 		1.0 		0.0 		0.0 		
4 	5 	5 		85.42 		0.0 		14.58 		205 		0 		35 		0.8542 		0.1458 		0.0 		
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Average----------------------------------------------------------------------------------------------------------------------->	0.958 		0.042 		0.0
Average----------------------------------------------------------------------------------------------------------------------->	0.061 		0.061 		0.0
No of nodes with 100% relaibilty---------------------------------------------------------------------------------------------->	3
Non-Victim nodes : [4, 2, 1]
No of victim nodes ----------------------------------------------------------------------------------------------------------->	1
Victim nodes : [5]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CDF TABLE-NO CHANGE 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
0.999 	|	2 	|	[4, 1]
0.99 	|	2 	|	[4, 1]
0.985 	|	2 	|	[4, 1]
0.98 	|	2 	|	[4, 1]
0.975 	|	3 	|	[4, 2, 1]
0.9 	|	3 	|	[4, 2, 1]
0.8 	|	4 	|	[4, 2, 1, 5]
0.7 	|	4 	|	[4, 2, 1, 5]
0.6 	|	4 	|	[4, 2, 1, 5]
0.5 	|	4 	|	[4, 2, 1, 5]
0.4 	|	4 	|	[4, 2, 1, 5]
0.3 	|	4 	|	[4, 2, 1, 5]
0.2 	|	4 	|	[4, 2, 1, 5]
0.1 	|	4 	|	[4, 2, 1, 5]
0.05 	|	4 	|	[4, 2, 1, 5]
0 	|	4 	|	[4, 2, 1, 5]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Node:	ZERO	ONE	TWO	THREE	FOUR	FIVE	
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
4 	0 	240 	0 	0 	0 	0
2 	5 	236 	0 	0 	0 	0
1 	0 	241 	0 	0 	0 	0
5 	35 	205 	0 	0 	0 	0
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
0.0 
1.0 
1.0 
1.0 
1.0 
1.0
