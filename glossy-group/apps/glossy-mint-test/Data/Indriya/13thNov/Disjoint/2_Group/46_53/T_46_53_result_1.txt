------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
With No Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SNo	Node:	Init_Id	R_Self_init	R_other1	Nt recv	Num_Self_init	Num_other1	No_pkt_recv	Reliability	Damage	Intrusion	Radio-on	Relay-count	Latency
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
1 	45 	46 	97.78 		0.77 		1.45 	2023 		16 		30 		0.9778 		0.0145 	0.0077 		10.054 		2.554 		2.892
2 	57 	53 	94.48 		1.21 		4.31 	1952 		25 		89 		0.9448 		0.0431 	0.0121 		8.226 		3.057 		3.296
3 	47 	46 	95.3 		0.66 		4.04 	1887 		13 		80 		0.953 		0.0404 	0.0066 		8.932 		1.878 		2.352
4 	15 	53 	91.25 		3.79 		4.96 	1878 		78 		102 		0.9125 		0.0496 	0.0379 		15.451 		4.863 		4.734
5 	30 	53 	25.53 		72.93 		1.54 	530 		1514 		32 		0.2553 		0.0154 	0.7293 		19.189 		14.511 		12.641
6 	16 	46 	69.69 		29.15 		1.16 	1439 		602 		24 		0.6969 		0.0116 	0.2915 		9.543 		4.499 		4.228
7 	17 	46 	69.85 		29.47 		0.68 	1441 		608 		14 		0.6985 		0.0068 	0.2947 		9.03 		3.795 		3.61
8 	48 	46 	95.6 		0.68 		3.72 	1978 		14 		77 		0.956 		0.0372 	0.0068 		7.666 		1.655 		2.174
9 	55 	53 	100.0 		0.0 		0.0 	2061 		0 		0 		1.0 		0.0 	0.0 		5.295 		1.001 		1.655
10 	23 	46 	68.87 		30.49 		0.63 	1414 		626 		13 		0.6887 		0.0063 	0.3049 		7.848 		4.069 		3.875
11 	18 	46 	75.7 		23.29 		1.01 	1567 		482 		21 		0.757 		0.0101 	0.2329 		7.75 		3.594 		3.462
12 	28 	46 	73.39 		25.3 		1.31 	1517 		523 		27 		0.7339 		0.0131 	0.253 		14.236 		4.96 		4.322
13 	1 	53 	98.54 		0.34 		1.12 	2022 		7 		23 		0.9854 		0.0112 	0.0034 		7.695 		3.014 		3.922
14 	44 	46 	95.47 		0.77 		3.76 	1982 		16 		78 		0.9547 		0.0376 	0.0077 		9.595 		2.11 		2.545
15 	54 	53 	98.16 		0.43 		1.4 	2032 		9 		29 		0.9816 		0.014 	0.0043 		7.013 		2.753 		3.052
16 	20 	46 	63.14 		27.06 		9.8 	322 		138 		50 		0.6314 		0.098 	0.2706 		20.945 		5.435 		5.05
17 	13 	53 	99.61 		0.34 		0.05 	2046 		7 		1 		0.9961 		0.0005 	0.0034 		7.522 		3.807 		3.891
18 	61 	53 	95.57 		0.14 		4.28 	1987 		3 		89 		0.9557 		0.0428 	0.0014 		10.705 		2.785 		3.082
19 	52 	46 	69.1 		29.93 		0.97 	1422 		616 		20 		0.691 		0.0097 	0.2993 		9.94 		4.514 		4.254
20 	60 	53 	97.76 		2.04 		0.19 	2012 		42 		4 		0.9776 		0.0019 	0.0204 		11.108 		4.577 		4.498
21 	51 	53 	97.57 		0.97 		1.46 	2007 		20 		30 		0.9757 		0.0146 	0.0097 		8.951 		3.966 		4.015
22 	24 	46 	75.01 		24.31 		0.68 	1543 		500 		14 		0.7501 		0.0068 	0.2431 		8.092 		3.871 		3.651
23 	10 	53 	99.61 		0.34 		0.05 	2045 		7 		1 		0.9961 		0.0005 	0.0034 		7.086 		3.17 		3.38
24 	49 	53 	100.0 		0.0 		0.0 	2056 		0 		0 		1.0 		0.0 	0.0 		5.269 		1.0 		1.655
25 	41 	46 	99.03 		0.48 		0.48 	2042 		10 		10 		0.9903 		0.0048 	0.0048 		8.157 		1.638 		2.171
26 	7 	53 	99.61 		0.34 		0.05 	2049 		7 		1 		0.9961 		0.0005 	0.0034 		7.602 		3.728 		3.828
27 	40 	46 	95.97 		0.83 		3.21 	1976 		17 		66 		0.9597 		0.0321 	0.0083 		8.656 		1.931 		2.382
28 	58 	53 	100.0 		0.0 		0.0 	2073 		0 		0 		1.0 		0.0 	0.0 		7.246 		1.572 		2.108
29 	9 	53 	98.89 		0.34 		0.77 	2043 		7 		16 		0.9889 		0.0077 	0.0034 		8.272 		3.765 		3.857
30 	26 	46 	70.2 		29.17 		0.63 	1449 		602 		13 		0.702 		0.0063 	0.2917 		7.631 		3.79 		3.76
31 	27 	46 	60.67 		30.84 		8.49 	1257 		639 		176 		0.6067 		0.0849 	0.3084 		16.879 		5.592 		4.886
32 	35 	46 	75.27 		23.29 		1.45 	1558 		482 		30 		0.7527 		0.0145 	0.2329 		11.337 		4.385 		4.254
33 	53 	53 	100.0 		0.0 		0.0 	2062 		0 		0 		1.0 		0.0 	0.0 		4.846 		2.0 		0.0
34 	22 	46 	76.44 		22.44 		1.12 	1574 		462 		23 		0.7644 		0.0112 	0.2244 		7.532 		3.256 		3.333
35 	56 	53 	97.42 		0.97 		1.6 	2004 		20 		33 		0.9742 		0.016 	0.0097 		8.218 		3.199 		3.405
36 	46 	46 	98.08 		0.67 		1.25 	2038 		14 		26 		0.9808 		0.0125 	0.0067 		6.672 		3.068 		0.0
37 	42 	46 	94.59 		0.72 		4.68 	1960 		15 		97 		0.9459 		0.0468 	0.0072 		10.877 		2.154 		2.575
38 	5 	53 	99.47 		0.49 		0.05 	2049 		10 		1 		0.9947 		0.0005 	0.0049 		10.291 		4.612 		4.525
39 	29 	53 	84.37 		11.19 		4.44 	1749 		232 		92 		0.8437 		0.0444 	0.1119 		14.326 		6.338 		5.904
40 	12 	53 	99.56 		0.34 		0.1 	2044 		7 		2 		0.9956 		0.001 	0.0034 		7.048 		3.032 		3.272
41 	19 	46 	64.56 		30.09 		5.36 	1326 		618 		110 		0.6456 		0.0536 	0.3009 		8.914 		4.461 		4.057
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Average------------------------------------------------------------------------------------------------------->	0.869 		0.02 	0.111 		9.552 		3.658 		3.574
No of nodes with 100% relaibilty---------------------------------------------------------------------------------------------->	26
Non-Victim nodes : [45, 57, 47, 15, 48, 55, 1, 44, 54, 13, 61, 60, 51, 10, 49, 41, 7, 40, 58, 9, 53, 56, 46, 42, 5, 12]
No of victim nodes ----------------------------------------------------------------------------------------------------------->	15
Victim nodes : [30, 16, 17, 23, 18, 28, 20, 52, 24, 26, 27, 35, 22, 29, 19]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CDF TABLE-NO CHANGE 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
0.999 	|	4 	|	[55, 49, 58, 53]
0.99 	|	10 	|	[55, 13, 10, 49, 41, 7, 58, 53, 5, 12]
0.985 	|	12 	|	[55, 1, 13, 10, 49, 41, 7, 58, 9, 53, 5, 12]
0.98 	|	14 	|	[55, 1, 54, 13, 10, 49, 41, 7, 58, 9, 53, 46, 5, 12]
0.975 	|	17 	|	[45, 55, 1, 54, 13, 60, 51, 10, 49, 41, 7, 58, 9, 53, 46, 5, 12]
0.9 	|	26 	|	[45, 57, 47, 15, 48, 55, 1, 44, 54, 13, 61, 60, 51, 10, 49, 41, 7, 40, 58, 9, 53, 56, 46, 42, 5, 12]
0.8 	|	27 	|	[45, 57, 47, 15, 48, 55, 1, 44, 54, 13, 61, 60, 51, 10, 49, 41, 7, 40, 58, 9, 53, 56, 46, 42, 5, 29, 12]
0.7 	|	33 	|	[45, 57, 47, 15, 48, 55, 18, 28, 1, 44, 54, 13, 61, 60, 51, 24, 10, 49, 41, 7, 40, 58, 9, 26, 35, 53, 22, 56, 46, 42, 5, 29, 12]
0.6 	|	40 	|	[45, 57, 47, 15, 16, 17, 48, 55, 23, 18, 28, 1, 44, 54, 20, 13, 61, 52, 60, 51, 24, 10, 49, 41, 7, 40, 58, 9, 26, 27, 35, 53, 22, 56, 46, 42, 5, 29, 12, 19]
0.5 	|	40 	|	[45, 57, 47, 15, 16, 17, 48, 55, 23, 18, 28, 1, 44, 54, 20, 13, 61, 52, 60, 51, 24, 10, 49, 41, 7, 40, 58, 9, 26, 27, 35, 53, 22, 56, 46, 42, 5, 29, 12, 19]
0.4 	|	40 	|	[45, 57, 47, 15, 16, 17, 48, 55, 23, 18, 28, 1, 44, 54, 20, 13, 61, 52, 60, 51, 24, 10, 49, 41, 7, 40, 58, 9, 26, 27, 35, 53, 22, 56, 46, 42, 5, 29, 12, 19]
0.3 	|	40 	|	[45, 57, 47, 15, 16, 17, 48, 55, 23, 18, 28, 1, 44, 54, 20, 13, 61, 52, 60, 51, 24, 10, 49, 41, 7, 40, 58, 9, 26, 27, 35, 53, 22, 56, 46, 42, 5, 29, 12, 19]
0.2 	|	41 	|	[45, 57, 47, 15, 30, 16, 17, 48, 55, 23, 18, 28, 1, 44, 54, 20, 13, 61, 52, 60, 51, 24, 10, 49, 41, 7, 40, 58, 9, 26, 27, 35, 53, 22, 56, 46, 42, 5, 29, 12, 19]
0.1 	|	41 	|	[45, 57, 47, 15, 30, 16, 17, 48, 55, 23, 18, 28, 1, 44, 54, 20, 13, 61, 52, 60, 51, 24, 10, 49, 41, 7, 40, 58, 9, 26, 27, 35, 53, 22, 56, 46, 42, 5, 29, 12, 19]
0.05 	|	41 	|	[45, 57, 47, 15, 30, 16, 17, 48, 55, 23, 18, 28, 1, 44, 54, 20, 13, 61, 52, 60, 51, 24, 10, 49, 41, 7, 40, 58, 9, 26, 27, 35, 53, 22, 56, 46, 42, 5, 29, 12, 19]
0 	|	41 	|	[45, 57, 47, 15, 30, 16, 17, 48, 55, 23, 18, 28, 1, 44, 54, 20, 13, 61, 52, 60, 51, 24, 10, 49, 41, 7, 40, 58, 9, 26, 27, 35, 53, 22, 56, 46, 42, 5, 29, 12, 19]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Node:	ZERO	ONE	TWO	THREE	FOUR	FIVE	
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
45 	46 	124 	222 	1691 	0 	0
45 	0 	0 	0 	0 	0 	0
57 	114 	48 	127 	1792 	0 	0
47 	94 	66 	107 	1728 	0 	0
47 	0 	0 	0 	0 	0 	0
15 	184 	343 	544 	1001 	0 	0
15 	0 	0 	0 	0 	0 	0
30 	1549 	44 	108 	390 	0 	0
30 	0 	0 	0 	0 	0 	0
16 	627 	39 	51 	1362 	0 	0
16 	0 	0 	0 	0 	0 	0
16 	0 	0 	0 	0 	0 	0
17 	627 	44 	61 	1346 	0 	0
17 	0 	0 	0 	0 	0 	0
48 	91 	71 	115 	1806 	0 	0
55 	0 	0 	0 	2075 	0 	0
23 	641 	32 	51 	1344 	0 	0
18 	504 	57 	82 	1442 	0 	0
28 	552 	144 	319 	1066 	0 	0
28 	0 	0 	0 	0 	0 	0
1 	34 	4 	5 	2023 	0 	0
44 	94 	99 	200 	1697 	0 	0
44 	0 	0 	0 	0 	0 	0
44 	0 	0 	0 	0 	0 	0
44 	0 	0 	0 	0 	0 	0
44 	0 	0 	0 	0 	0 	0
54 	47 	0 	17 	2021 	0 	0
54 	0 	0 	0 	0 	0 	0
20 	195 	66 	129 	135 	0 	0
20 	0 	0 	0 	0 	0 	0
13 	13 	1 	1 	2053 	0 	0
13 	0 	0 	0 	0 	0 	0
13 	0 	0 	0 	0 	0 	0
61 	92 	55 	276 	1670 	0 	0
52 	638 	50 	128 	1257 	0 	0
60 	54 	31 	176 	1811 	0 	0
51 	59 	36 	159 	1818 	0 	0
51 	0 	0 	0 	0 	0 	0
51 	0 	0 	0 	0 	0 	0
51 	0 	0 	0 	0 	0 	0
24 	515 	55 	94 	1407 	0 	0
24 	0 	0 	0 	0 	0 	0
10 	13 	2 	3 	2049 	0 	0
10 	0 	0 	0 	0 	0 	0
10 	0 	0 	0 	0 	0 	0
10 	0 	0 	0 	0 	0 	0
10 	0 	0 	0 	0 	0 	0
49 	0 	0 	0 	2071 	0 	0
41 	21 	88 	127 	1841 	0 	0
41 	0 	0 	0 	0 	0 	0
41 	0 	0 	0 	0 	0 	0
7 	16 	1 	13 	2042 	0 	0
7 	0 	0 	0 	0 	0 	0
40 	83 	75 	174 	1742 	0 	0
40 	0 	0 	0 	0 	0 	0
58 	0 	15 	81 	1992 	0 	0
9 	31 	7 	52 	1990 	0 	0
9 	0 	0 	0 	0 	0 	0
26 	616 	25 	24 	1413 	0 	0
27 	820 	237 	363 	667 	0 	0
35 	514 	58 	213 	1300 	0 	0
35 	0 	0 	0 	0 	0 	0
53 	0 	0 	2066 	10 	0 	0
53 	0 	0 	0 	0 	0 	0
53 	0 	0 	0 	0 	0 	0
53 	0 	0 	0 	0 	0 	0
53 	0 	0 	0 	0 	0 	0
53 	0 	0 	0 	0 	0 	0
22 	487 	42 	38 	1507 	0 	0
56 	53 	35 	94 	1890 	0 	0
56 	0 	0 	0 	0 	0 	0
46 	40 	187 	1855 	10 	0 	0
42 	113 	122 	273 	1579 	0 	0
5 	19 	10 	260 	1786 	0 	0
29 	328 	472 	458 	830 	0 	0
12 	14 	0 	7 	2047 	0 	0
19 	732 	74 	93 	1169 	0 	0
19 	0 	0 	0 	0 	0 	0
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
0.134 
0.168 
0.234 
1.0 
1.0 
1.0
