With Channel change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SNo	Node:	Init_Id	R_Self_init	R_other1	Nt recv	Num_Self_init	Num_other1	No_pkt_recv	Reliability	Damage	Intrusion	Radio-on	Relay-count	Latency
1 	1 	2 	98.94 		0.98 		0.08 	2330 		23 		2 		0.9894 		0.0008 	0.0098 		6.267 		1.01 		2.339
2 	10 	10 	71.86 		28.01 		0.13 	1693 		660 		3 		0.7186 		0.0013 	0.2801 		4.077 		2.004 		0.0
3 	11 	25 	100.0 		0.0 		0.0 	2344 		0 		0 		1.0 		0.0 	0.0 		5.359 		1.0 		1.705
4 	13 	25 	99.58 		0.42 		0.0 	2346 		10 		0 		0.9958 		0.0 	0.0042 		5.363 		1.014 		1.713
5 	15 	2 	57.88 		42.12 		0.0 	1359 		989 		0 		0.5788 		0.0 	0.4212 		5.575 		1.028 		3.932
6 	16 	22 	78.93 		21.07 		0.0 	1858 		496 		0 		0.7893 		0.0 	0.2107 		6.717 		2.431 		2.895
7 	17 	25 	98.6 		1.15 		0.25 	2323 		27 		6 		0.986 		0.0025 	0.0115 		5.821 		1.118 		1.779
8 	18 	22 	100.0 		0.0 		0.0 	2353 		0 		0 		1.0 		0.0 	0.0 		6.152 		1.0 		1.684
9 	19 	25 	98.73 		1.27 		0.0 	2326 		30 		0 		0.9873 		0.0 	0.0127 		6.152 		1.824 		2.362
10 	2 	2 	87.63 		11.43 		0.94 	2054 		268 		22 		0.8763 		0.0094 	0.1143 		4.588 		2.455 		0.0
11 	20 	10 	0.08 		99.92 		0.0 	2 		2354 		0 		0.0008 		0.0 	0.9992 		8.055 		3.0 		7.629
12 	22 	22 	99.96 		0.04 		0.0 	2355 		1 		0 		0.9996 		0.0 	0.0004 		3.876 		2.0 		0.0
13 	23 	10 	0.04 		99.96 		0.0 	1 		2355 		0 		0.0004 		0.0 	0.9996 		7.65 		1.0 		7.232
14 	24 	10 	0.0 		100.0 		0.0 	0 		2356 		0 		0.0 		0.0 	1.0 		0.0 		0.0 		0.0
15 	25 	25 	100.0 		0.0 		0.0 	2347 		0 		0 		1.0 		0.0 	0.0 		3.988 		2.028 		0.0
16 	26 	10 	0.26 		99.74 		0.0 	6 		2346 		0 		0.0026 		0.0 	0.9974 		5.675 		1.0 		3.646
17 	27 	22 	100.0 		0.0 		0.0 	2351 		0 		0 		1.0 		0.0 	0.0 		7.228 		1.018 		1.696
18 	28 	3 	0.17 		99.83 		0.0 	4 		2331 		0 		0.0017 		0.0 	0.9983 		5.362 		1.0 		4.524
19 	3 	3 	56.39 		43.23 		0.38 	1324 		1015 		9 		0.5639 		0.0038 	0.4323 		4.755 		2.167 		0.0
20 	31 	3 	99.66 		0.34 		0.0 	2348 		8 		0 		0.9966 		0.0 	0.0034 		5.779 		1.0 		1.676
21 	32 	3 	21.1 		78.9 		0.0 	497 		1859 		0 		0.211 		0.0 	0.789 		5.721 		1.002 		3.798
22 	33 	3 	99.87 		0.13 		0.0 	2341 		3 		0 		0.9987 		0.0 	0.0013 		5.595 		1.009 		1.685
23 	4 	2 	98.34 		1.53 		0.13 	2317 		36 		3 		0.9834 		0.0013 	0.0153 		6.877 		1.06 		1.735
24 	6 	22 	79.12 		20.88 		0.0 	1864 		492 		0 		0.7912 		0.0 	0.2088 		6.157 		1.887 		2.421
25 	8 	2 	98.9 		0.98 		0.13 	2330 		23 		3 		0.989 		0.0013 	0.0098 		5.578 		1.002 		1.677
56.1256200019 138.365845532
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Average------------------------------------------------------------------------------------------------------->	0.698 		0.001 	0.301 		5.765 		1.461 		2.339
No of nodes with 100% relaibilty---------------------------------------------------------------------------------------------->	13
Non-Victim nodes : [1, 11, 13, 17, 18, 19, 22, 25, 27, 31, 33, 4, 8]
No of victim nodes ----------------------------------------------------------------------------------------------------------->	12
Victim nodes : [10, 15, 16, 2, 20, 23, 24, 26, 28, 3, 32, 6]
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CDF TABLE-NO CHANGE 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
0.999 	|	5 	|	[11, 18, 22, 25, 27]
0.99 	|	8 	|	[11, 13, 18, 22, 25, 27, 31, 33]
0.985 	|	12 	|	[1, 11, 13, 17, 18, 19, 22, 25, 27, 31, 33, 8]
0.98 	|	13 	|	[1, 11, 13, 17, 18, 19, 22, 25, 27, 31, 33, 4, 8]
0.975 	|	13 	|	[1, 11, 13, 17, 18, 19, 22, 25, 27, 31, 33, 4, 8]
0.9 	|	13 	|	[1, 11, 13, 17, 18, 19, 22, 25, 27, 31, 33, 4, 8]
0.8 	|	14 	|	[1, 11, 13, 17, 18, 19, 2, 22, 25, 27, 31, 33, 4, 8]
0.7 	|	17 	|	[1, 10, 11, 13, 16, 17, 18, 19, 2, 22, 25, 27, 31, 33, 4, 6, 8]
0.6 	|	17 	|	[1, 10, 11, 13, 16, 17, 18, 19, 2, 22, 25, 27, 31, 33, 4, 6, 8]
0.5 	|	19 	|	[1, 10, 11, 13, 15, 16, 17, 18, 19, 2, 22, 25, 27, 3, 31, 33, 4, 6, 8]
0.4 	|	19 	|	[1, 10, 11, 13, 15, 16, 17, 18, 19, 2, 22, 25, 27, 3, 31, 33, 4, 6, 8]
0.3 	|	19 	|	[1, 10, 11, 13, 15, 16, 17, 18, 19, 2, 22, 25, 27, 3, 31, 33, 4, 6, 8]
0.2 	|	20 	|	[1, 10, 11, 13, 15, 16, 17, 18, 19, 2, 22, 25, 27, 3, 31, 32, 33, 4, 6, 8]
0.1 	|	20 	|	[1, 10, 11, 13, 15, 16, 17, 18, 19, 2, 22, 25, 27, 3, 31, 32, 33, 4, 6, 8]
0.05 	|	20 	|	[1, 10, 11, 13, 15, 16, 17, 18, 19, 2, 22, 25, 27, 3, 31, 32, 33, 4, 6, 8]
0 	|	25 	|	[1, 10, 11, 13, 15, 16, 17, 18, 19, 2, 20, 22, 23, 24, 25, 26, 27, 28, 3, 31, 32, 33, 4, 6, 8]
