import sys
import os
import csv
import math  
import numpy as np
from os import walk
from fileinput import close

def analyze():
	group1=[4,1]
	group2=[5,2]
	group=[1,2,5,4]
	group1_initiator=group1[0]
	group2_initiator=group2[0]
	count=0

	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	print "SNo\t" "Node:\t""Init\t""R_Self_init\t""Corrupt\t\t""Nt_recv\t\t""other""\t\t""Self\t\t""Corrupt""\t\t""Nt_recv""\t\t""other""\t""Average RSSI""\tFreq\tPower"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	avg_relaibility_NC=0.0
	avg_damage_NC=0.0
	avg_intrusion_NC=0.0	
	relaibility_total=0.0
	damage_total=0.0
	intrusion_total=0.0
	#Average calculation
	count=0
	f=[]
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-multigrp-test/Data/Async_Sync_Experiment/Async/P_31_31/D_5'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-multigrp-test/Data/Async_Sync_Experiment/Async/P_31_31/D_5',name)
			f = open(fullName, 'r')
			rx_cnt=0
			self_initiator=0
			corrupt=0
			other_initiator=0
			no_rcv_cnt=0
			percent_self_initiator=0.0
			percent_other_initiator=0.0
			percent_initiator_nt_rcv=0.0
			percent_corrupt=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			recieved_less=0
			recieved_n_tx=0
			average_rssi=0.0
			rssi_list=[]
			corrupt_factor=0.0
	
			count=count+1
			for line in f:
				line = line.strip()
				columns = line.split()
				try:
					#print columns
					frequency=int(columns[18])
					#power=int(columns[18])
					for j in sorted(group1):
						if(j==int(columns[1])):
							group_ini=group1_initiator
					for j in sorted(group2):
						if(j==int(columns[1])):
							group_ini=group2_initiator
                                        if(int(columns[14])==group_ini and int(columns[13])==2):
						self_initiator=self_initiator+1;
						rssi_list.append(int(columns[15]))
					elif(int(columns[14])==0 and int(columns[13])==1):
						corrupt=corrupt+1;
						if(int(columns[16])!=0):
							rssi_list.append(int(columns[15]))
					elif(int(columns[14])==0 and int(columns[13])==0):
						no_rcv_cnt=no_rcv_cnt+1;
					else:
						# print group_ini,columns
						other_initiator=other_initiator+1;
				except (IndexError,ValueError):
					gotdata = 'null'
			if(self_initiator!=0 or other_initiator!=0 or no_rcv_cnt!=0 or corrupt!=0):
				#print self_initiator,other_initiator,no_rcv_cnt,corrupt
				total=float(self_initiator+other_initiator+no_rcv_cnt+corrupt)
				percent_self_initiator=float((self_initiator/total)*100)
        	                percent_other_initiator=float((other_initiator/total)*100)
       		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
				percent_corrupt=float((corrupt/total)*100)
				relaibility=self_initiator/total;
				damage_factor=no_rcv_cnt/total;
				intrusion=float(other_initiator)
				intrusion_factor=intrusion/total
				corrupt_factor=corrupt/total
				average_rssi=np.mean(rssi_list)	
				#print 	rssi_list	
			print count,"\t",columns[1],"\t",group_ini,"\t",self_initiator,"\t\t",corrupt,"\t\t",no_rcv_cnt,"\t\t",other_initiator,"\t\t",round(relaibility,4),"\t\t",round(corrupt_factor,4),"\t\t",round(damage_factor,4),"\t\t",round(intrusion_factor,4),"\t",round(average_rssi,4),"\t",frequency#,"\t",power
			self_initiator=0
			other_initiator=0
			no_rcv_cnt=0
			corrupt=0
			percent_self_initiator=0.0
			percent_other_initiator=0.0
			percent_initiator_nt_rcv=0.0
			percent_corrupt=0.0
			relaibility=0.0
			damage_factor=0.0
			corrupt_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			average_rssi=0.0
			rssi_list=[]
def main():

    if len(sys.argv) != 1:
        print('...')
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()

