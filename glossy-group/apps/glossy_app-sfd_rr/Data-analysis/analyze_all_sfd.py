import sys
import os
import csv
import math  
import numpy as np
from os import walk
from fileinput import close

def analyze():
	f=[]
	sfd_list=[]
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/apps/glossy_app-sfd_rr/Data/SFD_result_1'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/apps/glossy_app-sfd_rr/Data/SFD_result_1',name)
			f = open(fullName, 'r')
			for line in f:
				line = line.strip()
				columns = line.split()
				try:
					#print columns
					sfd_list.append(columns[21])		
				except (IndexError,ValueError):
					gotdata = 'null'
		sfd_set=set(sfd_list)
		print sfd_set,len(sfd_set)
			
	f=[]
	for j in sorted(sfd_set):
		f = open(fullName, 'r')
		radio_on=[]
		latency=[]
		hop_cont=[]
		reliability=[]
		rx=[]
		avg_latency=[]
		avg_rx=[]
		avg_hop=[]
		avg_radio=[]
                for line in f:
                	line = line.strip()
                        columns = line.split()
                        try:
                              	if(columns[21]==j):
					latency.append(float(columns[9]))
					rx.append(int(columns[11]))
					hop_cont.append(int(columns[19]))
					radio_on.append(float(columns[24]))
					if(int(columns[11])>0):
						reliability.append(1)
					else:
						reliability.append(0)
						print columns
                        except (IndexError,ValueError):
                        	gotdata = 'null'  
		avg_latency=np.mean(latency) 
		avg_rx=np.mean(rx) 
		avg_hop=np.mean(hop_cont)   
		avg_radio=np.mean(radio_on)
		avg_reliability=np.mean(reliability) 
		print(reliability)                  
		print j ,"\t",round(avg_latency,2),"\t",round(avg_radio,2),"\t",round(avg_hop,2),"\t",round(avg_rx,2),"\t",round(avg_reliability,2)
			
def main():

    if len(sys.argv) != 1:
        print('...')
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()

