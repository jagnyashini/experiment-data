import sys
import os
import csv

from os import walk
from fileinput import close

def analyze():
	nodes=[]	
	nodes_set=[]
	f=[]
	print "Node-Id" ,"\t","Number of Iteration","\t" ,"Number of CRC mismatch with Neighbour Trasmitting","\t","Number of CRC mismatch with Neighbour Not Trasmitting"
	print "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	"
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-analysis-test/Data/flocklab/Neighbour_Map/'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-analysis-test/Data/flocklab/Neighbour_Map/',name)
			f = open(fullName, 'r')
			for line in f:
				line = line.strip()
				columns = line.split()
				#print columns
				try:
					nodes.append(int(columns[0]))
				except (IndexError,ValueError):
					gotdata = 'null'
			f.close()
        		nodes_set = set(nodes)
			#print sorted(nodes_set)
	
	for node in sorted(nodes_set):
		count_error_with_neighbour_transmit=0
		count_with_no_neighbour_transmit=0
		nodes_neighbour=[]	
		nodes_neighbour_set=[]
		f_neighbour=[]
		f=[]
		for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-analysis-test/Data/flocklab/Neighbour_Map/'):
			f_neighbour.extend(filenames)
			for name in f_neighbour:
				fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-analysis-test/Data/flocklab/Neighbour_Map/',name)
				f_neighbour = open(fullName, 'r')
				for line in f_neighbour:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
						if(node==int(columns[0])):
							nodes_neighbour.append(int(columns[1]))
					except (IndexError,ValueError):
						gotdata = 'null'
				f_neighbour.close()
        			nodes_neighbour_set = set(nodes_neighbour)
				number_of_neigbours=len(nodes_neighbour_set)
		for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-analysis-test/Data/flocklab/79390'):
			f.extend(filenames)
			for name in f:
				fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-analysis-test/Data/flocklab/79390',name)
				f = open(fullName, 'r')
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
						if(int(columns[1])==node):	
							#zero=0
							#print int(columns[8]),zero
							#print int(columns[1]),node,int(columns[8])
							iteration=int(columns[3])
							if(int(columns[12])>0):
								seq_no=int(columns[3])
								time_slot=int(columns[15])
								#print "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n"
								#print (columns)
								#print "Node-id :", node,"\t","Neighbors:",sorted(nodes_neighbour_set),"\t","SeqNo:",seq_no,"\t","T-Slots:",time_slot
								#print "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n"
								w, h = time_slot+2, number_of_neigbours+1;
								Matrix = [[0 for x in range(w)] for y in range(h)] 
								Matrix[0][0]=node #stores the node_id
								Matrix[0][1]="-\t" #stores the crc_error_count
								start_index=19    #File index starts at 15 column to extract results of timeslot
								end_index=start_index+time_slot
								index=start_index
								target_index=2     #Matrix index starts at 2 to store results of timeslot
								while(index<end_index):
									Matrix[0][target_index]=(columns[index])
									target_index=target_index+1	
									index=index+1
								i=0
								while(i<w):
									#print Matrix[0][i], 
									i=i+1
								#print "\n"
								row_index=0
								for neighbour in sorted(nodes_neighbour_set):
									row_index=row_index+1
									for (dirpath1, dirnames1, filenames1) in walk('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-analysis-test/Data/flocklab/79390'):
										f_neighbour=[]
										f_neighbour.extend(filenames1)
										for name1 in f_neighbour:
											fullName1 = os.path.join('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-analysis-test/Data/flocklab/79390',name1)
											#print fullName1											
											f_neighbour = open(fullName1, 'r')
											for line in f_neighbour:
												line = line.strip()
												columns1 = line.split()
												try:
													#print columns1
													#print int(columns1[1]),neighbour,int(columns1[3]),seq_no
													if(int(columns1[1])==neighbour and int(columns1[3])== seq_no):
														#print (columns1)
														start_index=19    #File index starts at 15 column to extract results of timeslot
														end_index=start_index+time_slot
														index=start_index
														target_index=2     #Matrix index starts at 2 to store results of timeslot
														while(index<end_index):
															Matrix[row_index][target_index]=(columns1[index])
															Matrix[row_index][0]=neighbour
															Matrix[row_index][1]="-\t"
															target_index=target_index+1	
															index=index+1
												except (IndexError,ValueError):
													gotdata = 'null'
											f_neighbour.close()
									i=0
									while(i<w):
										#print Matrix[row_index][i], 
										i=i+1
									#print "\n"
								#For analysis of the Matrix
								j=2
								while(j<w):
									#print Matrix[0][j]
									temp=Matrix[0][j]
									#print temp,Matrix[0][j]
									if(Matrix[0][j]=="3"):
										error_index=j
										i=1
										flag=0
										while(i<h):
											if(Matrix[i][j]=="1"):
												flag=1
											i=i+1
										if(flag==1):
											count_error_with_neighbour_transmit=count_error_with_neighbour_transmit+1
											#print "count_error_with_neighbour_transmit" ,count_error_with_neighbour_transmit
										else:				
											count_with_no_neighbour_transmit=count_with_no_neighbour_transmit+1
									j=j+1
					
				
							
					except (IndexError,ValueError):
						gotdata = 'null'
				f.close()
		print node ,"\t\t\t",iteration,"\t\t\t\t" ,count_error_with_neighbour_transmit,"\t\t\t\t\t\t\t",count_with_no_neighbour_transmit
def main():

    if len(sys.argv) != 1:
        print('...')
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()

