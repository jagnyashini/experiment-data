import sys
import os
import csv

from os import walk
from fileinput import close

def analyze():
	print "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "NodeId","\t\t\t\t\t\t","CRC Mismatch","\t\t\t\t\t\t","Timeout","\t\t\t\t\t\t","Rx_Cnt"
	print "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "\t\t\t\t","Zero","\t","One","\t","Two","\t","Three","\t","Four","\t","Five","\t\t","Zero","\t","One","\t","Two","\t","Three","\t","Four","\t","Five","\t\t","Zero","\t","One","\t","Two","\t","Three","\t","Four","\t","Five"
	print "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	f=[]
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-analysis-test/Data/RX_CNT_result_1'):
		f.extend(filenames)
		for name in f:
			crc = [0,0,0,0,0,0,0]
			timeout = [0,0,0,0,0,0,0]
			rx_cnt=[0,0,0,0,0,0,0]
			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-analysis-test/Data/RX_CNT_result_1',name)
			#print fullName
#			node_neighbor_list = []
			f = open(fullName, 'r')
			i=0
			for line in f:
				line = line.strip()
				columns = line.split()
				if(i>7):
					try:
						#print(columns)
						crc_index=int(columns[4])
						crc[crc_index]+=1
						timeout_index=int(columns[3])
						timeout[timeout_index]+=1
						rx_cnt_index=int(columns[5])
						rx_cnt[rx_cnt_index]+=1
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1;
			print columns[0],"\t",crc[0],"\t",crc[1],"\t",crc[2],"\t",crc[3],"\t",crc[4],"\t",crc[5],"\t\t",timeout[0],"\t",timeout[1],"\t",timeout[2],"\t",timeout[3],"\t",timeout[4],"\t",timeout[5],"\t","\t",rx_cnt[0],"\t",rx_cnt[1],"\t",rx_cnt[2],"\t",rx_cnt[3],"\t",rx_cnt[4],"\t",rx_cnt[5]
			f.close()
def main():

    if len(sys.argv) != 1:
        print('...')
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()

