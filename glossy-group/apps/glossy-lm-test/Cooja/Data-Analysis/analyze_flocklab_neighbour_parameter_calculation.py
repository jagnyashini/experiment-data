
import sys
import os
import csv
import numpy as np
from os import walk
from fileinput import close
create_nearest_neighbour_lst=0	
def analyze():
	node_count=0
	parent_list = []
	print "Nodeid", "\t","Neighbour","\t","RSSI","\t","LQI","\t","Pkt_snt","\t","pkt_rcv","\t","prr"
	f = []
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/glossy-dynamic-group/Data/Cooja/neighbour_discover_80_Nodes'):

		f.extend(filenames)
		for name in f:

			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/glossy-dynamic-group/Data/Cooja/neighbour_discover_80_Nodes',name)
			node_list = []
			f = open(fullName, 'r')
			line_no=0;

			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				try:
					#print columns
					#print(columns[8])
					node_list.append(int(columns[3]))
				except (IndexError,ValueError):
					gotdata = 'null'
 			f.close()
			node_set = set(node_list)
			#print len(node_set)
			#print sorted(node_set)
			node_neighbor_list=[]
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				for line in f:
					line = line.strip()
					columns = line.split()
					try:
						if int(columns[3]) == i and int(columns[16])!=0 :
							node_neighbor_list.append(int(columns[16]))
					except (IndexError,ValueError):
						gotdata = 'null'
	 			f.close()
				node_neighbor_set = set(node_neighbor_list)
				#print i,":"
				#print sorted(node_neighbor_set)	
				for j in sorted(node_neighbor_set):
					rssi=[]
					packet_received=[]
					packet_sent=[]
					lqi=[]
					average_rssi=0.0
					average_lqi=0.0	
					average_prr=0.0
					f = open(fullName, 'r')
					for line in f:
						line = line.strip()
						columns = line.split()
						try:			
							if(i==int(columns[3]) and j==int(columns[16])):
								#print columns
								if(int(columns[15])==2):
									#print columns
									packet_received.append(int(columns[16]))
								rssi.append(int(columns[17]))
								lqi.append(int(columns[18]))
							if(j==int(columns[3])):
								packet_sent=int(columns[5])
						except (IndexError,ValueError):
							gotdata = 'null'
					f.close()
					average_rssi=np.mean(rssi)
					average_lqi=np.mean(lqi)
					average_prr=(float(len(packet_received))/float(packet_sent))
					#print packet_received
					if(average_prr!=0.0):
						print i,"\t",j,"\t",round(average_rssi,2),"\t",round(average_lqi,2),"\t",round(average_prr,2),"\t",len(packet_received),"\t",(packet_sent)
						#print i,"\t",j,"\t",round(average_prr,2)	



def main():

    if len(sys.argv) != 1:
        print '...'
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()


