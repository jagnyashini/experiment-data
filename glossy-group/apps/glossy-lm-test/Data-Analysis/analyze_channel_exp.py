import sys
import os
import csv
import math  

from os import walk
from fileinput import close

def analyze():

	
	group1=[2,4]
	group2=[5,1]
	group=[1,2,5,4]
	group1_initiator=group1[0]
	group2_initiator=group2[0]

	count=0

	
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "With No Change"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	print "SNo\t" "Node:\t""Init_Id\t\t""R_Self_init\t""R_other1\t""Nt recv\t\t""Num_Self_init\t""Num_other1\t""No_pkt_recv""\t""Reliability""\t""Damage""\t\t""Intrusion"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	avg_relaibility_NC=0.0
	avg_damage_NC=0.0
	avg_intrusion_NC=0.0	
	relaibility_total=0.0
	damage_total=0.0
	intrusion_total=0.0
	#Average calculation
	count=0
	f=[]
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-lm-test/4Nov2019/Multiple_SFD/D_0'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/apps/glossy-lm-test/4Nov2019/Multiple_SFD/D_0',name)
			f = open(fullName, 'r')
			rx_cnt=0
			self_initiator=0
			other_initiator=0
			no_rcv_cnt=0
			percent_self_initiator=0.0
			percent_other_initiator=0.0
			percent_initiator_nt_rcv=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			recieved_less=0
			recieved_n_tx=0
			count=count+1
			for line in f:
				line = line.strip()
				columns = line.split()
				try:
					#print columns[10]
					for j in sorted(group1):
						if(j==int(columns[1])):
							group_ini=group1_initiator
							other_ini=group2_initiator
					for j in sorted(group2):
						if(j==int(columns[1])):
							group_ini=group2_initiator
							other_ini=group1_initiator
                                        if(int(columns[10])==group_ini):
						self_initiator=self_initiator+1;
					elif(int(columns[10])==0 or int(columns[10])!=other_ini):
						no_rcv_cnt=no_rcv_cnt+1;
					else:
						other_initiator=other_initiator+1;
				except (IndexError,ValueError):
					gotdata = 'null'
			if(self_initiator!=0 or other_initiator!=0 or no_rcv_cnt!=0):
				total=float(self_initiator+other_initiator+no_rcv_cnt)
				percent_self_initiator=float((self_initiator/total)*100)
        	                percent_other_initiator=float((other_initiator/total)*100)
       		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
				relaibility=self_initiator/total;
				damage_factor=no_rcv_cnt/total;
				intrusion=float(other_initiator)
				intrusion_factor=intrusion/total
				relaibility_total=relaibility_total+relaibility
				damage_total=damage_total+damage_factor
				intrusion_total=intrusion_total+intrusion_factor			
			print count,"\t",columns[1],"\t",group_ini,"\t\t",round(percent_self_initiator,2),"\t\t",round(percent_other_initiator,2),"\t\t",round(percent_initiator_nt_rcv,2),"\t\t",self_initiator,"\t\t",other_initiator,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,4),"\t\t",round(damage_factor,4),"\t\t",round(intrusion_factor,4)
			self_initiator=0
			other_initiator=0
			no_rcv_cnt=0
			percent_self_initiator=0.0
			percent_other_initiator=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
	avg_relaibility_NC=relaibility_total/count
	avg_damage_NC=damage_total/count
	avg_intrusion_NC=intrusion_total/count
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#print round(relaibility_total,3),round(damage_total,3),round(intrusion_total,3),count
	print "Average----------------------------------------------------------------------------------------------------------------------->\t",round(avg_relaibility_NC,3),"\t\t",round(avg_damage_NC,3),"\t\t",round(avg_intrusion_NC,3)

def main():

    if len(sys.argv) != 1:
        print('...')
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()

