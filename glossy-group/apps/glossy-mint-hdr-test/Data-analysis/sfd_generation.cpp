#include <bits/stdc++.h>
using namespace std;
#define SYMBOL 1
#define BYTE 0

#if BYTE
#define SIZE_ARR 60000
#define MAX_VAL 255
#define MIN_VAL 1
#define HAMMING_DISTANCE 2
#endif

#if SYMBOL
#define SIZE_ARR 15
#define MAX_VAL 15
#define MIN_VAL 1
#define HAMMING_DISTANCE 1
#define HAMMING_DISTANCE_1 1
#endif

void decToBinary(int n)
{
    // array to store binary number
    int binaryNum[32];

    // counter for binary array
    int i = 0;
    while (n > 0) {

        // storing remainder in binary array
        binaryNum[i] = n % 2;
        n = n / 2;
        i++;
    }

    // printing binary array in reverse order
    for (int j = i - 1; j >= 0; j--)
        cout << binaryNum[j];
}
void decToHexa(int n)
{
    // char array to store hexadecimal number
    char hexaDeciNum[100];

    // counter for hexadecimal number array
    int i = 0;
    while(n!=0)
    {
        // temporary variable to store remainder
        int temp  = 0;

        // storing remainder in temp variable.
        temp = n % 16;

        // check if temp < 10
        if(temp < 10)
        {
            hexaDeciNum[i] = temp + 48;
            i++;
        }
        else
        {
            hexaDeciNum[i] = temp + 55;
            i++;
        }

        n = n/16;
    }

    // printing hexadecimal number array in reverse order
    for(int j=i-1; j>=0; j--)
        cout << hexaDeciNum[j];
}

// Function to calculate hamming distance
int hammingDistance(int n1, int n2)
{
    int x = n1 ^ n2;
    int setBits = 0;

    while (x > 0) {
        setBits += x & 1;
        x >>= 1;
    }

    return setBits;
}

// Driver code

int main()
{
    int hex_value_1[SIZE_ARR][2];
    int row=0;
    int flag;
    int number_entries_1=0;
    for(int i=MIN_VAL;i<MAX_VAL;i++)
	{
	  for(int j=MIN_VAL;j<MAX_VAL;j++)
		{
		    if(i!=15 && i!=31 && i!=47 && i!=63 && i!=79 && i!=95 && i!=111 && i!=127 && i!=143 && i!=159 && i!=175 && i!=191  && i!=207 && i!=223 && i!=239 && i!=255 && i<240 &&
		    		j!=15 && j!=31 && j!=47 && j!=63 && j!=79 && j!=95 && j!=111 && j!=127 && j!=143 && j!=159 && j!=175 && j!=191  && j!=207 && j!=223 && j!=239 && j!=255 && j<240 )
		    {
			flag=0;
			if(hammingDistance(i, j)==HAMMING_DISTANCE)
				{
				   for(int k=0;k<number_entries_1;k++)
					{
						if(hex_value_1[k][0]==j && hex_value_1[k][1]==i )
							{
								flag=1;
								break;
							}
					}
				   if(flag==0)
					{
						number_entries_1++;
				   		hex_value_1[row][0]=i;
			           	hex_value_1[row][1]=j;
			           	row++;
					}
				}
		    }
			if(row==SIZE_ARR)
				{
					break;
				}

		}
	 if(row==SIZE_ARR)
		{
			break;
		}
	}


#if SYMBOL
int hex_value2[SIZE_ARR][2];
row=0;
int number_entries_2=0;

for(int i=MAX_VAL;i>MIN_VAL;i--)
	{
	  for(int j=MAX_VAL;j>MIN_VAL;j--)
		{
		  if(i!=15 && i!=31 && i!=47 && i!=63 && i!=79 && i!=95 && i!=111 && i!=127 && i!=143 && i!=159 && i!=175 && i!=191  && i!=207 && i!=223 && i!=239 && i!=255 && i<240 &&
		  		    		j!=15 && j!=31 && j!=47 && j!=63 && j!=79 && j!=95 && j!=111 && j!=127 && j!=143 && j!=159 && j!=175 && j!=191  && j!=207 && j!=223 && j!=239 && j!=255 && j<240 )
		  {
			flag=0;
			if(hammingDistance(i, j)==HAMMING_DISTANCE_1)
				{
				   for(int k=0;k<SIZE_ARR;k++)
					{
						if(hex_value2[k][0]==j && hex_value2[k][1]==i )
							{
								flag=1;
								break;
							}
					}
				   if(flag==0)
					{
						number_entries_2++;
				   		hex_value2[row][0]=i;
			           		hex_value2[row][1]=j;
			           		row++;
					}
				}
		   }
			if(row==SIZE_ARR)
				{
					break;
				}

		}
	 if(row==SIZE_ARR)
		{
			break;
		}
	}
#endif

#if BYTE
//     for(int i=0;i<SIZE_ARR;i++)
//	{
////		cout<<hex_value[i][0]<<","<<hex_value[i][1]<<"\n";
//		//cout << "Hamming distance between ::  ";
//		printf("Hamming distance between 0x%x0F", hex_value[i][0]);
//    		//decToHexa();
//    		cout<<" and ";
//		printf("0x%x0F",hex_value[i][1]);
//    		cout<<" is " <<hammingDistance(hex_value[i][0], hex_value[i][1]) << endl;
//	}
    printf("\n------------------------------------------------------------------\n");
	printf("BYTE:MIN HAMMING DISTANCE=%d  NUMBER OF ENTRIES=%d",HAMMING_DISTANCE,number_entries_1);
	printf("\n------------------------------------------------------------------\n");

		printf("{");
    for(int i=0;i<number_entries_1;i++)
	{
//		cout<<hex_value[i][0]<<","<<hex_value[i][1]<<"\n";
		//cout << "Hamming distance between ::  ";
    	if(hex_value_1[i][0]<16)
    	{
		printf("{0x0%X0F,", hex_value_1[i][0]);
    	}
    	else
    	{
    		printf("{0x%X0F,", hex_value_1[i][0]);

    	}
    		//decToHexa();
    	if(hex_value_1[i][1]<16)
    	{
		printf("0x0%X0F},\n",hex_value_1[i][1]);
    	}
    	else
    	{
    		printf("0x%X0F},\n",hex_value_1[i][1]);

    	}
    		//cout<<" is " <<hammingDistance(hex_value[i][0], hex_value[i][1]) << endl;
	}
    printf("}\n");
#endif
    #if SYMBOL
 //for(int i=0;i<SIZE_ARR;i++)
//      {
////            cout<<hex_value[i][0]<<","<<hex_value[i][1]<<"\n";
//              //cout << "Hamming distance between ::  ";
//              printf("Hamming distance between 0x%x0F", hex_value[i][0]);
//              //decToHexa();
//              cout<<" and ";
//              printf("0x%x0F",hex_value[i][1]);
//              cout<<" is " <<hammingDistance(hex_value[i][0], hex_value[i][1]) << endl;
//      }

                printf("{");

    for(int i=0;i<number_entries_1;i++)
        {
    	for(int j=0;j<number_entries_2;j++)
//              cout<<hex_value[i][0]<<","<<hex_value[i][1]<<"\n";
                //cout << "Hamming distance between ::  ";
    	{
                printf("{0x%X%X0F,", hex_value_1[i][0],hex_value2[j][0]);
                //decToHexa();
                printf("0x%X%X0F},\n",hex_value_1[i][1],hex_value2[j][1]);
                //cout<<" is " <<hammingDistance(hex_value[i][0], hex_value[i][1]) << endl;
                printf("{0x%X%X0F,", hex_value_1[i][0],hex_value2[j][1]);
                             //decToHexa();
                printf("0x%X%X0F},\n",hex_value_1[i][1],hex_value2[j][0]);
                             //cout<<" is " <<hammingDistance(hex_value[i][0], hex_value[i][1]) << endl;
    	}
        }
    for(int i=0;i<number_entries_2;i++)
           {
       	for(int j=0;j<number_entries_1;j++)
   //              cout<<hex_value[i][0]<<","<<hex_value[i][1]<<"\n";
                   //cout << "Hamming distance between ::  ";
       	{
                   printf("{0x%X%X0F,", hex_value2[i][0],hex_value_1[j][0]);
                   //decToHexa();
                   printf("0x%X%X0F},\n",hex_value2[i][1],hex_value_1[j][1]);
                   //cout<<" is " <<hammingDistance(hex_value[i][0], hex_value[i][1]) << endl;
                   printf("{0x%X%X0F,", hex_value2[i][0],hex_value_1[j][1]);
                                //decToHexa();
                   printf("0x%X%X0F},\n",hex_value2[i][1],hex_value_1[j][0]);
       	}
                                //cout<<" is " <<hammingDistance(hex_value[i][0], hex_value[i][1]) << endl;
           }
    printf("}\n");


#endif
    return 0;
}
