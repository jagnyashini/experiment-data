import sys
import os
import csv
import math  


from os import walk
from fileinput import close

def analyze():

	node_count=0
	parent_list = []
	node_list_1=[]
	node_list_2=[]
	node_list_3=[]
	#group1=[2,12,7,10,9,29,34,28,30,20,17,26,18,16,51,57,61,53,62,65,46,45,48,47]
	#group2=[11,14,5,1,13,6,15,27,32,37,35,19,22,24,52,60,56,54,55,74,43,42,44,40]
	#group1=[44,38,47,46,48,18,24,19,52,56,51,54,55,30,28,35,37,15,5,13,10,14,1]
	#group2=[65,45,42,40,62,16,17,26,57,60,61,53,49,27,32,34,29,6,2,12,7,11,9]
	#group1=[56,57,60,51,54,61,55,53,62,74,45,38,42,40,6,15,29,5,2,14,11,13,12,7,1,9,10]
	#group2=[17,20,22,18,24,16,52,65,43,44,46,48,27,37,28,35,30,32,34]
	group1=[60,19,26,20,51,61,1,7,10,6,12,13,2,11,14,5,15,27,29,34,32,28,30,35,37]
	group2=[57,52,16,17,22,18,24,49,53,55,54,9,46,38,42,48,44,40,47,45,43,74,62,61,56]	
	#group1=[30,28,34,27,15,13,11,14,1,20,17,22,18,10,51,52,56,55,46,43,42,48,45,40]
	#group2=[35,32,29,6,5,2,12,7,9,37,19,16,24,54,60,57,61,53,62,38,65,74,44,47]
	group1_initiator=group1[0]
	group2_initiator=group2[0]
	count_node_1=0
	count_node_2=0
	count_node_both=0
	group_ini=0
	f = []
	f2 = []
	f3=  []
	f4=[]	
	f5=[]
	f6=[]			
	count=0
	ten=0
	not_ten=0
	nine=0
	eight=0
	seven=0
	six=0
	five=0
	four=0
	three=0
	two=0
	relaibility_total=0.0
	damage_total=0.0
	intrusion_total=0.0
	relaibility_d=0.0
	damage_d=0.0
	intrusion_d=0.0
	relaibility_sd_H=0.0
	damage_sd_H=0.0
	intrusion_sd_H=0.0
	relaibility_sum=0.0
	damage_sum=0.0
	intrusion_sum=0.0

	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "With Header Change"
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	
	count=0
	print "SNo\t" "Node:\t""Init_Id\t\t""R_Self_init\t""R_other1\t""Nt recv\t\t""Num_Self_init\t""Num_other1\t""No_pkt_recv""\t""Reliability""\t""Damage""\t\t""Intrusion"

#Average calculation

	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-hdr-test/Data/H_1_60_57'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-hdr-test/Data/H_1_60_57',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:
						if int(columns[13])==group1_initiator:							 
                                                	initiator1=initiator1+1;
						elif int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
				total=float(initiator1+initiator2+no_rcv_cnt)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
       		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
				if(group_ini==group1_initiator):
					count=count+1
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2)
				if(group_ini==group2_initiator):
					count=count+1
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1)
				intrusion_factor=intrusion/total
				relaibility_total=relaibility_total+relaibility
				damage_total=damage_total+damage_factor
				intrusion_total=intrusion_total+intrusion_factor
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
	avg_relaibility_H=relaibility_total/count
	avg_damage_H=damage_total/count
	avg_intrusion_H=intrusion_total/count

#Average Calculation ends here
	count=0
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-hdr-test/Data/H_1_60_57'):
		f5.extend(filenames)
		for name in f5:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-hdr-test/Data/H_1_60_57',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>5):
					try:
						#print(columns)
						#print(columns),group1_initiator,group2_initiator
						
 						if int(columns[13])==group1_initiator:							 
						#	print(columns)
                                                	initiator1=initiator1+1;
						elif int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						elif int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						group_ini=0
						for j in sorted(group1):
							if(j==int(columns[2])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0 or  no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
					total=float(initiator1+initiator2+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator1/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator2)
					intrusion_factor=intrusion/total
					if(group_ini==group1_initiator):
						relaibility_d=(avg_relaibility_H-relaibility)**2
						damage_d=(avg_damage_H-damage_factor)**2
						intrusion_d=(avg_intrusion_H-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						count=count+1
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator1,"\t\t",initiator2,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3),"\t"#,round(relaibility_sum,3),"\t",round(damage_sum,3),"\t",round(intrusion_sum,3)
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	for (dirpath, dirnames, filenames2) in walk('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-hdr-test/Data/H_1_60_57'):
		f6.extend(filenames2)
		for name in f6:
			fullName = os.path.join('/home/jagnyashini/Code_Base/glossy-frame/apps/glossy-mint-hdr-test/Data/H_1_60_57',name)
			node_list = []
#			node_neighbor_list = []
			f2 = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
			for line in f2:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
				 		#print(columns)
						#print(columns[1],columns[22])
	 					if int(columns[13])==group1_initiator:
	                                                initiator1=initiator1+1;
						if int(columns[13])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[13])==0:
							no_rcv_cnt=no_rcv_cnt+1;
						group_ini=0
						for j in sorted(group2):
							if(j==int(columns[2])):
								group_ini=group2_initiator
						for i in sorted(group1):
							if(i==int(columns[2])):
								group_ini=group1_initiator
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0 or  no_rcv_cnt!=0):
				#print(initiator1)
				#print(initiator2)
				if(initiator1!=0 or initiator2!=0 or no_rcv_cnt!=0):
					total2=initiator1+initiator2+no_rcv_cnt
					total=float(initiator1+initiator2+no_rcv_cnt)
					#print(total)
					percent_initiator1=float((initiator1/total)*100)
        		                percent_initiator2=float((initiator2/total)*100)
        		                percent_initiator_nt_rcv=float((no_rcv_cnt/total)*100)
					relaibility=initiator2/total;
					damage_factor=no_rcv_cnt/total;
					intrusion=float(initiator1)
					intrusion_factor=intrusion/total
					if(group_ini==group2_initiator):
						count=count+1
						relaibility_d=(avg_relaibility_H-relaibility)**2
						damage_d=(avg_damage_H-damage_factor)**2
						intrusion_d=(avg_intrusion_H-intrusion_factor)**2
						relaibility_sum=relaibility_sum+relaibility_d
						damage_sum=damage_sum+damage_d
						intrusion_sum=intrusion_sum+intrusion_d
						print count,"\t",columns[2],"\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t",round(percent_initiator1,1),"\t\t",round(percent_initiator_nt_rcv,1),"\t\t",initiator2,"\t\t",initiator1,"\t\t",no_rcv_cnt,"\t\t",round(relaibility,3),"\t\t",round(damage_factor,3),"\t\t",round(intrusion_factor,3)#,"\t",round(relaibility_sum,3),"\t",round(damage_sum,3),"\t",round(intrusion_sum,3)
				
                	initiator1=0
			initiator2=0
			no_rcv_cnt=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			relaibility=0.0
			damage_factor=0.0
			intrusion=0.0
			intrusion_factor=0.0
			relaibility_d=0.0
			damage_d=0.0
			intrusion_d=0.0
	relaibility_sd_H=math.sqrt(relaibility_sum/(count-1))
	damage_sd_H=math.sqrt(damage_sum/(count-1))
	intrusion_sd_H=math.sqrt(intrusion_sum/(count-1))
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	#print round(relaibility_total,3),round(damage_total,3),round(intrusion_total,3),count
	print "\t\t\t\t\t\t\t\t\t\t\t\t\t\tAverage------->",round(avg_relaibility_H,3),"\t\t",round(avg_damage_H,3),"\t\t",round(avg_intrusion_H,3)
	print "\t\t\t\t\t\t\t\t\t\t\t\t\tStandard Deviation---->",round(relaibility_H_sd,3),"\t\t",round(damage_H_sd,3),"\t\t",round(intrusion_sd_H,3)
	print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"


def main():

    if len(sys.argv) != 1:
        print('...')
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()
			
