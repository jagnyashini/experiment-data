
#A single .csv file must be present inside the folder. For indriya which contains multiple file use cat *.csv>combine.csv to make it one file and run this script
#Make a note of the columns being accessed , may vary depending on your program. Change the colomn according to your program

import sys
import os
import csv

from os import walk
from fileinput import close
create_nearest_neighbour_lst=1	
def analyze():
	node_count=0
	parent_list = []
	print "Nodeid", "\t","Neighbour","\t","RSSI","\t","LQI","\t","Pkt_snt","\t","pkt_rcv","\t","prr"
	f = []
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Downloads/logs_42232'):

		f.extend(filenames)
		for name in f:

			fullName = os.path.join('/home/jagnyashini/Downloads/logs_42232',name)
			node_list = []
			f = open(fullName, 'r')
			line_no=0;

			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				try:
					#print columns
					#print(columns[8])
					node_list.append(int(columns[0]))
				except (IndexError,ValueError):
					gotdata = 'null'
 			f.close()
			node_set = set(node_list)
			print "*Vertices ",len(node_set)
			for i in sorted(node_set):
				print i,"\t","\"",i,"\""
			print "*Edges "
			#print len(node_set)
			#print sorted(node_set)
			node_neighbor_list=[]
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				for line in f:
					line = line.strip()
					columns = line.split()
					try:
						if int(columns[0]) == i:
							node_neighbor_list.append(int(columns[4]))
					except (IndexError,ValueError):
						gotdata = 'null'
	 			f.close()
				node_neighbor_set = set(node_neighbor_list)
				#print i,":"
				#print sorted(node_neighbor_set)	
				for j in sorted(node_neighbor_set):
					net_rssi=0
					count=0
					net_lqi=0
					average_rssi=0.0
					average_lqi=0.0
					f = open(fullName, 'r')
					for line in f:
						line = line.strip()
						columns = line.split()
						try:			
							if int(columns[0])==i:
								#print columns[1],i,columns[4],j,columns[6],(columns[7])			
								if int(columns[4])==j:
									packet_sent=float(columns[2])
									rssi=int(columns[6])
									net_rssi=net_rssi+rssi
									lqi=int(columns[7])
									net_lqi=net_lqi+lqi
									count=count+1
									#print columns
									packet_recieved=0
									f1=open(fullName, 'r')
									for line1 in f1:
										line1 = line1.strip()
										columns1 = line1.split()
										try:
											if int(columns1[0])==j and int(columns1[4])==i :
								#				print int(columns1[1]),j,int(columns1[4]),i
												packet_recieved=float(columns1[5])
										except (IndexError,ValueError):
											gotdata = 'null'
									f1.close()

						except (IndexError,ValueError):
							gotdata = 'null'
					#print lqi,rssi,count
					if count!=0:
						average_rssi=net_rssi/count
						average_lqi=net_lqi/count	
					f.close()
					prr=float(packet_recieved/packet_sent)
					if (create_nearest_neighbour_lst==0): 
						print i,"\t", j ,"\t\t" ,average_rssi ,"\t", average_lqi,"\t",packet_sent,"\t\t",packet_recieved,"\t\t",round(prr,5)
					else:
						print i,"\t", j ,"\t\t",round(average_rssi,2),"\t\t",round(average_lqi,2),"\t\t",round(prr,2)
				node_neighbor_list = []


def main():

    if len(sys.argv) != 1:
        print '...'
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()


