#The files Without_SFD.csv and With_SFD.cvs contains the output after running the python script to analyze the neighbours and these are then compared using this script.First run the Neighbour discovery analysis python script on the Indriya output and then then run this comparision script to compare with change of SFD and without change.

import sys
import os
import csv

from os import walk
from fileinput import close

def analyze():

	node_count=0
	parent_list = []
	#print "Nodeid", "\t\t","Neighbour","\t","WOS_RSSI","\t","WOS_LQI","\t","WOS_Pkt_snt","\t","WOS_pkt_rcv","\t","WOS_prr","\t","WS_RSSI","\t","WS_LQI","\t","WS_Pkt_snt","\t","WS_pkt_rcv","\t","WS_prr"
	#print ("Nodeid","\t","Neighbour","\t","0xA50F","\t","0xA50F","\t","0xA50F","\t","no_change","\t","no_change","\t","no_change",,"\t","0xA30F","\t","0xA30F","\t","0xA30F","\t","0xA80F","\t","0xA80F","\t","0xA80F")

	#print ("-----------------------------------------------------------------------------------------------------------------------------------------") 
	#data output as follows:
	#File 1 File 4 File3 File 2
	f = []
	f2 = []
	f4 = []
	f3= []
	fullName1 = os.path.join('/home/jagnyashini/Code_Base/contikis/data/Indriya/Comparision/all_result','0xA50F.csv')
	fullName2 = os.path.join('/home/jagnyashini/Code_Base/contikis/data/Indriya/Comparision/all_result','0xA80F.csv')
	fullName3 = os.path.join('/home/jagnyashini/Code_Base/contikis/data/Indriya/Comparision/all_result','0xA30F.csv')
	fullName4 = os.path.join('/home/jagnyashini/Code_Base/contikis/data/Indriya/Comparision/all_result','no_change.csv')
print "Nodeid", "\t\t","Neighbour","\t","F1_RSSI","\t","F1_PRR","\t","F1_LQI","\t","F4_RSSI","\t","F4_PRR","\t","F4_LQI","\t","F3_RSSI","\t","F3_PRR","\t","F3_LQI","\t","F2_RSSI","\t","F2_PRR","\t","F2_LQI"
	
	#print fullName1
	#print fullName2
	node_list = []
	f = open(fullName1, 'r')
	line_no=0;
	for line in f:
		line = line.strip()
		#print(line)
		columns = line.split()
		#print(columns)
		f2 = open(fullName2, 'r')
		flag2=0
		for line2 in f2:
			line2 = line2.strip()
			#print(line)
			columns1 = line2.split()
			if(columns[0]==columns1[0] and columns[1]==columns1[1]):
				f3 = open(fullName3, 'r')
				flag2=1
				flag3=0
				for line3 in f3:
					line3 = line3.strip()
					#print(line)
					columns2 = line3.split()
					if(columns[0]==columns2[0] and columns[1]==columns2[1]):
						f4 = open(fullName4, 'r')
						flag3=1
						flag4=0
						for line4 in f4:
							line4 = line4.strip()
							columns3 = line4.split()
							if(columns[0]==columns3[0] and columns[1]==columns3[1]):
								#print("here")
								flag4=1
								try:
									c6=float(columns[6])
									c6_4=float(columns3[6])
									print (columns[0] ,"\t",columns[1], "\t",columns[2],"\t",round(c6,2),"\t" ,columns[3],'\t',columns3[2],"\t",round(c6_4,2), "\t",columns3[3],"\t",end =" ")
								except (IndexError,ValueError):
									gotdata = 'null' 	
						if(flag4==0):
							try:
								c6=float(columns[6])
								print (columns[0] ,"\t",columns[1], "\t",columns[2],"\t",round(c6,2) ,"\t",columns[3],"\t", "--","\t","--", "\t","--","\t",end =" ")	
							except (IndexError,ValueError):
									gotdata = 'null' 
						try:
							c6_3=float(columns2[6])
							print (columns2[2],"\t",round(c6_3,2),"\t",columns2[3],"\t",end =" ")
						except (IndexError,ValueError):
							gotdata = 'null' 
				if(flag3==0):
					c6=float(columns[6])
					f4 = open(fullName4, 'r')
					flag4=0
					for line4 in f4:
						line4 = line4.strip()
						columns3 = line4.split()
						if(columns[0]==columns3[0] and columns[1]==columns3[1]):
							#print("here")
							flag4=1
							try:
								c6=float(columns[6])
								c6_4=float(columns3[6])
								print (columns[0] ,"\t",columns[1], "\t",columns[2],"\t",round(c6,2),"\t" ,columns[3],'\t',columns3[2],"\t",round(c6_4,2),  "\t",columns3[3],"\t",end =" ")
							except (IndexError,ValueError):
									gotdata = 'null' 	
					if(flag4==0):
						try:
							c6=float(columns[6])
							print (columns[0] ,"\t",columns[1], "\t",columns[2],"\t",round(c6,2) ,"\t",columns[3],"\t", "--","\t","--", "\t","--","\t",end =" ")	
						except (IndexError,ValueError):
							gotdata = 'null' 
					print ("--","\t","--","\t","--","\t",end =" ")
				try:
					c6_2=float(columns1[6])
					print (columns1[2],"\t",round(c6_2,2),"\t",columns1[3],"\t")
				except (IndexError,ValueError):
					gotdata = 'null' 			
		if (flag2==0):	
			flag3=0
			for line3 in f3:
				line3 = line3.strip()
				#print(line)
				columns2 = line3.split()
				if(columns[0]==columns2[0] and columns[1]==columns2[1]):
					f4 = open(fullName4, 'r')
					flag3=1
					flag4=0
					for line4 in f4:
						line4 = line4.strip()
						columns3 = line4.split()
						if(columns[0]==columns3[0] and columns[1]==columns3[1]):
							#print("here")
							flag4=1
							try:
								c6=float(columns[6])
								c6_4=float(columns3[6])
								print (columns[0] ,"\t",columns[1], "\t",columns[2],"\t",round(c6,2),"\t" ,columns[3],'\t',columns3[2],"\t",round(c6_4,2),"\t",columns3[3],"\t",end =" ")
							except (IndexError,ValueError):
								gotdata = 'null' 	
					if(flag4==0):
						try:
							c6=float(columns[6])
							print (columns[0] ,"\t",columns[1], "\t",columns[2],"\t",round(c6,2) ,"\t",columns[3],"\t", "--","\t","--", "\t","--","\t",end =" ")	
						except (IndexError,ValueError):
							gotdata = 'null' 
					try:
						c6_3=float(columns2[6])
						print (columns2[2],"\t",round(c6_3,2),"\t",columns2[3],"\t",end =" ")
					except (IndexError,ValueError):
						gotdata = 'null' 
			if(flag3==0):
				c6=float(columns[6])
				f4 = open(fullName4, 'r')
				flag4=0
				for line4 in f4:
					line4 = line4.strip()
					columns3 = line4.split()
					if(columns[0]==columns3[0] and columns[1]==columns3[1]):
						#print("here")
						flag4=1
						try:
							c6=float(columns[6])
							c6_4=float(columns3[6])
							print (columns[0] ,"\t",columns[1], "\t",columns[2],"\t",round(c6,2),"\t" ,columns[3],'\t',columns3[2],"\t",round(c6_4,2), "\t",columns3[3],"\t",end =" ")
						except (IndexError,ValueError):
							gotdata = 'null' 	
				if(flag4==0):
					try:
						c6=float(columns[6])
						print (columns[0] ,"\t",columns[1], "\t",columns[2],"\t",round(c6,2) ,"\t",columns[3],"\t", "--","\t","--", "\t","--","\t",end =" ")	
					except (IndexError,ValueError):
						gotdata = 'null' 
				print ("--","\t","--","\t","--","\t",end =" ")
			print ("--","\t","--","\t","--","\t")
	f.close()
def main():

    if len(sys.argv) != 1:
        print ('...')
        sys.exit(1)

    analyze();
if __name__ == '__main__':
  main()


