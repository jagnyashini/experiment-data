import sys
import os
import csv

from os import walk
from fileinput import close

def analyze():

	node_count=0
	parent_list = []
	node_list_1=[]
	node_list_2=[]
	node_list_3=[]
	group1=[2,1,5,7,9,10,12,13,14,15,16,19,27,28,29,30,32,34,35,51,52,56]
	group2=[40,16,17,18,19,20,22,24,26,37,42,43,44,45,46,47,48,52,53,54,55,60,61]
	level_1=[53,55,61,54,51,56,57,60,52,16,19,17,24,22,18]
	group1_initiator=2
	group2_initiator=40
	count_node_1=0
	count_node_2=0
	count_node_both=0
	group_ini=0
	f = []
	f2 = []
	f3=  []
	f4=[]
	print"Node Id:\t""Initiator_Id\t""Reception_from_Grp_Initiator\t""Reception_from_other"
	for (dirpath, dirnames, filenames) in walk('../data/June28/2/WithoutSFD/'):

		f.extend(filenames)
		for name in f:

			fullName = os.path.join('../data/June28/2/WithoutSFD/',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>1):
				#	print(columns)
 					if int(columns[8])==2:
                                                initiator1=initiator1+1;
					if int(columns[8])==40:
						initiator2=initiator2+1;
					node_list.append(columns[1])
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0):
				#print(initiator1)
				#print(initiator2)
				total2=initiator1+initiator2
				total=float(initiator1+initiator2)
				#print(total)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
				if(initiator1==total2):
					node_list_1.append(columns[1])
					count_node_1=count_node_1+1
				if(initiator2==total2):
					node_list_2.append(columns[1])
					count_node_2=count_node_2+1										
				if((initiator1!=total2) and (initiator2!=total2)):
					node_list_3.append(columns[1])
					count_node_both=count_node_both+1
				group_ini=0
				for i in sorted(group1):
					if(i==int(columns[1])):
						group_ini=group1_initiator
				for i in sorted(group2):
					if(i==int(columns[1])):
						group_ini=group2_initiator
				if(group_ini==group1_initiator):
					print columns[1],"\t\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t\t\t",round(percent_initiator2,1)

                        initiator1=0
                        initiator2=0
			percent_initiator1=0
			percent_initiator2=0
		count_node_total=float(count_node_1+count_node_2+count_node_both)
		count_node_1_percent=float((count_node_1/count_node_total)*100)
		count_node_2_percent=float((count_node_2/count_node_total)*100)
		count_node_both_percent=float((count_node_both/count_node_total)*100)
		node_set1 = set(node_list_1)
		node_set2 = set(node_list_2)
		node_set3 = set(node_list_3)
	for (dirpath, dirnames, filenames2) in walk('../data/June28/2/WithoutSFD/'):
		f2.extend(filenames2)
		for name in f2:

			fullName = os.path.join('../data/June28/2/WithoutSFD/',name)
			node_list = []
#			node_neighbor_list = []
			f2 = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			for line in f2:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>1):
				#	print(columns)
 					if int(columns[8])==2:
                                                initiator1=initiator1+1;
					if int(columns[8])==40:
						initiator2=initiator2+1;
					node_list.append(columns[1])
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0):
				#print(initiator1)
				#print(initiator2)
				total2=initiator1+initiator2
				total=float(initiator1+initiator2)
				#print(total)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
				if(initiator1==total2):
					node_list_1.append(columns[1])
					count_node_1=count_node_1+1
				if(initiator2==total2):
					node_list_2.append(columns[1])
					count_node_2=count_node_2+1										
				if((initiator1!=total2) and (initiator2!=total2)):
					node_list_3.append(columns[1])
					count_node_both=count_node_both+1
				group_ini=0
				for j in sorted(group2):
					if(j==int(columns[1])):
						group_ini=group2_initiator
				for i in sorted(group1):
					if(i==int(columns[1])):
						group_ini=group1_initiator
				if(group_ini==group2_initiator):
					print columns[1],"\t\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t\t\t",round(percent_initiator1,1)
                        initiator1=0
                        initiator2=0
			percent_initiator1=0
			percent_initiator2=0
		count_node_total=float(count_node_1+count_node_2+count_node_both)
		count_node_1_percent=float((count_node_1/count_node_total)*100)
		count_node_2_percent=float((count_node_2/count_node_total)*100)
		count_node_both_percent=float((count_node_both/count_node_total)*100)
		node_set1 = set(node_list_1)
		node_set2 = set(node_list_2)
		node_set3 = set(node_list_3)
	print"Node Id:\t""Initiator_Id\t""Reception_from_Grp_Initiator\t""Reception_from_other"
	for (dirpath, dirnames, filenames3) in walk('../data/June28/2/WithoutSFD/'):
		f3.extend(filenames3)
		for name in f3:

			fullName = os.path.join('../data/June28/2/WithoutSFD/',name)
			node_list = []
#			node_neighbor_list = []
			f3 = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			for line in f3:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>1):
				#	print(columns)
 					if int(columns[8])==2:
                                                initiator1=initiator1+1;
					if int(columns[8])==40:
						initiator2=initiator2+1;
					node_list.append(columns[1])
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0):
				#print(initiator1)
				#print(initiator2)
				total2=initiator1+initiator2
				total=float(initiator1+initiator2)
				#print(total)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
				if(initiator1==total2):
					node_list_1.append(columns[1])
					count_node_1=count_node_1+1
				if(initiator2==total2):
					node_list_2.append(columns[1])
					count_node_2=count_node_2+1										
				if((initiator1!=total2) and (initiator2!=total2)):
					node_list_3.append(columns[1])
					count_node_both=count_node_both+1
				group_ini=0
				for i in sorted(group1):
					if(i==int(columns[1])):
						group_ini=group1_initiator
				for i in sorted(group2):
					if(i==int(columns[1])):
						group_ini=group2_initiator
				if(group_ini==group1_initiator):
					for j in sorted(level_1):
						if(int(columns[1])==j):
							print columns[1],"\t\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t\t\t",round(percent_initiator2,1)

                        initiator1=0
                        initiator2=0
			percent_initiator1=0
			percent_initiator2=0
		count_node_total=float(count_node_1+count_node_2+count_node_both)
		count_node_1_percent=float((count_node_1/count_node_total)*100)
		count_node_2_percent=float((count_node_2/count_node_total)*100)
		count_node_both_percent=float((count_node_both/count_node_total)*100)
		node_set1 = set(node_list_1)
		node_set2 = set(node_list_2)
		node_set3 = set(node_list_3)
	for (dirpath, dirnames, filenames4) in walk('../data/June28/2/WithoutSFD/'):
		f4.extend(filenames4)
		for name in f4:

			fullName = os.path.join('../data/June28/2/WithoutSFD/',name)
			node_list = []
#			node_neighbor_list = []
			f4 = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			for line in f4:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>1):
				#	print(columns)
 					if int(columns[8])==2:
                                                initiator1=initiator1+1;
					if int(columns[8])==40:
						initiator2=initiator2+1;
					node_list.append(columns[1])
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0):
				#print(initiator1)
				#print(initiator2)
				total2=initiator1+initiator2
				total=float(initiator1+initiator2)
				#print(total)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
				if(initiator1==total2):
					node_list_1.append(columns[1])
					count_node_1=count_node_1+1
				if(initiator2==total2):
					node_list_2.append(columns[1])
					count_node_2=count_node_2+1										
				if((initiator1!=total2) and (initiator2!=total2)):
					node_list_3.append(columns[1])
					count_node_both=count_node_both+1
				group_ini=0
				for i in sorted(group2):
					if(i==int(columns[1])):
						group_ini=group2_initiator
				for i in sorted(group1):
					if(i==int(columns[1])):
						group_ini=group1_initiator
				if(group_ini==group2_initiator):
					for j in sorted(level_1):
						if(int(columns[1])==j):
							print columns[1],"\t\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t\t\t",round(percent_initiator1,1)
                        initiator1=0
                        initiator2=0
			percent_initiator1=0
			percent_initiator2=0
		count_node_total=float(count_node_1+count_node_2+count_node_both)
		count_node_1_percent=float((count_node_1/count_node_total)*100)
		count_node_2_percent=float((count_node_2/count_node_total)*100)
		count_node_both_percent=float((count_node_both/count_node_total)*100)
		node_set1 = set(node_list_1)
		node_set2 = set(node_list_2)
		node_set3 = set(node_list_3)
		print("Percentage from Initiator 2:",round(count_node_1_percent,3))
		print("Percentage from Initiator 40::",round(count_node_2_percent,3))
		print("Precentage from Both Initiator::",round(count_node_both_percent,3))
#			print len(node_set)
			#print(node_set)
		print("List of node from only Initiator:2 :",sorted(node_set1))
		print("List of node from only Initiator:40 :",sorted(node_set2))
		print("List of node from Both Initiator :",sorted(node_set3))
def main():

    if len(sys.argv) != 1:
        print('...')
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()

