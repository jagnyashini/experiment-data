import sys
import os
import csv

from os import walk
from fileinput import close
flocklab=0
indriya=1
virtual_node=1
real_node=0
if(indriya==1):
	nodes=[1,5,7,9,10,12,13,15,16,17,18,19,20,22,23,24,26,27,28,29,30,35,40,41,42,44,45,46,47,48,49,51,52,53,54,55,56,57,58,60,61];
else:
	nodes=[1,2,3,4,6,8,10,11,13,15,16,17,18,19,20,22,23,24,25,26,27,28,31,32,33];

def analyze():
	f = []
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/Centrality/data/Indriya/Onetime_collection_Hop_Count_24_Dec/Night/Level_Data'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/Centrality/data/Indriya/Onetime_collection_Hop_Count_24_Dec/Night/Level_Data',name)
			f = open(fullName, 'r')
			level1=[]
			level2=[]
			level3=[]
			level4=[]
			level5=[]
			level6=[]
			level7=[]
			for line in f:
				line = line.strip()
				columns = line.split()
				try:
					index=0
					for i in nodes:
						index=index+1
						if(int(columns[1])==i and virtual_node==1):
							node=index
						elif(int(columns[1])==i and real_node==1):
							node=i
						if(int(columns[0])==i and virtual_node==1):
							node_id=index
						elif(int(columns[0])==i and real_node==1):
							node_id=i
					if(float(columns[2])>0.5 and float(columns[2])<=1.5):
						level1.append(node)
					elif(float(columns[2])>1.5 and float(columns[2])<=2.5):
						level2.append(node)
					elif(float(columns[2])>2.5 and float(columns[2])<=3.5):
						level3.append(node)
					elif(float(columns[2])>3.5 and float(columns[2])<=4.5):
						level4.append(node)
					elif(float(columns[2])>4.5 and float(columns[2])<=5.5):
						level5.append(node)
					elif(float(columns[2])>5.5 and float(columns[2])<=6.5):
						level6.append(node)
					elif(float(columns[2])>6.5 and float(columns[2])<=7.5):
						level7.append(node)
				except (IndexError,ValueError):
                                        gotdata = 'null'
			print "\n\n"
			print "--------------------------------------------------------------------------------"
			print "Node-Id :" ,node_id
			print "--------------------------------------------------------------------------------"
			print "Level-1 :" ,level1
			print "--------------------------------------------------------------------------------"
			print "Level-2 :" ,level2
			print "--------------------------------------------------------------------------------"
			print "Level-3 :" ,level3
			print "--------------------------------------------------------------------------------"
			print "Level-4 :" ,level4
			print "--------------------------------------------------------------------------------"
			print "Level-5 :" ,level5
			print "--------------------------------------------------------------------------------"
			print "Level-6 :" ,level6
			print "--------------------------------------------------------------------------------"
			print "Level-7 :" ,level7
			print "--------------------------------------------------------------------------------"
def main():

    if len(sys.argv) != 1:
        print('...')
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()

