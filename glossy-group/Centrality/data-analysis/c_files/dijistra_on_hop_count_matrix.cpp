// reading a text file
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include<stdio.h>
#include <stdio.h>
#include <limits.h>
using namespace std;
#define INDRIYA 0
#define FLOCKLAB 1
#define PRINTING 0
#define RELAIBILITY_TARGET 0.8
#if INDRIYA==1
#define NODES 41
int nodes[NODES]={1,5,7,9,10,12,13,15,16,17,18,19,20,22,23,24,26,27,28,29,30,35,40,41,42,44,45,46,47,48,49,51,52,53,54,55,56,57,58,60,61};
#endif
#if FLOCKLAB==1
#define NODES 25
int nodes[NODES]={1, 2, 3, 4, 6, 8, 10, 11, 13, 15, 16, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 31, 32, 33};
#endif
int shortest_distance_matrix[NODES][NODES];

int minDistance(int dist[], bool sptSet[])
{
	// Initialize min value
	int min = INT_MAX, min_index;

	for (int v = 0; v < NODES; v++)
		if (sptSet[v] == false && dist[v] <= min)
			min = dist[v], min_index = v;

	return min_index;
}

// A utility function to print the constructed distance array
int printSolution(int dist[],int src)
{
	//printf("Vertex \t\t Distance from Source\n");
	for (int i = 0; i < NODES; i++)
	{ //printf("%d \t\t %d\n", i, dist[i]);
		if(dist[i]==INT_MAX)
			shortest_distance_matrix[src][i]==0;
		else
		shortest_distance_matrix[src][i]=dist[i];
	}



}
int dijkstra(int graph[NODES][NODES], int src)
{
	int dist[NODES]; // The output array.  dist[i] will hold the shortest
	// distance from src to i

	bool sptSet[NODES]; // sptSet[i] will be true if vertex i is included in shortest
	// path tree or shortest distance from src to i is finalized

	// Initialize all distances as INFINITE and stpSet[] as false
	for (int i = 0; i < NODES; i++)
		dist[i] = INT_MAX, sptSet[i] = false;

	// Distance of source vertex from itself is always 0
	dist[src] = 0;

	// Find shortest path for all vertices
	for (int count = 0; count < NODES -1; count++) {
		// Pick the minimum distance vertex from the set of vertices not
		// yet processed. u is always equal to src in the first iteration.
		int u = minDistance(dist, sptSet);

		// Mark the picked vertex as processed
		sptSet[u] = true;

		// Update dist value of the adjacent vertices of the picked vertex.
		for (int v = 0; v < NODES; v++)

			// Update dist[v] only if is not in sptSet, there is an edge from
			// u to v, and total weight of path from src to  v through u is
			// smaller than current value of dist[v]
			if (!sptSet[v] && graph[u][v] && dist[u] != INT_MAX
					&& dist[u] + graph[u][v] < dist[v])
				dist[v] = dist[u] + graph[u][v];
	}

	// print the constructed distance array
	//cout<<"Node"<<src<<"::"<<"\n";
	printSolution(dist,src);
}

int main () {
	string line;
#if INDRIYA==1
	ifstream myfile ("/home/jagnyashini/Code_Base/glossy-frame/Centrality/data/Indriya/Neighbour_Discovery/Neighbour_Discovery_sept_12/neighbour_prr.txt");
#endif
#if FLOCKLAB==1
	ifstream myfile ("/home/jagnyashini/Code_Base/glossy-frame/Centrality/data/Flocklab/Neighbour_Discovery/0X07/neighbour_prr.txt");
#endif

	float data[1000][10];
	int row,column;
	int count=0;

	if (myfile.is_open())
	{
		row=0;
		while ( getline (myfile,line) )
		{
			//cout << line << '\n';
			// word variable to store word
			string word;

			// making a string stream
			stringstream iss(line);
			// Read and print each word.
			column=0;
			while (iss >> word)
			{
				stringstream geek(word);
				// sscanf(word, "%d", &data[row][column]);
				geek>>data[row][column];
				// cout<<data[row][column]<<"\t";
				column=column+1;
			}
			row=row+1;
		}
		myfile.close();
	}
	else cout << "Unable to open file";


	/*************************************************************************************************/
	/* Print the elements which are been read from the file and stored in an array                   */
	/*************************************************************************************************/
#if PRINTING==1

	for(int i=0;i<row;i++)
	{
		for(int j=0;j<column;j++)
		{
			cout<<data[i][j]<<"\t";
		}
		cout<<"\n";
	}
#endif


	//  /*************************************************************************************************/
	//  /* Fill in the adjacency matrix    															   */
	//  /*************************************************************************************************/
	float adj_prr_matrix[NODES][NODES];
	int neighbourhood_matrix[NODES][NODES];

	for(int i=0;i<NODES;i++)
	{
		for(int j=0; j<NODES; j++)
		{
			adj_prr_matrix[i][j]=0;
			neighbourhood_matrix[i][j]=0;
			shortest_distance_matrix[i][j]=0;
		}
	}
	int src_node,src_node_index=0;
	int dst_node,dst_node_index=0;
	for(int i=0;i<row;i++)
	{
		src_node=data[i][0];
		dst_node=data[i][1];
		for(int j=0;j<NODES;j++)
		{
			if(src_node==nodes[j])
			{
				src_node_index=j;
			}
			if(dst_node==nodes[j])
			{
				dst_node_index=j;
			}
		}
		adj_prr_matrix[src_node_index][dst_node_index]=data[i][2];

	}
	//  /*************************************************************************************************/
	//  /* Print  the adjacency matrix    															   */
	//  /*************************************************************************************************/
#if PRINTING==1

	for(int i=0;i<NODES;i++)
	{
		cout<<nodes[i]<<"\n";

		for(int j=0; j<NODES; j++)
		{
			cout<<adj_prr_matrix[i][j]<<" ";
		}
		cout<<"\n";
	}
#endif
	//  /*************************************************************************************************/
	//  /* Fill in the  neighbourhood matrix    														   */
	//  /*************************************************************************************************/
	for(int i=0;i<NODES;i++)
	{
		for(int j=0; j<NODES; j++)
		{
			if(adj_prr_matrix[i][j]>RELAIBILITY_TARGET)
			{
				neighbourhood_matrix[i][j]=1;
			}
		}
	}

	//  /*************************************************************************************************/
	//  /* Print  the neighbourhood matrix    															   */
	//  /*************************************************************************************************/
#if PRINTING==1

	for(int i=0;i<NODES;i++)
	{			cout<<nodes[i]<<"\n";

		for(int j=0; j<NODES; j++)
		{
			cout<<neighbourhood_matrix[i][j]<<" ";
		}
		cout<<"\n";
	}
#endif

	/*************************************************************************************************/
	/* ruun dijistra over all nodes matrix    														 */
	/*************************************************************************************************/
	for(int i=0;i<NODES;i++)
	{
		dijkstra(neighbourhood_matrix, i);
	}

	//  /*************************************************************************************************/
		//  /* Print  the shortest_distance_matrix matrix    															   */
		//  /*************************************************************************************************/

	for(int i=0;i<NODES;i++)
		{
			//cout<<nodes[i]<<"\n";
			for(int j=0; j<NODES; j++)
			{
				cout<<shortest_distance_matrix[i][j]<<" ";
			}
			cout<<"\n";
		}

}
