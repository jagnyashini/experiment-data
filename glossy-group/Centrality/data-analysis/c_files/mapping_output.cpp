// reading a text file
#include <iostream>
#include <fstream>
#include <string>
#include<bits/stdc++.h>
using namespace std;
#define INDRIYA 0
#define FLOCKLAB 1
#define MAX_NUM_GROUPS 1
#define MAX_NUM_NODES_PER_GROUP 41

#if INDRIYA==1
#define NODES 41
int nodes[NODES]={1,5,7,9,10,12,13,15,16,17,18,19,20,22,23,24,26,27,28,29,30,35,40,41,42,44,45,46,47,48,49,51,52,53,54,55,56,57,58,60,61};
#endif
#if FLOCKLAB==1
#define NODES 25
int nodes[NODES]={1, 2, 3, 4, 6, 8, 10, 11, 13, 15, 16, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 31, 32, 33};
#endif
int group[MAX_NUM_GROUPS][MAX_NUM_NODES_PER_GROUP] ={28,
28,
28,
6,
28,
6,
28,
6,
28,
28,
6,
6,
28,
6,
28,
6,
28,
28,
6,
6,
28,
6,
6,
6,
6
};




int main () {
	for(int i=0;i<MAX_NUM_GROUPS;i++)
	{
		cout<<"{";
		for(int j=0;j<MAX_NUM_NODES_PER_GROUP;j++)
		{
			int node_id=group[i][j];
			if(node_id!=0)
			{
				for (int k=0;k<NODES;k++)
				{
					if(nodes[k]==node_id)
					{
						cout<<k+1<<"\n";
						break;
					}
				}
			}
		}
		cout<<"},";
	}
}
