// reading a text file
#include <iostream>
#include <fstream>
#include <string>
#include<bits/stdc++.h>
using namespace std;
#define INDRIYA 0
#define FLOCKLAB 1
#define PRINTING 0
#define RELAIBILITY_TARGET 0.8
#define MAX_NUM_GROUPS 2
#define MAX_NUM_NODES_PER_GROUP 25
#define MAX_ENTRIES 14
#if INDRIYA==1
#define NODES 41
int nodes[NODES]={1,5,7,9,10,12,13,15,16,17,18,19,20,22,23,24,26,27,28,29,30,35,40,41,42,44,45,46,47,48,49,51,52,53,54,55,56,57,58,60,61};
#endif
#if FLOCKLAB==1
#define NODES 25
int nodes[NODES]={1, 2, 3, 4, 6, 8, 10, 11, 13, 15, 16, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 31, 32, 33};
int group[MAX_INPUT][MAX_NUM_GROUPS][MAX_NUM_NODES_PER_GROUP]={
		{
				{4,1,2,8,15,33,3,31,32,10,6,16},
				{20,26,19,25,13,11,17,23,24,27,18,28,22}
		},
		{
				{2,1,10,8,15,33,3,31,32,4,6,16},
				{13,20,19,25,26,11,17,23,24,27,18,28,22}
		},
		{
				{6,1,10,8,15,33,3,31,32,4,6,16},
				{28,20,19,25,26,11,17,23,24,27,18,28,22}
		},
		{
				{15,1,10,8,6,33,3,31,32,4,6,16},
				{17,20,19,25,26,11,28,23,24,27,18,28,22}
		},
		{
				{32,1,10,8,15,33,3,31,6,4,6,16},
				{27,20,19,25,26,11,17,23,24,28,18,28,22}
		},
		{
				{10,1,6,8,15,33,3,31,32,4,6,16},
				{18,20,19,25,26,11,17,23,24,27,28,28,22}
		},
		{
				{8,1,10,6,15,33,3,31,32,4,6,16},
				{11,20,19,25,26,28,17,23,24,27,18,28,22}
		},
		{
				{3,1,10,8,15,33,6,31,32,4,6,16},
				{23,20,19,25,26,11,17,28,24,27,18,28,22}
		},
		{
				{31,1,4,8,15,33,32,3,2,6,28,22,16},
				{10,11,25,13,17,19,20,26,23,24,27,18}
		},
		{
				{3,6,31,4,8,33,16,22,26,20,24,18,13},
				{23,10,32,15,1,2,28,25,19,27,11,17}
		},
		{
				{4,6,31,3,8,33,16,22,26,20,24,18,13},
				{15,10,32,23,1,2,28,25,19,27,11,17}
		},
		{
				{24,6,31,4,8,33,16,22,26,20,3,18,13},
				{2,10,32,15,1,23,28,25,19,27,11,17}
		},
		{
				{6,3,31,4,8,33,16,22,26,20,24,18,13},
				{28,10,32,15,1,2,23,25,19,27,11,17}
		},
		{
				{18,6,31,4,8,33,16,22,26,20,24,3,13},
				{27,10,32,15,1,2,28,25,19,23,11,17}
		}
};
int group_for_comp[MAX_NUM_GROUPS-1][MAX_NUM_NODES_PER_GROUP];
#endif
int shortest_distance_matrix[NODES][NODES];
int neighbourhood_matrix[NODES][NODES];

int minDistance(int dist[], bool sptSet[])
{
	// Initialize min value
	int min = INT_MAX, min_index;

	for (int v = 0; v < NODES; v++)
		if (sptSet[v] == false && dist[v] <= min)
			min = dist[v], min_index = v;

	return min_index;
}

// A utility function to print the constructed distance array
int printSolution(int dist[],int src)
{
	//printf("Vertex \t\t Distance from Source\n");
	for (int i = 0; i < NODES; i++)
	{ //printf("%d \t\t %d\n", i, dist[i]);
		if(dist[i]==INT_MAX)
			shortest_distance_matrix[src][i]==0;
		else
			shortest_distance_matrix[src][i]=dist[i];
	}



}
int dijkstra(int graph[NODES][NODES], int src)
{
	int dist[NODES]; // The output array.  dist[i] will hold the shortest
	// distance from src to i

	bool sptSet[NODES]; // sptSet[i] will be true if vertex i is included in shortest
	// path tree or shortest distance from src to i is finalized

	// Initialize all distances as INFINITE and stpSet[] as false
	for (int i = 0; i < NODES; i++)
		dist[i] = INT_MAX, sptSet[i] = false;

	// Distance of source vertex from itself is always 0
	dist[src] = 0;

	// Find shortest path for all vertices
	for (int count = 0; count < NODES -1; count++) {
		// Pick the minimum distance vertex from the set of vertices not
		// yet processed. u is always equal to src in the first iteration.
		int u = minDistance(dist, sptSet);

		// Mark the picked vertex as processed
		sptSet[u] = true;

		// Update dist value of the adjacent vertices of the picked vertex.
		for (int v = 0; v < NODES; v++)

			// Update dist[v] only if is not in sptSet, there is an edge from
			// u to v, and total weight of path from src to  v through u is
			// smaller than current value of dist[v]
			if (!sptSet[v] && graph[u][v] && dist[u] != INT_MAX
					&& dist[u] + graph[u][v] < dist[v])
				dist[v] = dist[u] + graph[u][v];
	}

	// print the constructed distance array
	//cout<<"Node"<<src<<"::"<<"\n";
	printSolution(dist,src);
}
void find_hop_count_distance(int hp_cnt[NODES][NODES],int diameter,int node)
{
	int node_index;
	for(int i=0;i<NODES;i++)
	{
		if(nodes[i]==node)
		{
			node_index=i;
		}
	}

	#if (PRINTING==1)

	cout<<"-----------------------------------------------------------------------------------------------------------\n";
	cout<<"\t\t\t\t\tNode_id::"<<node<<"\n";
	cout<<"-----------------------------------------------------------------------------------------------------------\n";
	#endif
	for(int k=1;k<=diameter;k++)
	{
		#if (PRINTING==1)

		cout<<"Diameter::"<<std::setw(2) <<std::setfill('0') <<k<<"-->";
		for(int j=0;j<NODES;j++)
		{
			if(hp_cnt[node_index][j]==k)
			{
				cout<<std::setw(2) <<std::setfill('0') <<nodes[j]<<" |";
			}
		}
		cout<<"\n-----------------------------------------------------------------------------------------------------------";
		cout<<"\n";
		#endif

	}


}
int find_network_diameter(int hp_cnt[NODES][NODES])

{
	int max=hp_cnt[0][0];
	for(int i=1;i<NODES;i++)
	{
		for(int j=1;j<NODES;j++)
		{
			if(hp_cnt[i][j]>max)
			{
				max=hp_cnt[i][j];
			}
		}
	}
	return max;
}
void find_interspection_factor(int index)
{
	for(int j=0;j<MAX_NUM_GROUPS-1;j++)
	{
		for(int k=0;k<MAX_NUM_NODES_PER_GROUP;k++)
		{
			group_for_comp[j][k]=0;
		}
	}

	int diameter=find_network_diameter(shortest_distance_matrix);
	cout<<"Network diameter:"<<diameter<<"\n";
	int num_nodes[MAX_NUM_GROUPS][diameter];
	for(int i=0;i<MAX_NUM_GROUPS;i++)
	{
		for(int j=0;j<=diameter;j++)
		{
			num_nodes[i][j]=0;
		}
		cout<<"\n";
	}
	int group_initiator=0;
	int max_value=0;
	for(int i=0;i<MAX_NUM_GROUPS;i++)
	{
		group_initiator=group[index][i][0];
		cout<<"Group Initiator::"<<group_initiator<<"\n";
		cout<<"-----------------------------------------------------------------------------------------------------------\n";


		/**************************************************************/
		/* copy the groups tobe compared to another 2-D array         */
		/**************************************************************/

		int num=0;
		int count_other_group_member=0;
		for(int j=0;j<MAX_NUM_GROUPS;j++)
		{
			if(j!=i)
			{
				for(int k=0;k<MAX_NUM_NODES_PER_GROUP;k++)
				{
					if(group[index][j][k]!=0)
					{
						group_for_comp[num][k]=group[index][j][k];
						count_other_group_member++;
					}
				}
				num=num+1;
			}
		}

		for(int j=0;j<MAX_NUM_GROUPS-1;j++)
		{
			for(int k=0;k<MAX_NUM_NODES_PER_GROUP;k++)
			{
				cout<<group_for_comp[j][k]<<" ";
			}
			cout<<"\n";
		}
		/**************************************************************/
		/* Draw the hop count graph,not much use in calculation of    */
		/* interprese factor                                          */
		/**************************************************************/
		find_hop_count_distance(shortest_distance_matrix,diameter,group_initiator);

		/**************************************************************/
		/*Find the number of nodes of each group at distance 1 to D   */
		/**************************************************************/
		cout<<"GROUP DIAMETER FROM INITIATOR ::"<<diameter<<"\n";
		for(int j=1;j<=diameter;j++)
		{
#if (PRINTING==1)

			cout<<"--------------------------------------------------------------------------------------------------------------------------------\n";
			cout<<"Diameter"<<j<<"\n";
			cout<<"--------------------------------------------------------------------------------------------------------------------------------\n";
			cout<<"\tInitiator\t|"<<"\tTarget Node\t|"<<"\tRow Index\t|"<<"\tColumn Index\t|"<<"\tNode Count\n";
			cout<<"--------------------------------------------------------------------------------------------------------------------------------\n";
#endif
			for(int k=0;k<num;k++)
			{
				for(int l=0;l<MAX_NUM_NODES_PER_GROUP;l++)
				{
					int node_to_compare= group_for_comp[k][l];
					int node_index_dst=-1;
					int node_index_src=-1;
					if(node_to_compare!=0)
					{
						//cout<<node_to_compare<<"\n";
						for(int m=0;m<NODES;m++)
						{
							if(nodes[m]==node_to_compare)
							{
								node_index_dst=m;
							}
							if(nodes[m]==group_initiator)
							{
								node_index_src=m;
							}
						}

						if(shortest_distance_matrix[node_index_src][node_index_dst]==j)
						{
							num_nodes[i][j]++;
						}
					}

				}
			}
		}

		max_value=max_value+(diameter*count_other_group_member);
		cout<<"count_other_group_member::"<<count_other_group_member<<"\n";
		cout<<"Max value :: "<<max_value<<"\n";
		//cout<<"Other grp member::"<<count_other_group_member<<"\n";
		//cout<<"diameter::"<<diameter<<"\n";
	}
	for(int i=0;i<MAX_NUM_GROUPS;i++)
	{
		for(int j=1;j<=diameter;j++)
		{
			cout<<std::setw(2) <<std::setfill('0') << num_nodes[i][j]<<" ";
		}
		cout<<"\n";
	}

	/**************************************************************/
	/* Calculate interspection factor                             */
	/**************************************************************/
	float interspection_factor=0;
#if (PRINTING==1)
	cout<<"--------------------------------------------------------------------------------------------------------------------------------\n";

	cout<<"interspection_factor::\n"<<
			cout<<"--------------------------------------------------------------------------------------------------------------------------------\n";

#endif
	for (int m=1;m<=diameter;m++)
	{
		int sum=0;
		for(int n=0;n<MAX_NUM_GROUPS;n++)
		{
			sum=sum+num_nodes[n][m];
			//cout<<sum<<"\n";
		}
		interspection_factor=interspection_factor+sum*(diameter-(m-1));
		//#if (PRINTING==1)
		cout<<sum<<"*"<<"("<<diameter-(m-1)<<")+";
		//#endif
	}

	/**************************************************************/
	/*Normalizing                                                 */
	/**************************************************************/
	//cout<<"Max value"<<max_value<<"\n";
	float normalized_value = interspection_factor/max_value;


	cout<<"interspection_factor="<<interspection_factor<<"\n";
	cout<<"Normalized interspection_factor="<<normalized_value<<"\n";

}


int main () {
	string line;
#if INDRIYA==1
	ifstream myfile ("/home/jagnyashini/Code_Base/glossy-frame/Centrality/data/Indriya/Neighbour_Discovery/Neighbour_Discovery_sept_12/neighbour_prr.txt");
#endif
#if FLOCKLAB==1
	ifstream myfile ("/home/jagnyashini/Code_Base/glossy-frame/Centrality/data/Flocklab/Neighbour_Discovery/0X07/neighbour_prr.txt");
#endif

	float data[1000][10];
	int row,column;
	int count=0;

	if (myfile.is_open())
	{
		row=0;
		while ( getline (myfile,line) )
		{
			//cout << line << '\n';
			// word variable to store word
			string word;

			// making a string stream
			stringstream iss(line);
			// Read and print each word.
			column=0;
			while (iss >> word)
			{
				stringstream geek(word);
				// sscanf(word, "%d", &data[row][column]);
				geek>>data[row][column];
				// cout<<data[row][column]<<"\t";
				column=column+1;
			}
			row=row+1;
		}
		myfile.close();
	}
	else cout << "Unable to open file";


	/*************************************************************************************************/
	/* Print the elements which are been read from the file and stored in an array                   */
	/*************************************************************************************************/
#if PRINTING==1

	for(int i=0;i<row;i++)
	{
		for(int j=0;j<column;j++)
		{
			cout<<data[i][j]<<"\t";
		}
		cout<<"\n";
	}
#endif


	//  /*************************************************************************************************/
	//  /* Fill in the adjacency matrix    															   */
	//  /*************************************************************************************************/
	float adj_prr_matrix[NODES][NODES];

	for(int i=0;i<NODES;i++)
	{
		for(int j=0; j<NODES; j++)
		{
			adj_prr_matrix[i][j]=0;
			neighbourhood_matrix[i][j]=0;
			shortest_distance_matrix[i][j]=0;
		}
	}
	int src_node,src_node_index=0;
	int dst_node,dst_node_index=0;
	for(int i=0;i<row;i++)
	{
		src_node=data[i][0];
		dst_node=data[i][1];
		for(int j=0;j<NODES;j++)
		{
			if(src_node==nodes[j])
			{
				src_node_index=j;
			}
			if(dst_node==nodes[j])
			{
				dst_node_index=j;
			}
		}
		adj_prr_matrix[src_node_index][dst_node_index]=data[i][2];

	}
	//  /*************************************************************************************************/
	//  /* Print  the adjacency matrix    															   */
	//  /*************************************************************************************************/
#if PRINTING==1

	for(int i=0;i<NODES;i++)
	{
		cout<<nodes[i]<<"\n";

		for(int j=0; j<NODES; j++)
		{
			cout<<adj_prr_matrix[i][j]<<" ";
		}
		cout<<"\n";
	}
#endif
	//  /*************************************************************************************************/
	//  /* Fill in the  neighbourhood matrix    														   */
	//  /*************************************************************************************************/
	for(int i=0;i<NODES;i++)
	{
		for(int j=0; j<NODES; j++)
		{
			if(adj_prr_matrix[i][j]>RELAIBILITY_TARGET)
			{
				neighbourhood_matrix[i][j]=1;
			}
		}
	}

	//  /*************************************************************************************************/
	//  /* Print  the neighbourhood matrix    															   */
	//  /*************************************************************************************************/
#if PRINTING==1

	for(int i=0;i<NODES;i++)
	{			cout<<nodes[i]<<"\n";

	for(int j=0; j<NODES; j++)
	{
		cout<<neighbourhood_matrix[i][j]<<" ";
	}
	cout<<"\n";
	}
#endif

	/*************************************************************************************************/
	/* ruun dijistra over all nodes matrix    														 */
	/*************************************************************************************************/
	for(int i=0;i<NODES;i++)
	{
		dijkstra(neighbourhood_matrix, i);
	}

	//  /*************************************************************************************************/
	//  /* Print  the shortest_distance_matrix matrix    															   */
	//  /*************************************************************************************************/

	for(int i=0;i<NODES;i++)
	{
		//cout<<nodes[i]<<"\n";
		for(int j=0; j<NODES; j++)
		{
			cout<<shortest_distance_matrix[i][j]<<" ";
		}
		cout<<"\n";
	}


	for(int i=0;i<MAX_ENTRIES;i++)
	{cout<<"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";
	find_interspection_factor(i);
	cout<<"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";
	}
}
