// reading a text file
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include<stdio.h>
#include <stdio.h>
#include <limits.h>
using namespace std;
#define NODES 80

int minDistance(int dist[], bool sptSet[])
{
   // Initialize min value
   int min = INT_MAX, min_index;

   for (int v = 0; v < NODES; v++)
     if (sptSet[v] == false && dist[v] <= min)
         min = dist[v], min_index = v;

   return min_index;
}

// A utility function to print the constructed distance array
int compute_closeness(int dist[], int n,int src)
{
 int cnt=0;
 int total_dis=0;
   //printf("Vertex   Distance from Source\n");
 
   for (int i = 0; i < NODES; i++)
   {
	   if(dist[i] <10000 )
	  {
		  total_dis=total_dis+dist[i];
          cnt++;
          //printf("%d\t\t%d\t\t%d\t\t%d\n", cnt,src,i, dist[i]);

	  }
	 if(dist[i] <10000 && src ==22)
	  {
          printf("%d\t\t%d\t\t%d\t\t%d\n", cnt,src,i, dist[i]);

	  }
   }
   return total_dis;

}
int dijkstra(int graph[NODES][NODES], int src)
{
     int dist[NODES];     // The output array.  dist[i] will hold the shortest
                      // distance from src to i

     bool sptSet[NODES]; // sptSet[i] will be true if vertex i is included in shortest
                     // path tree or shortest distance from src to i is finalized

     // Initialize all distances as INFINITE and stpSet[] as false
     for (int i = 0; i < NODES; i++)
        dist[i] = INT_MAX, sptSet[i] = false;

     // Distance of source vertex from itself is always 0
     dist[src] = 0;

     // Find shortest path for all vertices
     for (int count = 0; count < NODES-1; count++)
     {
       // Pick the minimum distance vertex from the set of vertices not
       // yet processed. u is always equal to src in the first iteration.
       int u = minDistance(dist, sptSet);

       // Mark the picked vertex as processed
       sptSet[u] = true;

       // Update dist value of the adjacent vertices of the picked vertex.
       for (int v = 0; v < NODES; v++)

         // Update dist[v] only if is not in sptSet, there is an edge from
         // u to v, and total weight of path from src to  v through u is
         // smaller than current value of dist[v]
         if (!sptSet[v] && graph[u][v] && dist[u] != INT_MAX
                                       && dist[u]+graph[u][v] < dist[v])
            dist[v] = dist[u] + graph[u][v];
     }

     // print the constructed distance array
     int total_dis=compute_closeness(dist, NODES,src);
     return total_dis;
}

int main () {
  string line;
  ifstream myfile ("/home/jagnyashini/Code_Base/glossy-frame/Centrality/data/Indriya/Neighbour_Discovery/Neighbour_Discover_17072019.txt");
  float data[1000][10];
  int row,column;
  int count=0;

  if (myfile.is_open())
  {
	row=0;
    while ( getline (myfile,line) )
    {
      //cout << line << '\n';
      // word variable to store word
         string word;

         // making a string stream
         stringstream iss(line);
         // Read and print each word.
         column=0;
         while (iss >> word)
         {
        	 stringstream geek(word);
        	// sscanf(word, "%d", &data[row][column]);
        	 geek>>data[row][column];
        	 column=column+1;
         }
         row=row+1;
    }
    myfile.close();
  }
  else cout << "Unable to open file";


  /*************************************************************************************************/
  /* Print the elements which are been read from the file and stored in an array                   */
  /*************************************************************************************************/
//  for(int i=0;i<row;i++)
//  {
//	  for(int j=0;j<column;j++)
//	  {
//	  cout<<data[i][j]<<"\t";
//	  }
//   cout<<"\n";
//  }

  /*************************************************************************************************/
  /* Get the list of nodes																		   */
  /*************************************************************************************************/

  int node_list[row];
  int node[100];
  int distinct_count=0;
  //Get List of nodes
  for(int i=0;i<row;i++)
    {
  	  node_list[i]=data[i][0];
    }
  int n = sizeof(node_list)/sizeof(node_list[0]);

  /*************************************************************************************************/
  /* Get the list of distinct nodes																	*/
  /*************************************************************************************************/

  //Get list of distinct nodes
  for (int i=0; i<n; i++)
     {
	  int j;
         // Check if the picked element is already printed
         for (j=0; j<i; j++)
            if (node_list[i] == node_list[j])
                break;

         // If not printed earlier, then print it
         if (i == j)
         {
           node[distinct_count]=node_list[i];
           distinct_count++;
         }
     }
  for(int i=0;i<distinct_count;i++)
      {
    	 cout<<node[i]<<"\t";
      }
  cout<<"\n";


  /*************************************************************************************************/
  /* Fill in the adjacency matrix    															   */
  /*************************************************************************************************/
  int adj_matrix[NODES][NODES];
  for(int i=0;i<NODES;i++)
  {
	  for(int j=0; j<NODES; j++)
	  {
		  adj_matrix[i][j]=0;
	  }
  }
  int src_node;
  int dst_node;
  for(int i=0;i<distinct_count;i++)
        {
      	  for (int j=0; j<row; j++)
      	  {
      		  if(node[i]==int(data[j][0]))
      		  {
      			cout<<data[j][0]<<"\t"<<data[j][1]<<"\n";
      			src_node=data[j][0];
      			dst_node=data[j][1];
      			adj_matrix[src_node][dst_node]=1;
      		  }
      	  }
        }
  /*************************************************************************************************/
  /* Print  the adjacency matrix    															   */
  /*************************************************************************************************/
  for(int i=0;i<NODES;i++)
     {
   	  for(int j=0; j<NODES; j++)
   	  {
   		cout<<adj_matrix[i][j]<<" ";
   	  }
   	  cout<<"\n";
     }


  /*************************************************************************************************/
  /* Fill in the LQI adjacency matrix    														   */
  /*************************************************************************************************/
  int lqi_adj_matrix[NODES][NODES];
    for(int i=0;i<NODES;i++)
    {
  	  for(int j=0; j<NODES; j++)
  	  {
  		lqi_adj_matrix[i][j]=0;
  	  }
    }
    int lqi_src_node;
    int lqi_dst_node;
    for(int i=0;i<distinct_count;i++)
          {
        	  for (int j=0; j<row; j++)
        	  {
        		  if(node[i]==int(data[j][0]))
        		  {
        			//cout<<node[i]<<"\t"<<data[j][0]<<"\t"<<data[j][1]<<"\n";
        			  lqi_src_node=data[j][0];
        			  lqi_dst_node=data[j][1];
        			  lqi_adj_matrix[lqi_src_node][lqi_dst_node]=data[j][2];
        		  }
        	  }
          }
    /*************************************************************************************************/
    /* Print  the LQI adjacency matrix    															 */
    /*************************************************************************************************/
//  for(int i=0;i<NODES;i++)
//    {
//  	  for(int j=0; j<NODES; j++)
//  	  {
//  		cout<< lqi_adj_matrix[i][j]<<" ";
//  	  }
//  	  cout<<"\n";
//    }
    /*************************************************************************************************/
    /*Find the degree centrality of each node                                                        */
    /*************************************************************************************************/
  int degree_centarlity[NODES];
  for(int i=0;i<NODES;i++)
         {
   	  	  degree_centarlity[i]=0;
         }
  for(int i=0;i<NODES;i++)
      {
	  	  for(int j=0;j<NODES;j++)
	  	  {
	  		degree_centarlity[i]=degree_centarlity[i]+adj_matrix[i][j];
	  	  }
      }

  /*************************************************************************************************/
  /*Print the degree centrality of each node                                                       */
  /*************************************************************************************************/
//  for(int i=0;i<NODES;i++)
//        {
//	  	  if(degree_centarlity[i]!=0)
//	  	  {
//	  	  count=count+1;
//  	  	  cout<<count<<"\t"<< i <<"\t" << degree_centarlity[i]<<"\n";
//	  	  }
//        }

  /*************************************************************************************************/
  /*Sort the degree centrality of each node                                                        */
  /*************************************************************************************************/
  int degree_centarlity_sort[NODES];
      int degree_centarlity_sort_index[NODES];
      int temp,index_temp;
      //Copy degree centarlity to closeness_centarlity_sor
      for(int i=0;i<NODES;i++)
          {
    	  degree_centarlity_sort[i]=degree_centarlity[i];
    	  degree_centarlity_sort_index[i]=i;
          }
//      for(int i=0;i<NODES;i++)
//          {
//  	  if(degree_centarlity_sort[i]!=0)
//  	  {
//  	  count=count+1;
//  	  cout<<count<<"\t"<< degree_centarlity_sort_index[i] <<"\t" << degree_centarlity_sort[i]<<"\n";
//  	  }
//  	   }

      for(int i=0;i<NODES;i++)
           {
      		for(int j=i+1;j<NODES;j++)
      		{
      			if(degree_centarlity_sort[j]>degree_centarlity_sort[i])
      			{
      				temp=degree_centarlity_sort[j];
      				degree_centarlity_sort[j]=degree_centarlity_sort[i];
      				degree_centarlity_sort[i]=temp;
      				index_temp=degree_centarlity_sort_index[j];
      				degree_centarlity_sort_index[j]=degree_centarlity_sort_index[i];
      				degree_centarlity_sort_index[i]=index_temp;
      			}
      		}
      		cout<<"\n\n";
           }
      /**************************************************************************************************/
      /*Print the degree centrality of each node                                                        */
      /**************************************************************************************************/
      cout<<"Seqno\t"<<"Node_id\tDegree "<<"\n";
      count=0;
      for(int i=0;i<NODES;i++)
                    {
     	  	  if(degree_centarlity_sort[i]!=0)
     	  	  {
     	  	  count=count+1;
     	  	  cout<<count<<"\t"<< degree_centarlity_sort_index[i] <<"\t" << degree_centarlity_sort[i]<<"\n";
     	  	  }
     	  	   }
      /*************************************************************************************************/
      /*Find the closeness centrality of each node and prints the distance of a node from other        */
      /*************************************************************************************************/
  float closeness_centarlity[NODES];
    for(int i=0;i<NODES;i++)
           {
    	closeness_centarlity[i]=0;
           }
    printf("Seqno \t Nodeid \t Neighbour\t\tDistance \n");
    for(int i=0;i<distinct_count;i++)
          {
    	closeness_centarlity[node[i]]=(distinct_count-1)/float((dijkstra(lqi_adj_matrix, node[i])));
          }
    count=0;


    /*************************************************************************************************/
    /*Sort the closeness centrality of each node                                                     */
    /*************************************************************************************************/
    float closeness_centarlity_sort[NODES];
    float closeness_centarlity_sort_index[NODES];
    float temp2,temp2_index;
    //Copy closeless centarlity to closeness_centarlity_sor
    for(int i=0;i<NODES;i++)
        {
    	closeness_centarlity_sort[i]=closeness_centarlity[i];
    	closeness_centarlity_sort_index[i]=i;
        }
//    for(int i=0;i<NODES;i++)
//        {
//	  if(closeness_centarlity_sort[i]!=0)
//	  {
//	  count=count+1;
//	  cout<<count<<"\t"<< i <<"\t" << closeness_centarlity_sort[i]<<"\n";
//	  }
//	   }

    for(int i=0;i<NODES;i++)
         {
    		for(int j=i+1;j<NODES;j++)
    		{
    			if(closeness_centarlity_sort[j]>closeness_centarlity_sort[i])
    			{
    				temp2=closeness_centarlity_sort[j];
    				closeness_centarlity_sort[j]=closeness_centarlity_sort[i];
    				closeness_centarlity_sort[i]=temp2;
    				temp2_index=closeness_centarlity_sort_index[j];
    				closeness_centarlity_sort_index[j]=closeness_centarlity_sort_index[i];
    				closeness_centarlity_sort_index[i]=temp2_index;
    			}

    		}
         }

	/**************************************************************************************************/
	/*Print the tCloseness centrality of each node                                                    */
	/**************************************************************************************************/
 cout<<"Seqno\t"<<"Node_id\tCloseness "<<"\n";

    for(int i=0;i<NODES;i++)
          {
   	  	  if(closeness_centarlity_sort[i]!=0)
   	  	  {
   	  	  count=count+1;
   	  	  cout<<count<<"\t"<< closeness_centarlity_sort_index[i] <<"\t" << closeness_centarlity_sort[i]<<"\n";
   	  	  }
   	  	   }
}
