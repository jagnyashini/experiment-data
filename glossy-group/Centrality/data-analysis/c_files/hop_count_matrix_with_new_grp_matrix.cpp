#include <iostream>
#include <fstream>
#include <string>
#include<bits/stdc++.h> 

using namespace std;
#define NODES 42	
#define MAX_NUM_GROUPS 2
#define MAX_NUM_NODES_PER_GROUP 25
#define PRINTING 1

//int group[MAX_NUM_GROUPS][MAX_NUM_NODES_PER_GROUP]={{28,35,30,29,27,15,5,13,12,7,1,9,10},{17,19,16,23,52,60,56,51,54,61,57,55,53,58},{18,26,24,20,22,46,41,42,48,44,45,47,40,49}};

//int group[MAX_NUM_GROUPS][MAX_NUM_NODES_PER_GROUP]={{40,42,46,22,24,17,20,16,60,51,61,55,37,30,28,15,5,10,1},{47,48,44,45,18,26,19,52,57,56,49,53,54,27,35,29,13,12,7,9}};
//int group[MAX_NUM_GROUPS][MAX_NUM_NODES_PER_GROUP]={{45,20,22,26,18,46,42,48,40,47,44,24,41,53,49,55,57,56,61,58},{13,17,19,23,52,51,54,10,1,9,35,30,28,60,29,15,5,16,12,7,1,27}};
//int group[MAX_NUM_GROUPS][MAX_NUM_NODES_PER_GROUP]={{26,20,22,24,18,46,42,48,40,47,44,45,41,53,49,55,57,56,61,58},{19,17,16,23,52,51,54,10,1,9,35,30,28,60,29,15,5,13,12,7,1,27}};
int group[MAX_NUM_GROUPS][MAX_NUM_NODES_PER_GROUP]={{45, 40, 48, 41, 42, 47, 44, 46, 18, 24, 22, 17, 35, 28, 19, 27, 20, 26, 23, 52, 16},{49, 54, 1, 7,10, 12, 13, 5, 15, 9, 55, 53, 58, 61, 56, 60, 51, 57, 29, 30}};
int group_for_comp[MAX_NUM_GROUPS-1][MAX_NUM_NODES_PER_GROUP];
int hp_cnt[NODES][NODES];
int hp_cnt_new_matrix[NODES][NODES];

int nodes[NODES-1]={1,5,7,9,10,12,13,15,16,17,18,19,20,22,23,24,26,27,28,29,30,35,40,41,42,44,45,46,47,48,49,51,52,53,54,55,56,57,58,60,61};

void find_hop_count_distance(int hp_cnt[NODES][NODES],int diameter,int node)
{

	for(int i=1;i<NODES;i++)
	{
		if(hp_cnt[i][0]==node)
		{
			int node_id=hp_cnt[i][0];
#if (PRINTING==1)

			cout<<"-----------------------------------------------------------------------------------------------------------\n";
			cout<<"\t\t\t\t\tNode_id::"<<node_id<<"\n";
			cout<<"-----------------------------------------------------------------------------------------------------------\n";
#endif
			for(int k=1;k<=diameter;k++)
			{
#if (PRINTING==1)

				cout<<"Diameter::"<<std::setw(2) <<std::setfill('0') <<k<<"-->";
				for(int j=1;j<NODES;j++)
				{
					if(hp_cnt[i][j]==k)
					{
						cout<<std::setw(2) <<std::setfill('0') <<hp_cnt[0][j]<<" |";
					}
				}
				cout<<"\n-----------------------------------------------------------------------------------------------------------";
				cout<<"\n";
#endif

			}
		}
	}


}
void find_new_hop_count_matrix()
{
	for(int i=0;i<NODES;i++)
	{
		for(int j=0;j<NODES;j++)
		{
			if((hp_cnt[i][j])>=1 && hp_cnt[i][j]<=1.3)
				hp_cnt_new_matrix[i-1][j-1]=hp_cnt[i][j];
			else
				hp_cnt_new_matrix[i-1][j-1]=0;
		}

	}

#if (PRINTING==1)
	cout<<"\n\n--------------------------------------------------------------------------------------------------------------------------------\n";
	cout<<"NEW GRAPH MATRIX"<<"\n";
	cout<<"--------------------------------------------------------------------------------------------------------------------------------\n";

	for(int i=0;i<NODES;i++)
	{
		for(int j=0;j<NODES;j++)
		{
			cout << std::setw(2) <<std::setfill('0') << hp_cnt_new_matrix[i][j] << " ";
		}
		cout<<"\n";
	}
#endif
}
int minDistance(int dist[], bool sptSet[])
{
   // Initialize min value
   int min = INT_MAX, min_index;

   for (int v = 0; v < NODES-1; v++)
     if (sptSet[v] == false && dist[v] <= min)
         min = dist[v], min_index = v;

   return min_index;
}
// A utility function to print the constructed distance array
int compute_closeness(int dist[], int n,int src)
{
 int cnt=0;
 int total_dis=0;
   //printf("Vertex   Distance from Source\n");

   for (int i = 0; i < NODES-1; i++)
   {
	   if(dist[i] <10000 )
	  {
		  total_dis=total_dis+dist[i];
          cnt++;
          //printf("%d\t\t%d\t\t%d\t\t%d\n", cnt,src,i, dist[i]);

	  }
	 if(dist[i] <10000)
	  {
          printf("%d\t\t%d\t\t%d\t\t%d\n", cnt,nodes[src],nodes[i], dist[i]);

	  }
   }
   return total_dis;

}

	int compute_dijistra_fill_new_matrix(int graph[NODES][NODES], int src)
	{
	     int dist[NODES-1];     // The output array.  dist[i] will hold the shortest
	                      // distance from src to i

	     bool sptSet[NODES-1]; // sptSet[i] will be true if vertex i is included in shortest
	                     // path tree or shortest distance from src to i is finalized

	     // Initialize all distances as INFINITE and stpSet[] as false
	     for (int i = 0; i < NODES-1; i++)
	        dist[i] = INT_MAX, sptSet[i] = false;

	     // Distance of source vertex from itself is always 0
	     dist[src] = 0;

	     // Find shortest path for all vertices
	     for (int count = 0; count < NODES-1; count++)
	     {
	       // Pick the minimum distance vertex from the set of vertices not
	       // yet processed. u is always equal to src in the first iteration.
	       int u = minDistance(dist, sptSet);

	       // Mark the picked vertex as processed
	       sptSet[u] = true;

	       // Update dist value of the adjacent vertices of the picked vertex.
	       for (int v = 0; v < NODES-1; v++)

	         // Update dist[v] only if is not in sptSet, there is an edge from
	         // u to v, and total weight of path from src to  v through u is
	         // smaller than current value of dist[v]
	         if (!sptSet[v] && graph[u][v] && dist[u] != INT_MAX
	                                       && dist[u]+graph[u][v] < dist[v])
	            dist[v] = dist[u] + graph[u][v];
	     }

	     // print the constructed distance array
	     int total_dis=compute_closeness(dist, NODES-1,src);
	     return total_dis;
	}

int find_network_diameter(int hp_cnt[NODES][NODES])

{
	int max=hp_cnt[1][1];
	for(int i=1;i<NODES;i++)
	{
		for(int j=1;j<NODES;j++)
		{
			if(hp_cnt[i][j]>max)
			{
				max=hp_cnt[i][j];
			}
		}
	}
	return max;
}
void find_interspection_factor()
{

	for(int j=0;j<MAX_NUM_GROUPS-1;j++)
	{
		for(int k=0;k<MAX_NUM_NODES_PER_GROUP;k++)
		{
			group_for_comp[j][k]=0;
		}
	}

	int diameter=find_network_diameter(hp_cnt);
	cout<<"Network diameter:"<<diameter<<"\n";
	int num_nodes[MAX_NUM_GROUPS][diameter+1];
	for(int i=0;i<MAX_NUM_GROUPS;i++)
	{
		for(int j=0;j<=diameter;j++)
		{
			num_nodes[i][j]=0;
		}
		cout<<"\n";
	}
	int group_initiator=0;
	int max_value=0;
	for(int i=0;i<MAX_NUM_GROUPS;i++)
	{
		group_initiator=group[i][0];
		cout<<"Group Initiator::"<<group_initiator<<"\n";
		int group_diameter=0;
		int row_i=0;
		for(int m=1;m<NODES;m++)
		{
			if (hp_cnt[m][0]==group_initiator)
			{

				row_i=m;
			}

		}
		group_diameter=hp_cnt[row_i][1];
		for(int m=1;m<NODES;m++)
		{
			if(hp_cnt[row_i][m]>group_diameter)
			{
				group_diameter=hp_cnt[row_i][m];
			}
		}

		/**************************************************************/
		/* copy the groups tobe compared to another 2-D array         */
		/**************************************************************/

		int num=0;
		int count_other_group_member=0;
		for(int j=0;j<MAX_NUM_GROUPS;j++)
		{
			if(j!=i)
			{
				for(int k=0;k<MAX_NUM_NODES_PER_GROUP;k++)
				{
					if(group[j][k]!=0)
					{
						group_for_comp[num][k]=group[j][k];
						count_other_group_member++;
					}
				}
				num=num+1;
			}
		}

		for(int j=0;j<MAX_NUM_GROUPS-1;j++)
		{
			for(int k=0;k<MAX_NUM_NODES_PER_GROUP;k++)
			{
				cout<<group_for_comp[j][k]<<" ";
			}
			cout<<"\n";
		}
		/**************************************************************/
		/* Draw the hop count graph,not much use in calculation of    */
		/* interprese factor                                          */
		/**************************************************************/
		find_hop_count_distance(hp_cnt,diameter,group_initiator);

		/**************************************************************/
		/*Find the number of nodes of each group at distance 1 to D   */
		/**************************************************************/
		cout<<"GROUP DIAMETER FROM INITIATOR ::"<<group_diameter<<"\n";
		for(int j=1;j<=group_diameter;j++)
		{
#if (PRINTING==1)

			cout<<"--------------------------------------------------------------------------------------------------------------------------------\n";
			cout<<"Diameter"<<j<<"\n";
			cout<<"--------------------------------------------------------------------------------------------------------------------------------\n";
			cout<<"\tInitiator\t|"<<"\tTarget Node\t|"<<"\tRow Index\t|"<<"\tColumn Index\t|"<<"\tNode Count\n";
			cout<<"--------------------------------------------------------------------------------------------------------------------------------\n";
#endif
			for(int k=0;k<MAX_NUM_GROUPS-1;k++)
			{
				for(int l=0;l<MAX_NUM_NODES_PER_GROUP;l++)
				{
					int target_node=group_for_comp[k][l];
					//cout<<"Target Node::"<<target_node<<"\n";
					if(target_node!=0)
					{
						int row_index=1;
						int column_index=1;
						for(int m=1;m<NODES;m++)
						{
							//cout<<hp_cnt[0][m]<<"\n";
							if (hp_cnt[0][m]==target_node)
							{
								//cout<<"here";
								column_index=m;
							}
							if (hp_cnt[m][0]==group_initiator)
							{

								row_index=m;
							}

						}
						//cout<<"Row index ::"<<row_index<<"\n";
						//cout<<"Column index::"<<column_index<<"\n";
						if(hp_cnt[row_index][column_index]==j)
						{
							num_nodes[i][j]=num_nodes[i][j]+1;
							//cout<<group_initiator<<"::"<<target_node<<"::"<<column_index<<"::"<<row_index<<"::"<<hp_cnt[row_index][column_index];
#if (PRINTING==1)

							cout<<group_initiator<<"\t\t\t|"<<target_node<<"\t\t\t|"<<row_index<<"\t\t\t|"<<column_index<<"\t\t\t|"<<hp_cnt[row_index][column_index];
							cout<<"["<<i<<"]"<<"["<<j<<"]="<<num_nodes[i][j]<<"\n";
							cout<<"--------------------------------------------------------------------------------------------------------------------------------\n";

#endif

						}
					}
				}
			}
		}

		max_value=max_value+(diameter*count_other_group_member);
		cout<<"Max value :: "<<max_value<<"\n";
		cout<<"Other grp member::"<<count_other_group_member<<"\n";
		cout<<"diameter::"<<diameter<<"\n";
	}
	for(int i=0;i<MAX_NUM_GROUPS;i++)
	{
		for(int j=1;j<=diameter;j++)
		{
			cout<<std::setw(2) <<std::setfill('0') << num_nodes[i][j]<<" ";
		}
		cout<<"\n";
	}

	/**************************************************************/
	/* Calculate interspection factor                             */
	/**************************************************************/
	float interspection_factor=0;
#if (PRINTING==1)
	cout<<"--------------------------------------------------------------------------------------------------------------------------------\n";

	cout<<"interspection_factor::\n"<<
			cout<<"--------------------------------------------------------------------------------------------------------------------------------\n";

#endif
	for (int m=1;m<=diameter;m++)
	{
		int sum=0;
		for(int n=0;n<MAX_NUM_GROUPS;n++)
		{
			sum=sum+num_nodes[n][m];
			//cout<<sum<<"\n";
		}
		interspection_factor=interspection_factor+sum*(diameter-(m-1));
#if (PRINTING==1)
		cout<<sum<<"*"<<"("<<diameter-(m-1)<<")+";
#endif
	}

	/**************************************************************/
	/*Normalizing                                                 */
	/**************************************************************/
	//cout<<"Max value"<<max_value<<"\n";
	float normalized_value = interspection_factor/max_value;


	cout<<"interspection_factor="<<interspection_factor<<"\n";
	cout<<"Normalized interspection_factor="<<normalized_value<<"\n";

}
int main () {
	int i;
	for(i=0;i<NODES;i++)
	{
		for(int j=0;j<NODES;j++)
			hp_cnt[i][j]=0;
	}
	for(int j=1;j<NODES;j++)
	{
		hp_cnt[0][j]=nodes[j-1];
		hp_cnt[j][0]=nodes[j-1];
	}

	for(i=0;i<NODES;i++)
	{
		//for(int j=0;j<NODES;j++)
		//cout << std::setw(2) << std::setfill('0') << hp_cnt[i][j] << "  ";

		//cout<<"\n";
	}
	string line;
	ifstream myfile ("result.txt");
	string word;
	i=0;
	int row,column,data;
	int row_insert=0,column_insert=0;
	if (myfile.is_open())
	{
		while ( myfile.good() )
		{
			getline (myfile,line);
			//cout << line << endl;
			stringstream iss(line);
			i=0 ;
			while (iss >> word)
			{
				stringstream geek(word);
				int x = 0;
				geek >> x;
				if (i==0)
					row=x;
				if (i==1)
					column=x;
				if (i==2)
					data=x;
				i=i+1;
			}
			for(int j=1;j<NODES;j++)
			{
				if (hp_cnt[j][0]==row)
				{
					row_insert=j;
				}
				if (hp_cnt[0][j]==column)
				{
					column_insert=j;
				}
			}
			//cout<<row<<" "<<column<<" "<<row_insert<<" "<<column_insert<<"\n";
			hp_cnt[row_insert][column_insert]=data;
		}
		myfile.close();
	}

	else cout << "Unable to open file";
#if (PRINTING==1)

	for(i=0;i<NODES;i++)
	{
		for(int j=0;j<NODES;j++)
			//cout <<  hp_cnt[i][j] << "  ";
			cout << std::setw(2) <<std::setfill('0') << hp_cnt[i][j] << " ";
		cout<<"\n";
	}
#endif
	find_new_hop_count_matrix();

	//for(int i=0;i<NODES-1;i++)
	 //         {
	//compute_dijistra_fill_new_matrix(hp_cnt_new_matrix,1);
	      //    }

//	find_interspection_factor();

	return 0;

}
