import sys
import os
import csv
import math  
import numpy as np
import array as arr
from os import walk
from fileinput import close
#SFD Change,FReq Change Channel change =1
#No Change =2
TYPE_OF_INPUT=1


if TYPE_OF_INPUT==1:
	INITITATOR_FLAG=1
	round_list=[]
	avg_disjoint=0.0
	disjoint_list_final=[]
	max_number_of_nodes=41
	group = arr.array('i', [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]) 
	balanced_value=arr.array('i',[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
	avg_deviation_list=arr.array('f',[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0])
	avg_disjoint_grp_wise_list=arr.array('f',[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0])
	count=0
	
	
	
	fileCreationPath="/home/jagnyashini/Code_Base/experiment-data/glossy-dynamic-group/Data-Analysis/Indriya/Files/"
	for x in balanced_value:
		if(count!=0):
			balanced_value[count]=max_number_of_nodes/count
		count=count+1
	def analyze():
		#print "---------------------------------------------------------------------------------------------------"				
		print "Round","\t", "Number of Nodes","\t","No of grps","\t","Bal Val","\t","Avg Dev","\t","Avg Dis"
		#print "---------------------------------------------------------------------------------------------------"				
		f=[]
		for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/glossy-dynamic-group/Data/Indriya/SFD/SFD_All'):
			f.extend(filenames)
			for name in f:
				fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/glossy-dynamic-group/Data/Indriya/SFD/SFD_All',name)
				f = open(fullName, 'r')
				os.system('rm {}out*'.format(fileCreationPath))
				for line in f:
					line = line.strip()
					columns = line.split()
					try:
						#print columns[4]	
						if(int(columns[4])>30):	
							#print columns			
							round_list.append(int(columns[4]))
							f1=[]
							fileName=fileCreationPath+"out"+str(columns[4])+".txt"
							#print fileName
							f1 = open(fileName, "a")
							f1.write(line)
							f1.write("\n")
							f1.close()		
					except (IndexError,ValueError):
						gotdata = 'null'	
				round_set=set(round_list)
				#print round_set
		f.close()
		f=[]
		f1=[]
		fileName=[]
		for round_ in sorted(round_set):
			initiator_list=[]
			self_rcv=0
			other_rcv=0
			no_rcv=0
			latency=[]
			radio_on=[]
			avg_latency=[]
			avg_radio_on=[]
			sfd_list=[]
			fileName=fileCreationPath+"out"+str(round_)+".txt"
			f1 = open(fileName, "r")
			for line in f1:
				line = line.strip()
				columns = line.split()
				try:
					#if(round_>1200):
					#print  (columns)
					if(int(columns[17])==INITITATOR_FLAG):
						initiator_list.append(int(columns[2]))	
				except (IndexError,ValueError):
					gotdata = 'null'
			#print round_,"\t",initiator_list
			f1.close()
			index=len(initiator_list)
			bal_cnt=balanced_value[index]  
			total_nodes=0
			deviation_list=[]
			normalized_avd_dev=0.0
			avg_deviation=0.0
			avd_disjoint=0.0
			disjoint_list=[]
			print "------------------------------------------------------------------------------------------------------------------------------"
			print round_,"\t","Initiator_List:",set(initiator_list),"\t","Balance Count : ",bal_cnt
			print "------------------------------------------------------------------------------------------------------------------------------"
			for j in sorted(set(initiator_list)):
				f1 = open(fileName, 'r')
				group_list=[]
				length=0
				for line in f1:
					line = line.strip()
					columns = line.split()
					try:
						if(int(columns[15])==j):
							group_list.append(int(columns[2]))	
					except (IndexError,ValueError):
						gotdata = 'null'
				f1.close()
				#print group_list
				total_nodes=len(group_list)+total_nodes
						#print j ,"\t::\t",group_list,"\t",len(group_list)
						#print j ,"\t::\t",len(group_list),"\t",abs(len(group_list)-bal_cnt)
				#print "------------------------------------------------------------------------------------------------------------------------------"
				if(len(initiator_list)==4 or len(initiator_list)==5):
					print j,"\t\t",group_list
				deviation_list.append(abs(len(group_list)-bal_cnt))
				self_grp=0
				other_grp=0
				disjoint=0.0
				length=len(group_list)
						#print length
				if(length>0):
					for node in sorted(set(group_list)):
						other_grp_prnt=0	
						self_grp_prnt=0	
						f2=[]	
						#for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/glossy-group/Centrality/data/Flocklab/Neighbour_Discovery/With_7_14/Neighbour'):
						for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/glossy-group/Centrality/data/Indriya/Neighbour_Discovery/neighbour_prr'):
							f2.extend(filenames)
							for name in f2:
								#fullName2 = os.path.join('/home/jagnyashini/Code_Base/experiment-data/glossy-group/Centrality/data/Flocklab/Neighbour_Discovery/With_7_14/Neighbour',name)	
								fullName2 = os.path.join('/home/jagnyashini/Code_Base/experiment-data/glossy-group/Centrality/data/Indriya/Neighbour_Discovery/neighbour_prr',name)	
								f2 = open(fullName2, 'r')
								for line in f2:
									line = line.strip()
									columns = line.split()	
									try:
												#print node,int(columns[0]),float(columns[2])
										if(node == int(columns[0]) and float(columns[2])>0.5):
													#print node,int(columns[0]),float(columns[2])
											neighbour=int(columns[1])
											if neighbour in set(group_list):
												self_grp=self_grp+1
												self_grp_prnt=self_grp_prnt+1
											else:
												other_grp=other_grp+1
												other_grp_prnt=other_grp_prnt+1
									except (IndexError,ValueError):
											gotdata = 'null'
								f2.close()	
						f1=[]
						f1 = open(fileName, 'r')
						for line in f1:
							line = line.strip()
							columns = line.split()
							try:
								if (node==int(columns[2])):
													#print columns			
									if(int(columns[22])>0 and int(columns[24])>0):
										self_rcv=self_rcv+1
										latency.append(float(columns[20]))
														#print latency
									if (int(columns[23])>0 and int(columns[24])>0 and int(columns[22])==0):
										other_rcv=other_rcv+1
									if (int(columns[22])==0 and int(columns[23])==0):
										no_rcv=no_rcv+1		
									radio_on.append(float(columns[34]))
									sfd_list.append(columns[18])								 
							except (IndexError,ValueError):
								gotdata = 'null'	
							#print node,"\t",self_grp_prnt,"\t",other_grp_prnt
							#print self_grp,"\t",other_grp

					if(self_grp==0 and other_grp==0):
						disjoint=0.0
					else:
						disjoint=float(self_grp)/(float(self_grp)+float(other_grp))
					disjoint_list.append(disjoint)

			avd_disjoint=np.mean(disjoint_list)
			if(total_nodes==max_number_of_nodes):
				disjoint_list_final.append(avd_disjoint)
			avg_deviation=np.mean(deviation_list)
			normalized_avd_dev=avg_deviation/bal_cnt
			avg_latency=np.mean(latency)
			avg_radio_on=np.mean(radio_on)
					#print latency, radio_on
					#print "------------------------------------------------------------------------------------------------------------------------------"
					#print "Total Nodes : ", total_nodes,"\tNumber of Groups :",len(initiator_list),"\t Normalized Average Deviation:",normalized_avd_dev,"\t disjoint:",avd_disjoint
			print round_,"\t", total_nodes,"\t",len(initiator_list),"\t",bal_cnt,"\t",round(avg_deviation,3),"\t",round(avd_disjoint,3),"\t",self_rcv,"\t",other_rcv,"\t",no_rcv,"\t",round(avg_latency,2),"\t",round(avg_radio_on,2),"\t",set(sfd_list)
				#print "-----------------------------------------------------------------------------------------------------------------------------------------------------------------"			
			if(total_nodes==max_number_of_nodes):	
				group[index]=group[index]+1
					#avg_deviation_list[index]=avg_deviation_list[index]+normalized_avd_dev
				avg_deviation_list[index]=avg_deviation_list[index]+avg_deviation
				avg_disjoint_grp_wise_list[index]=avg_disjoint_grp_wise_list[index]+avd_disjoint
		print "No.of Grp","\t","| Count","\t","| Balance_Value","\t","| Avg Dev","\t","| Avg Disjoint Grpwise"
		print "------------------------------------------------------------------------------------------------------------------------------"
		count=0
		for x in group:
			if(count!=0 and group[count]!=0):
				print count,"\t\t|  ",group[count],"\t\t | ",balanced_value[count]  ,"\t\t |  ",avg_deviation_list[count]/group[count],"\t\t |  ",avg_disjoint_grp_wise_list[count]/group[count]	
			count=count+1
	#print disjoint_list_final
		avg_disjoint_final=np.mean(disjoint_list_final)
		print "------------------------------------------------------------------------------------------------------------------------------"
		print "Avg Disjointness", avg_disjoint_final ," | 50-Nodes Iteration:" , len(disjoint_list_final)
		print "------------------------------------------------------------------------------------------------------------------------------"
		
		disjointness_value_Range=[0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
		m=0
		print "Disjointness","\t|\t","Number of rounds"
		print "------------------------------------------------------------------------------------------------------------------------------"
        	for target in disjointness_value_Range:
			if(m<10):
				count=0
				for d in disjoint_list_final:
					if(d>disjointness_value_Range[m] and d<=disjointness_value_Range[m+1]):
						count=count+1
				print disjointness_value_Range[m],"-",disjointness_value_Range[m+1],"\t|\t",count
			m=m+1
if TYPE_OF_INPUT==2:
	INITITATOR_FLAG=1
	round_list=[]
	avg_disjoint=0.0
	disjoint_list_final=[]
	max_number_of_nodes=41
	group = arr.array('i', [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]) 
	balanced_value=arr.array('i',[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
	avg_deviation_list=arr.array('f',[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0])
	avg_disjoint_grp_wise_list=arr.array('f',[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0])
	count=0	
	fileCreationPath="/home/jagnyashini/Code_Base/experiment-data/glossy-dynamic-group/Data-Analysis/Indriya/Files/"
	for x in balanced_value:
		if(count!=0):
			balanced_value[count]=max_number_of_nodes/count
		count=count+1
	def analyze():
		#print "---------------------------------------------------------------------------------------------------"				
		print "Round","\t", "Number of Nodes","\t","No of grps","\t","Bal Val","\t","Avg Dev","\t","Avg Dis"
		#print "---------------------------------------------------------------------------------------------------"				
		f=[]
		for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/glossy-dynamic-group/Data/Indriya/WOC/WOC_All'):
			f.extend(filenames)
			for name in f:
				fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/glossy-dynamic-group/Data/Indriya/WOC/WOC_All',name)
				f = open(fullName, 'r')
				os.system('rm {}out*'.format(fileCreationPath))
				for line in f:
					line = line.strip()
					columns = line.split()
					try:
						#print columns	
						if(int(columns[4])>30):	
							#if int(columns[4])==3128:
							#	print columns			
							round_list.append(int(columns[4]))
							f1=[]
							fileName=fileCreationPath+"out"+str(columns[4])+".txt"
							f1 = open(fileName, "a")
							f1.write(line)
							f1.write("\n")
							f1.close()		
					except (IndexError,ValueError):
						gotdata = 'null'	
				round_set=set(round_list)
		f.close()
		f=[]
		f1=[]
		fileName=[]
		for round_ in sorted(round_set):
			initiator_list=[]
			self_rcv=0
			other_rcv=0
			no_rcv=0
			latency=[]
			radio_on=[]
			avg_latency=[]
			avg_radio_on=[]
			sfd_list=[]
			fileName=fileCreationPath+"out"+str(round_)+".txt"
			f1 = open(fileName, "r")
			for line in f1:
				line = line.strip()
				columns = line.split()
				try:
					#if(round_>1200):
						#print  (columns)
					if(int(columns[17])==INITITATOR_FLAG):
						initiator_list.append(int(columns[2]))	
				except (IndexError,ValueError):
					gotdata = 'null'
			#print round_,"\t",initiator_list
			f1.close()
			index=len(initiator_list)
			bal_cnt=balanced_value[index]  
			total_nodes=0
			deviation_list=[]
			normalized_avd_dev=0.0
			avg_deviation=0.0
			avd_disjoint=0.0
			disjoint_list=[]
					#print "------------------------------------------------------------------------------------------------------------------------------"
					#print i,"\t","Initiator_List:",set(initiator_list),"\t","Balance Count : ",bal_cnt
					#print "------------------------------------------------------------------------------------------------------------------------------"
			for j in sorted(set(initiator_list)):
				f1 = open(fileName, 'r')
				group_list=[]
				length=0
				for line in f1:
					line = line.strip()
					columns = line.split()
					try:
						if(int(columns[15])==j):
							group_list.append(int(columns[2]))	
					except (IndexError,ValueError):
						gotdata = 'null'
				f1.close()
				total_nodes=len(group_list)+total_nodes
						#print j ,"\t::\t",group_list,"\t",len(group_list)
						#print j ,"\t::\t",len(group_list),"\t",abs(len(group_list)-bal_cnt)
						#print "------------------------------------------------------------------------------------------------------------------------------"
						#print i,"\t\t",len((initiator_list)),"\t\t",bal_cnt,"\t\t",j,"\t\t",len(group_list),"\t\t",abs(len(group_list)-bal_cnt)
				deviation_list.append(abs(len(group_list)-bal_cnt))
				self_grp=0
				other_grp=0
				disjoint=0.0
				length=len(group_list)
				print group_list
				if(length>0):
					for node in sorted(set(group_list)):
						other_grp_prnt=0	
						self_grp_prnt=0	
						f2=[]	
						f#for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/glossy-group/Centrality/data/Flocklab/Neighbour_Discovery/With_7_14/Neighbour'):
						for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/glossy-group/Centrality/data/Indriya/Neighbour_Discovery/neighbour_prr'):
							f2.extend(filenames)
							for name in f2:
								#fullName2 = os.path.join('/home/jagnyashini/Code_Base/experiment-data/glossy-group/Centrality/data/Flocklab/Neighbour_Discovery/With_7_14/Neighbour',name)	
								fullName2 = os.path.join('/home/jagnyashini/Code_Base/experiment-data/glossy-group/Centrality/data/Indriya/Neighbour_Discovery/neighbour_prr',name)	
								f2 = open(fullName2, 'r')
								for line in f2:
									line = line.strip()
									columns = line.split()	
									try:
												#print node,int(columns[0]),float(columns[2])
										if(node == int(columns[0]) and float(columns[2])>0.5):
													#print node,int(columns[0]),float(columns[2])
											neighbour=int(columns[1])
											if neighbour in set(group_list):
												self_grp=self_grp+1
												self_grp_prnt=self_grp_prnt+1
											else:
												other_grp=other_grp+1
												other_grp_prnt=other_grp_prnt+1
									except (IndexError,ValueError):
											gotdata = 'null'
								f2.close()	
						f1=[]
						f1 = open(fileName, 'r')
						for line in f1:
							line = line.strip()
							columns = line.split()
							try:
								if (node==int(columns[2])):
									if(int(columns[22])>0 and int(columns[24])>0):
										#print columns			
										self_rcv=self_rcv+1
										latency.append(float(columns[20]))
														#print latency
									if (int(columns[23])>0 and int(columns[24])>0 and int(columns[22])==0):
										other_rcv=other_rcv+1
									if (int(columns[22])==0 and int(columns[23])==0):
										no_rcv=no_rcv+1		
									radio_on.append(float(columns[33]))
									#sfd_list.append(columns[33])								 
							except (IndexError,ValueError):
								gotdata = 'null'	
							#print node,"\t",self_grp_prnt,"\t",other_grp_prnt
							#print self_grp,"\t",other_grp
					if(self_grp==0 and other_grp==0):
						disjoint=0.0
					else:
						disjoint=float(self_grp)/(float(self_grp)+float(other_grp))
					disjoint_list.append(disjoint)
			avd_disjoint=np.mean(disjoint_list)
			if(total_nodes==max_number_of_nodes):
				disjoint_list_final.append(avd_disjoint)
			avg_deviation=np.mean(deviation_list)
			normalized_avd_dev=avg_deviation/bal_cnt
			avg_latency=np.mean(latency)
			avg_radio_on=np.mean(radio_on)
					#print latency, radio_on
					#print "------------------------------------------------------------------------------------------------------------------------------"
					#print "Total Nodes : ", total_nodes,"\tNumber of Groups :",len(initiator_list),"\t Normalized Average Deviation:",normalized_avd_dev,"\t disjoint:",avd_disjoint
			print round_,"\t", total_nodes,"\t",len(initiator_list),"\t",bal_cnt,"\t",round(avg_deviation,3),"\t",round(avd_disjoint,3),"\t",self_rcv,"\t",other_rcv,"\t",no_rcv,"\t",round(avg_latency,2),"\t",round(avg_radio_on,2),"\t",set(sfd_list)
				#print "-----------------------------------------------------------------------------------------------------------------------------------------------------------------"			
			if(total_nodes==max_number_of_nodes):	
				group[index]=group[index]+1
					#avg_deviation_list[index]=avg_deviation_list[index]+normalized_avd_dev
				avg_deviation_list[index]=avg_deviation_list[index]+avg_deviation
				avg_disjoint_grp_wise_list[index]=avg_disjoint_grp_wise_list[index]+avd_disjoint
		print "No.of Grp","\t","| Count","\t","| Balance_Value","\t","| Avg Dev","\t","| Avg Disjoint Grpwise"
		print "------------------------------------------------------------------------------------------------------------------------------"
		count=0
		for x in group:
			if(count!=0 and group[count]!=0):
				print count,"\t\t|  ",group[count],"\t\t | ",balanced_value[count]  ,"\t\t |  ",avg_deviation_list[count]/group[count],"\t\t |  ",avg_disjoint_grp_wise_list[count]/group[count]	
			count=count+1
	#print disjoint_list_final
		avg_disjoint_final=np.mean(disjoint_list_final)
		print "------------------------------------------------------------------------------------------------------------------------------"
		print "Avg Disjointness", avg_disjoint_final ," | 50-Nodes Iteration:" , len(disjoint_list_final)
		print "------------------------------------------------------------------------------------------------------------------------------"
		
		disjointness_value_Range=[0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
		m=0
		print "Disjointness","\t|\t","Number of rounds"
		print "------------------------------------------------------------------------------------------------------------------------------"
        	for target in disjointness_value_Range:
			if(m<10):
				count=0
				for d in disjoint_list_final:
					if(d>disjointness_value_Range[m] and d<=disjointness_value_Range[m+1]):
						count=count+1
				print disjointness_value_Range[m],"-",disjointness_value_Range[m+1],"\t|\t",count
			m=m+1
	
def main():

    if len(sys.argv) != 1:
        print '...'
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()
	
