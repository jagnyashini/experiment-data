import sys
import os
import csv
import math  
import numpy as np
import array as arr
from os import walk
from fileinput import close
INITITATOR_FLAG=1
round_list=[]
avg_disjoint=0.0
disjoint_list_final=[]
max_number_of_nodes=24
group = arr.array('i', [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]) 
balanced_value=arr.array('i',[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
avg_deviation_list=arr.array('f',[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0])
avg_disjoint_grp_wise_list=arr.array('f',[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0])
count=0
for x in balanced_value:
	if(count!=0):
		balanced_value[count]=max_number_of_nodes/count
	count=count+1
def analyze():
	print "---------------------------------------------------------------------------------------------------"				
	print "Round","\t", "Number of Nodes","\t","No of grps","\t","Bal Val","\t","Avg Dev","\t","Avg Dis"
	print "---------------------------------------------------------------------------------------------------"				
	f=[]
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/glossy-dynamic-group/Data/Flocklab/Tp_100/serial'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/glossy-dynamic-group/Data/Flocklab/Tp_100/serial',name)
			f = open(fullName, 'r')
			for line in f:
				line = line.strip()
				columns = line.split()
				try:
					#print columns	
					if(int(columns[4])!=0):				
						round_list.append(int(columns[4]))	
				except (IndexError,ValueError):
					gotdata = 'null'	
			round_set=set(round_list)	
			print sorted(round_set)		
			f=[]
			for i in sorted(round_set):
				f = open(fullName, 'r')
				initiator_list=[]
				for line in f:
					line = line.strip()
					columns = line.split()
					try:
						if(int(columns[4])==i and int(columns[18])==INITITATOR_FLAG):
							#print columns
							initiator_list.append(int(columns[2]))	
					except (IndexError,ValueError):
						gotdata = 'null'
				f.close()
				f=[]			
				index=len(initiator_list)
				#print index,(initiator_list)
				bal_cnt=balanced_value[index]  
				total_nodes=0
				deviation_list=[]
				normalized_avd_dev=0.0
				avg_deviation=0.0
				avd_disjoint=0.0
				disjoint_list=[]
				#print "------------------------------------------------------------------------------------------------------------------------------"
				#print i,"\t","Initiator_List:",set(initiator_list),"\t","Balance Count : ",bal_cnt
				#print "------------------------------------------------------------------------------------------------------------------------------"
				for j in sorted(set(initiator_list)):
					f = open(fullName, 'r')
					group_list=[]
					length=0
					for line in f:
						line = line.strip()
						columns = line.split()
						try:
							if(int(columns[4])==i and int(columns[16])==j):
								group_list.append(int(columns[2]))	
						except (IndexError,ValueError):
							gotdata = 'null'
					f.close()
					total_nodes=len(group_list)+total_nodes
					#print j ,"\t::\t",group_list,"\t",len(group_list)
					#print j ,"\t::\t",len(group_list),"\t",abs(len(group_list)-bal_cnt)
					#print "------------------------------------------------------------------------------------------------------------------------------"
					#print i,"\t\t",len((initiator_list)),"\t\t",bal_cnt,"\t\t",j,"\t\t",len(group_list),"\t\t",abs(len(group_list)-bal_cnt)
					deviation_list.append(abs(len(group_list)-bal_cnt))
					self_grp=0
					other_grp=0
					disjoint=0.0
					length=len(group_list)
					#print length
					if(length>0):
						for node in sorted(set(group_list)):
							other_grp_prnt=0	
							self_grp_prnt=0	
							f2=[]	
							for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/glossy-group/Centrality/data/Flocklab/Neighbour_Discovery/With_7_14/Neighbour'):
								f2.extend(filenames)
								for name in f2:
									fullName2 = os.path.join('/home/jagnyashini/Code_Base/experiment-data/glossy-group/Centrality/data/Flocklab/Neighbour_Discovery/With_7_14/Neighbour',name)	
									f2 = open(fullName2, 'r')
									for line in f2:
										line = line.strip()
										columns = line.split()	
										try:
											#print node,int(columns[0]),float(columns[2])
											if(node == int(columns[0]) and float(columns[2])>0.8):
												#print node,int(columns[0]),float(columns[2])
												neighbour=int(columns[1])
												if neighbour in set(group_list):
													self_grp=self_grp+1
													self_grp_prnt=self_grp_prnt+1
												else:
													other_grp=other_grp+1
													other_grp_prnt=other_grp_prnt+1
										except (IndexError,ValueError):
											gotdata = 'null'
									f2.close()	
							#print node,"\t",self_grp_prnt,"\t",other_grp_prnt
						#print self_grp,"\t",other_grp
						disjoint=float(self_grp)/(float(self_grp)+float(other_grp))
						disjoint_list.append(disjoint)
				avd_disjoint=np.mean(disjoint_list)
				if(total_nodes==max_number_of_nodes):
					disjoint_list_final.append(avd_disjoint)
				avg_deviation=np.mean(deviation_list)
				normalized_avd_dev=avg_deviation/bal_cnt


				#print "------------------------------------------------------------------------------------------------------------------------------"
				#print "Total Nodes : ", total_nodes,"\tNumber of Groups :",len(initiator_list),"\t Normalized Average Deviation:",normalized_avd_dev,"\t disjoint:",avd_disjoint
				print i,"\t\t", total_nodes,"\t\t",len(initiator_list),"\t\t",bal_cnt,"\t\t",round(avg_deviation,3),"\t\t",round(avd_disjoint,3),"\t\t",total_nodes
				print "---------------------------------------------------------------------------------------------------"			
				if(total_nodes==max_number_of_nodes):	
					group[index]=group[index]+1
				#avg_deviation_list[index]=avg_deviation_list[index]+normalized_avd_dev
					avg_deviation_list[index]=avg_deviation_list[index]+avg_deviation
					avg_disjoint_grp_wise_list[index]=avg_disjoint_grp_wise_list[index]+avd_disjoint
	print "No.of Grp","\t","| Count","\t","| Balance_Value","\t","| Avg Dev","\t","| Avg Disjoint Grpwise"
	print "------------------------------------------------------------------------------------------------------------------------------"
	count=0
	for x in group:
		if(count!=0 and group[count]!=0):
			print count,"\t\t|  ",group[count],"\t\t | ",balanced_value[count]  ,"\t\t |  ",avg_deviation_list[count]/group[count],"\t\t |  ",avg_disjoint_grp_wise_list[count]/group[count]
		count=count+1
	#print disjoint_list_final
	avg_disjoint_final=np.mean(disjoint_list_final)
	print "------------------------------------------------------------------------------------------------------------------------------"
	print "Avg Disjointness", avg_disjoint_final ," | 50-Nodes Iteration:" , len(disjoint_list_final)
	print "------------------------------------------------------------------------------------------------------------------------------"
	
	disjointness_value_Range=[0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
	m=0
	print "Disjointness","\t|\t","Number of rounds"
	print "------------------------------------------------------------------------------------------------------------------------------"
        for target in disjointness_value_Range:
		if(m<10):
			count=0
			for d in disjoint_list_final:
				if(d>disjointness_value_Range[m] and d<=disjointness_value_Range[m+1]):
					count=count+1
			print disjointness_value_Range[m],"-",disjointness_value_Range[m+1],"\t|\t",count
		m=m+1
		
	
def main():

    if len(sys.argv) != 1:
        print '...'
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()
	
