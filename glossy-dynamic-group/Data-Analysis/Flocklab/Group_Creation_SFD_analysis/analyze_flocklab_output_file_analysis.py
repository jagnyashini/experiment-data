import sys
import os
import csv
import math  
import numpy as np
import array as arr
from os import walk
from fileinput import close
group_list=[]
def analyze():
	print "---------------------------------------------------------------------------------------------------------------------------------"
	print "No.of Groups    | Disjointness          | R-AVg  | L-Avg  |Ron-Avg | R-AVg  | L-Std  | Ron-Std  | R-Std"
	f=[]
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/glossy-dynamic-group/Data/Flocklab/Dynamic_Group_Flooding/Channel_Change/Result'):
		f.extend(filenames)
		for name in f:
			fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/glossy-dynamic-group/Data/Flocklab/Dynamic_Group_Flooding/Channel_Change/Result',name)
			f = open(fullName, 'r')
			for line in f:
				line = line.strip()
				columns = line.split()
				try:
					#print columns	
					group_list.append(int(columns[2]))
				except (IndexError,ValueError):
					gotdata = 'null'	
	group_set=set(group_list)	
	#print group_set
	disjointness_value_Range=[0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
	for group in sorted(group_set):
		self_rcv=[0,0,0,0,0,0,0,0,0,0,0]
		other_rcv=[0,0,0,0,0,0,0,0,0,0,0]
		no_rcv=[0,0,0,0,0,0,0,0,0,0,0]
		reliability=[[],[],[],[],[],[],[],[],[],[],[]]
		latency=[[],[],[],[],[],[],[],[],[],[],[]]
		radio_on=[[],[],[],[],[],[],[],[],[],[],[]]
		f=[]
		for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/experiment-data/glossy-dynamic-group/Data/Flocklab/Dynamic_Group_Flooding/Channel_Change/Result'):
			f.extend(filenames)
			for name in f:
				fullName = os.path.join('/home/jagnyashini/Code_Base/experiment-data/glossy-dynamic-group/Data/Flocklab/Dynamic_Group_Flooding/Channel_Change/Result',name)
				f = open(fullName, 'r')
				for line in f:
					line = line.strip()
					columns = line.split()
					try:
						if(group==int(columns[2]) ):
							m=0
							for target in disjointness_value_Range:
								reliability_curr=0.0
								#print float(columns[5]),disjointness_value_Range[m],disjointness_value_Range[m+1]
								if(m<10 and float(columns[5])>disjointness_value_Range[m] and float(columns[5])<=disjointness_value_Range[m+1]):
									#print columns
									reliability_curr=float(columns[6])/(float(columns[6])+float(columns[7])+float(columns[8]))
									reliability[m].append(reliability_curr)
									self_rcv[m]=self_rcv[m]+int(columns[6])
									other_rcv[m]=other_rcv[m]+int(columns[7])
									no_rcv[m]=no_rcv[m]+int(columns[8])
									latency[m].append(float(columns[9]))
									radio_on[m].append(float(columns[10]))	
								m=m+1	
					except (IndexError,ValueError):
						gotdata = 'null'
		m=0
		print "---------------------------------------------------------------------------------------------------------------------------------"
		for target in disjointness_value_Range:
			if(m<10):
				latency_avg=0.0
				radio_on_avg=0.0
				if((float(self_rcv[m])+float(other_rcv[m])+float(no_rcv[m]))==0):
					print group,"\t\t|",disjointness_value_Range[m],"-",disjointness_value_Range[m+1],"\t\t|",0
				else:
					latency_avg=np.mean(latency[m])
					radio_on_avg=np.mean(radio_on[m])
					reliability_avg=np.mean(reliability[m])
					latency_sd=np.std(latency[m])
					radio_on_sd=np.std(radio_on[m])
					reliability_sd=np.std(reliability[m])
					#print reliability[m]
					print group,"\t\t|",disjointness_value_Range[m],"-",disjointness_value_Range[m+1],"\t\t|",round(float(self_rcv[m])/(float(self_rcv[m])+float(other_rcv[m])+float(no_rcv[m])),2),"\t",round(latency_avg,2),"\t",round(radio_on_avg,2),"\t",round(reliability_avg,2),"\t",round(latency_sd,2),"\t",round(radio_on_sd,2),"\t",round(reliability_sd,2)
			m=m+1
		
		
"""
	disjointness_value_Range=[0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
	m=0
	print "Disjointness","\t|\t","Number of rounds"
	print "------------------------------------------------------------------------------------------------------------------------------"
        for target in disjointness_value_Range:
		if(m<10):
			count=0
			for d in disjoint_list_final:
				if(d>disjointness_value_Range[m] and d<=disjointness_value_Range[m+1]):
					count=count+1
			print disjointness_value_Range[m],"-",disjointness_value_Range[m+1],"\t|\t",count
		m=m+1
"""		
	
def main():

    if len(sys.argv) != 1:
        print '...'
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()
	
