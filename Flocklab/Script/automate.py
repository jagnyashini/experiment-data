import xml.etree.ElementTree as ET
import base64

from datetime import datetime
from selenium import webdriver

import fileinput
import time
import os

# repo_root = '/Users/Madhav/Documents/chain-trans/apps/glossy-app-sync-test/'
repo_root = '/home/jagnyashini/Code_Base/experiment-data/Flocklab/Sky/'
src_root='/home/jagnyashini/Code_Base/glossy-dynamic-group/apps/glossy-app-mint-test/'
download_path = repo_root + 'data/results/'

ET.register_namespace('',"http://www.flocklab.ethz.ch")
ET.register_namespace('xsi',"http://www.w3.org/2001/XMLSchema-instance")

options = webdriver.firefox.options.Options()
options.headless = False

profile = webdriver.FirefoxProfile()
profile.set_preference("browser.download.folderList", 2)
profile.set_preference("browser.download.manager.showWhenStarting", False)
profile.set_preference("browser.download.dir", download_path)
profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/x-gzip")

driver = webdriver.Firefox(options = options, firefox_profile = profile)

################################### change parameters ###########################################

IDX = []
MINT_N_TX = [1,2,3,4,5]
CHAIN_LENGTH = [5,10,15]

#################################################################################################

def create(idx):
	global setting
	for line in fileinput.input(files=(repo_root+'project-conf.h',repo_root+'glossy-app-sync-test.h'), inplace=True):
		# have your conditions, match the setting acrross headers files
		if '#define MINT_N_TX' in line:
			print('#define MINT_N_TX {}'.format(idx[0]))
		elif '#define CHAIN_LENGTH' in line:
			print('#define CHAIN_LENGTH {}'.format(idx[1]))
		else:
			print(line,end='')

	setting = 'GT'
	for para in idx:
		setting += '_{}'.format(para)		

	os.system('cd {} && sudo make TARGET=sky glossy-app-sync-test.sky'.format(repo_root))
	print('created sky file for {}'.format(setting))

def make():
	global setting
	with open(repo_root+'glossy-mint-app-test.sky', 'rb') as file:
		encoded_string = str(base64.b64encode(file.read()))

	tree = ET.parse(repo_root+'data/flocklab_template.xml')
	root = tree.getroot()


	root[0][2][0].text = '2400' #seconds

	root[0][0].text = setting
	root[0][1].text = 'testing with parameters {}'.format(setting)
	root[1][2].text = setting
	root[3][0].text = setting
	root[3][1].text = setting
	root[3][2].text = 'image for {}'.format(setting)
	root[3][5].text = encoded_string[2:]

	tree.write(repo_root+'data/test.xml',encoding='UTF-8', xml_declaration=True)
	print('create xml config file for {}'.format(setting))


def login():
	driver.get("https://flocklab.ethz.ch/user/login.php")

	username = driver.find_element_by_name("username")
	password = driver.find_element_by_name("password")

	username.send_keys('jagnyash')	
	password.send_keys('12345678')

	button = driver.find_element_by_xpath('//input[@type="submit" and @value="Login"]')
	button.click()

	print('login to flocklab successful')


def upload():
	driver.get("https://flocklab.ethz.ch/user/newtest.php")

	xmlfile = driver.find_element_by_name("xmlfile")
	xmlfile.send_keys(repo_root+'data/test.xml')

	button = driver.find_element_by_xpath('//input[@type="submit" and @value="Create test"]')
	button.click()

	print('uploaded {}'.format(setting))

def wait():
	global setting
	time.sleep(10)
	while True:
		try:
			driver.get("https://flocklab.ethz.ch/user/index.php")
			status = driver.find_element_by_xpath('/html/body/div[1]/div[2]/table/tbody/tr[1]/td[4]/img').get_attribute("alt")

			if status == 'Finished':
				break
				
			end_time = driver.find_element_by_xpath('/html/body/div[1]/div[2]/table/tbody/tr[1]/td[6]/i/span').text
			end_time = end_time.split(' +')[0]
			end_time = datetime.strptime(end_time, '%Y-%m-%d %H:%M:%S')
			now = datetime.now()

			if(now > end_time):
				print('time passed, wait for 120 sec to be sure')
				time.sleep(120)
				status = driver.find_element_by_xpath('/html/body/div[1]/div[2]/table/tbody/tr[1]/td[4]/img').get_attribute("alt")
				if status == 'Finished':
					break
				else:
					print('website got stuck, upload test {} again'.format(setting))
					upload()

			time.sleep(60)

		except:
			print('couldn\'t find some element, fall back 120 sec')
			time.sleep(120)

	print('test {} completed'.format(setting))

def download():
	global setting

	button = driver.find_element_by_xpath('/html/body/div/div[2]/table/tbody/tr[1]/td[7]/img[2]')
	button.click()

	time.sleep(30)

	for file in os.listdir(download_path):
		if file.endswith('.tar.gz'):
			ID = file.split('_')[2].split('.')[0]

			filepath = download_path+file
			imagepath = repo_root + 'glossy-mint-app-test.sky'
			testpath = repo_root + 'data/test.xml'
			oldfolderpath = download_path+ID
			newfolderpath = download_path+setting

			os.system('tar xzf {} -C {}'.format(filepath,download_path))
			os.system('rm {}'.format(filepath))
			os.system('cp {} {}'.format(imagepath,oldfolderpath))
			os.system('cp {} {}'.format(testpath,oldfolderpath))
			os.system('mv {}/ {}/'.format(oldfolderpath, newfolderpath))

	print('downloading test results for {} completed'.format(setting))


setting = ''
for mint_n_tx in MINT_N_TX:
	for chain_length in CHAIN_LENGTH:
			IDX.append((mint_n_tx,chain_length))

login()
for file in os.listdir(src_root):
	if file.endswith('.sky'):
		name = file
		setting = file.split('F_')[1].split('.')[0]
		os.system('cp {}{} {}glossy-mint-app-test.sky'.format(src_root,file,repo_root))
		make()
		upload()
		wait()
		download()
driver.close()
driver.quit()
"""
for idx in 3:
	setting = 'GT'
	for para in idx:
		setting += '_{}'.format(para)	
#	create(idx)
	os.system('mv ')
	make()
	upload()
	wait()
	download()
	print('\n',end='')
"""



