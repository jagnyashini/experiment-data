import xml.etree.ElementTree as ET
import base64

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select

import fileinput
import time
import os


USER = 'jd12@iitbbs.ac.in'
PASS = 'A@12345678'
user_duration = '20'

repo_root = '/home/jagnyashini/Code_Base/experiment-data/Indriya/Sky/'
src_root='/home/jagnyashini/Code_Base/glossy-group/apps/glossy-freq-rr-test/'
download_path = repo_root + 'data/results/'


ET.register_namespace('',"http://www.flocklab.ethz.ch")
ET.register_namespace('xsi',"http://www.w3.org/2001/XMLSchema-instance")

options = webdriver.firefox.options.Options()
# options.headless = True

profile = webdriver.FirefoxProfile()
profile.set_preference("browser.download.folderList", 2)
profile.set_preference("browser.download.manager.showWhenStarting", False)
profile.set_preference("browser.download.dir", download_path)
profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/zip")

caps = DesiredCapabilities().FIREFOX
caps["pageLoadStrategy"] = "eager"

driver = webdriver.Firefox(capabilities=caps, options=options, firefox_profile=profile)

################################### change parameters ###########################################

IDX = []
MINT_N_TX = [1,2,3,4,5]
CHAIN_LENGTH = [5,10,15,20]

#################################################################################################

def load(xpath):
	while True:
		try:
			button = driver.find_element_by_xpath(xpath)
			return button
		except:
			time.sleep(1)

def create(idx):
	global setting
	for line in fileinput.input(files=(repo_root+'project-conf.h',repo_root+'glossy-app-sync-test.h'), inplace=True):
		# have your conditions, match the setting acrross headers files
		if '#define MINT_N_TX' in line:
			print('#define MINT_N_TX {}'.format(idx[0]))
		elif '#define CHAIN_LENGTH' in line:
			print('#define CHAIN_LENGTH {}'.format(idx[1]))
		else:
			print(line,end='')

	setting = 'GT'
	for para in idx:
		setting += '_{}'.format(para)		

	os.system('cd {} && sudo make TARGET=sky glossy-app-sync-test.sky'.format(repo_root))
	print('created sky file for {}'.format(setting))

def login():
	try:	
		driver.get("https://indriya.comp.nus.edu.sg")

		button = load('/html/body/header/div/div[1]/div/div/nav/ul/li[6]/div/div')
		button.click()

		driver.switch_to.window(driver.window_handles[1])

		username = load('//*[@id="identifierId"]')
		username.send_keys(USER)

		button = load('//*[@id="identifierNext"]')
		button.click()

		password = load('/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/div/div/div/div[1]/div/div[1]/input')
		password.send_keys(PASS)
		button = load('//*[@id="passwordNext"]')
		time.sleep(2)
		button.click()

		driver.switch_to.window(driver.window_handles[0])

	except:
		driver.close()
		login()

	print('login to indriya successful')


def upload():

	time.sleep(10)

	try:	
		button = driver.find_element_by_xpath('/html/body/div[6]/div/div[2]/div[1]/div[2]/div[1]/button[1]')
		button.click()

		time.sleep(5)

		print('buttong clikcked')
		jobname = driver.find_element_by_xpath('//*[@id="jobNametxt"]')
		jobname.send_keys(setting)

		select = Select(driver.find_element_by_id('moteTypesList'))
		select.select_by_value('8')

		select = Select(driver.find_element_by_id('dcube_select'))
		select.select_by_value('dcube_no')

		skyfile = driver.find_element_by_xpath('//*[@id="8@inputFile"]')
		skyfile.send_keys(repo_root+'glossy-freq-rr-test.sky')

		button = driver.find_element_by_xpath('//*[@id="btnNextAddJob"]')
		button.click()

		select = Select(driver.find_element_by_id('associationFilesList'))
		select.select_by_value('00@newFile')

		button = driver.find_element_by_xpath('//*[@id="selectAll"]')
		button.click()

		button = driver.find_element_by_xpath('//*[@id="btnAddAssociation"]')
		button.click()		
	except:
		driver.get("https://indriya.comp.nus.edu.sg")
		upload()
		return

	time.sleep(1)

	button = driver.find_element_by_xpath('//*[@id="btnAddJob"]')
	button.click()

	print('uploaded {}'.format(setting))

	time.sleep(10)

	schedule = driver.find_element_by_xpath('//*[@id="btnScheduleNowJobAddJob"]')
	schedule.click()	

def schedule():

	time.sleep(5)

	try:
		duration = driver.find_element_by_xpath('//*[@id="durationJob"]')
		duration.clear()
		duration.send_keys(user_duration)

		timeline = driver.find_element_by_xpath('/html/body/div[6]/div/div[2]/div[1]/div[2]/div[5]/div[2]/div/div/div[4]/div[1]/div/div[2]')
		offset = 5

		while True:
			action = webdriver.common.action_chains.ActionChains(driver)
			action.move_to_element_with_offset(timeline, offset, 0)
			action.double_click().perform()

			print('tried pressing')
			print(offset)

			try:
				new_timeline = driver.find_element_by_xpath('/html/body/div[6]/div/div[2]/div[1]/div[2]/div[5]/div[2]/div/div/div[4]/div[1]/div/div[2]/div')
				status = new_timeline.find_element_by_css_selector('div.vis-item.vis-range.blue.vis-editable')
				break
			except:
				offset += 5


		print('scheduling completed')

	except:
		driver.get("https://indriya.comp.nus.edu.sg")
		time.sleep(5)
		button = driver.find_element_by_xpath('/html/body/div[6]/div/div[2]/div[1]/div[2]/div[1]/div[1]/table/tbody/tr[1]/td[5]')
		button.click()
		schedule()
		return

	button = driver.find_element_by_xpath('//*[@id="btnScheduleJob"]')
	button.click()

	print('scheduled {}'.format(setting))

def download():
	global setting

	try:
		time.sleep(5)

		table =  driver.find_element_by_xpath('//*[@id="jobsTable"]')

		for row in table.find_elements_by_xpath('.//tr'):
			idstr = row.get_attribute('id')
			try:
				if idstr.split('@')[1] == 'jobRow':
					arrow = row.find_element_by_xpath('.//td[3]/div')
					arrow.click()
				elif idstr.split('@')[1] == 'trJobResults':
						remove_button = row.find_element_by_xpath('.//td/ol/li/a[1]')
						download_button = row.find_element_by_xpath('.//td/ol/li/a[2]')
						download_button.click()
						time.sleep(30)
						remove_button.click()
			except:
				continue
	except:
		download()
		return

	for file in os.listdir(download_path):
		if file.endswith('.zip'):
			name = file.split('.')[0]

			filepath = download_path+file
			imagepath = repo_root + 'glossy-freq-rr-test.sky'

			os.system('mkdir {}'.format(download_path+name))
			os.system('tar xzf {} -C {}'.format(filepath,download_path+name))
			os.system('rm {}'.format(filepath))
			os.system('cp {} {}'.format(imagepath,download_path+name))

	print('downloading test results for {} completed'.format(setting))


def clean():
	try:
		time.sleep(5)

		table =  driver.find_element_by_xpath('//*[@id="jobsTable"]')

		for row in table.find_elements_by_xpath('.//tr'):
			idstr = row.get_attribute('id')
			try:
				if idstr.split('@')[1] == 'jobRow':
					try:
						delete = row.find_element_by_xpath('.//td[6]/div')
						delete.click()
						time.sleep(5)
					except:
						arrow = row.find_element_by_xpath('.//td[3]/div')
						arrow.click()
				elif idstr.split('@')[1] == 'trJobResults':
						remove_button = row.find_element_by_xpath('.//td/ol/li/a[1]')
						remove_button.click()
						time.sleep(5)
			except:
				continue
	except:
		clean()

# setting = 'minicast_10_6'		
login()
for file in os.listdir(src_root):
	if file.endswith('.sky'):
		name = file
		# setting = file.split('.')[0]
		setting = file.split('F_')[1].split('.')[0]
		os.system('cp {}{} {}glossy-freq-rr-test.sky'.format(src_root,file,repo_root))
		upload()
		schedule()

download()
clean()

driver.close()
driver.quit()
