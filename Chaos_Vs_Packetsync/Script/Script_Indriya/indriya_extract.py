import re
import time
import pprint
import math as m
import statistics as st

file_list = ['/home/jagnyashini/Code_Base/experiment-data/Chaos_Vs_Packetsync/Result/Indriya_Result/chaos/chaos_37_i_55/{}.csv'.format(file_counter) for file_counter in range(1,70)]

total_data = 37
total_nodes = 37

pp = pprint.PrettyPrinter(indent=4)


pos=0
neg=0
tot=0
data1 = {}
data2 = {}

def get_initial_data(handler):

	lines = handler.readlines()

	for line in lines:
		extract = [word for word in re.split('\s|\n|\||r,',line) if word]

		try:
			node = int(extract[1])
			seqno = int(extract[3])

			if node not in data1.keys():
				data1[node] = {}

			if seqno > 5:
				data1[node][seqno] = extract[8:-1]

		except:
			continue

	
	#pp.pprint(data1)

def get_time_rx_data():
	for node in data1.keys():
		data2[node] = {itr:[] for itr in range(1,total_data+1)}

		for seqno in data1[node].keys():
			itr = 1
			for info in data1[node][seqno]:
				#pp.pprint(data2)
				relay,count = info.split(':')
				count,time = count.split('(')

				relay = int(relay)
				count = int(count)
				time = float(time[:-1])

				while count >= itr:
					data2[node][itr].append(time)
					itr += 1

		for i in data2[node].keys():
			try:
				data2[node][i] = [st.mean(data2[node][i]),st.stdev(data2[node][i])]
			except:
				continue

	# pp.pprint(data2)

def get_percent_data():
	f1 = open('nodes.csv', 'w')
	f1.write('{}\t{}\t{}\t{}\n'.format('node','%','avgtime','stdev'))

	xn = [x for x in range(10,110,10)]
	yn = {x:[] for x in xn}

	for node in sorted(data2.keys()):
		y = {x:data2[node][m.floor((x/100)*total_data)] for x in xn}

		for x in xn:
			try:
				yn[x].append(data2[node][m.floor((x/100)*total_data)][0])
			except:
				continue

		for x in xn:
			try:
				f1.write('{}\t{}\t{}\t{}\n'.format(node, x, y[x][0], y[x][1]))
			except:
				continue

	for x in xn:
		try:
			yn[x] = [st.mean(yn[x]),st.stdev(yn[x])]
		except:
			continue

	f2 = open('network.csv', 'w')
	f2.write('{}\t{}\t{}\n'.format('%','avgtime','stdev'))

	for x in xn:
		try:
			f2.write('{}\t{}\t{}\n'.format(x, yn[x][0], yn[x][1]))
		except:
			continue

	f1.close()
	f2.close()


for file in file_list:
	try:
		handler = open(file,'r')
	except:
		continue

	get_initial_data(handler)

	handler.close()

# pp.pprint(data1)
get_time_rx_data()
# pp.pprint(data2)
get_percent_data()
