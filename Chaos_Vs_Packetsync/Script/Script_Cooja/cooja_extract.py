import re
import time
import math
import pprint
import statistics as st
#import matplotlib.pyplot as plt

file = open('/home/jagnyashini/Code_Base/experiment-data/Chaos_Vs_Packetsync/Result/Cooja_Result/chaos/40_Nodes/40_8/loglistener_a.txt','r')
total_data = 40
total_nodes = 40

pp = pprint.PrettyPrinter(indent=4)

lines = file.readlines()

pos=0
neg=0
tot=0
data1 = {}
data2 = {}

for line in lines:
	extract = [word for word in re.split('\s|\n|\||r,',line) if word]

	# print(extract)
	try:

		node = int(extract[2])
		seqno = int(extract[4])


		if node not in data1.keys():
			data1[node] = {}

		if seqno > 5:
			data1[node][seqno] = extract[9:]

	except:
		continue

pp.pprint(data1)

for node in data1.keys():
	data2[node] = {itr:[] for itr in range(1,total_data+1)}

	for seqno in data1[node].keys():

		itr = 1

		for info in data1[node][seqno]:

			relay,count = info.split(':')
			count,time = count.split('(')

			relay = int(relay)
			count = int(count)
			time = float(time[:-1])

			while count >= itr:
				data2[node][itr].append(time)
				itr += 1


	for i in data2[node].keys():
		data2[node][i] = [st.mean(data2[node][i]),st.stdev(data2[node][i])]

pp.pprint(data2)

f1 = open('nodes.csv', 'w')
f1.write('{}\t{}\t{}\t{}\n'.format('node','%','avgtime','stdev'))

xn = [math.floor((i/100)*total_data) for i in range(10,110,10)]
yn = {x:[] for x in xn}

for node in sorted(data2.keys()):
	y = {x:data2[node][x] for x in xn}

	for x in xn:
		yn[x].append(data2[node][x][0])

	print('for node {}'.format(node))

	for count in y.keys():
		f1.write('{}\t{}\t{}\t{}\n'.format(node,((count/total_nodes*100)), y[count][0], y[count][1]))

for x in yn.keys():
	yn[x] = [st.mean(yn[x]),st.stdev(yn[x])]

f2 = open('network.csv', 'w')
f2.write('{}\t{}\t{}\n'.format('%','avgtime','stdev'))

for count in yn.keys():
	f2.write('{}\t{}\t{}\n'.format(((count/total_nodes*100)), yn[count][0], yn[count][1]))

f1.close()
f2.close()

# seqno = 200
# node = 5

# temp_x = []
# temp_y = []

# for info in data1[seqno][node]:

# 	print(info)

# 	relay,count = info.split(':')
# 	count,time = count.split('(')

# 	relay = int(relay)
# 	count = int(count)
# 	time = float(time[:-1])

# 	temp_x.append(time)
# 	temp_y.append(count)
	
# 	print(relay,count,time)

# print(temp_x)
# print(temp_y)
# plt.plot(temp_x,temp_y,label='node 1 seqno 100')
# plt.xlabel('time taken(ms)')
# plt.ylabel('data1 received')
# plt.title('all to all Ntx=10 Chainlen=40')
# plt.show()
