import sys
import os
import csv
import numpy as np
from os import walk
from fileinput import close
create_nearest_neighbour_lst=0	
def analyze():
	node_count=0
	parent_list = []
	print "Nodeid", "\t","Neighbour","\t","RSSI","\t","LQI","\t","Pkt_snt","\t","pkt_rcv","\t","prr"
	f = []
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Downloads/logs_42232'):

		f.extend(filenames)
		for name in f:

			fullName = os.path.join('/home/jagnyashini/Downloads/logs_42232',name)
			node_list = []
			f = open(fullName, 'r')
			line_no=0;

			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				try:
					#print columns
					#print(columns[8])
					node_list.append(int(columns[2]))
				except (IndexError,ValueError):
					gotdata = 'null'
 			f.close()
			node_set = set(node_list)
			print (node_set)
			#print len(node_set)
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				node_neighbor_list=[]
				for line in f:
					line = line.strip()
					columns = line.split()
					try:
						if int(columns[2]) == i and int(columns[8])!=0 :
							#print int(columns[8])
							node_neighbor_list.append(int(columns[8]))
					except (IndexError,ValueError):
						gotdata = 'null'
	 			f.close()
				node_neighbor_set = set(node_neighbor_list)
				print i,":",sorted(node_neighbor_set)	
				for j in sorted(node_neighbor_set):
					rssi=[]
					packet_received=[]
					packet_sent=[]
					lqi=[]
					average_rssi=0.0
					average_lqi=0.0	
					average_prr=0.0
					f = open(fullName, 'r')
					for line in f:
						line = line.strip()
						columns = line.split()
						#print columns
						try:			
							if(i==int(columns[2]) and j==int(columns[8])):
								packet_received.append(int(columns[9]))
								rssi.append(int(columns[10]))
								lqi.append(int(columns[11]))
							if(j==int(columns[2])):
								packet_sent.append(int(columns[4]))
						except (IndexError,ValueError):
							gotdata = 'null'
					f.close()
					average_rssi=np.mean(rssi)
					average_lqi=np.mean(lqi)
					packet_sent_set=set(packet_sent)
				        #print packet_received
					#print i,"\t",j,"\t",round(average_rssi,2),"\t",round(average_lqi,2),"\t",round(average_prr,2),"\t",len(packet_received),"\t",(packet_sent)
					if(len(packet_sent_set)!=0):
						average_prr=(float(len(packet_received))/float(len(packet_sent_set)))
					#print packet_received
					if(average_prr>=0.1):
						print i,"\t",j,"\t",round(average_rssi,2),"\t",round(average_lqi,2),"\t",round(average_prr,2),"\t",len(packet_received),"\t",(len(packet_sent_set))
						#print i,"\t",j,"\t",round(average_prr,2)	



def main():

    if len(sys.argv) != 1:
        print '...'
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()


