#include <iostream>
#include <fstream>
#include <string>
#include<bits/stdc++.h> 

using namespace std;
#define INDRIYA 0
#define FLOCKLAB 1
#define DCUBE 2

#define TESTBED DCUBE

#define HOP_COUNT_MATRIX_FROM_PRR 1
#define HOP_COUNT_MATRIX_FROM_RELAY 0

#define MAX_NUM_GROUPS 2
#define MAX_NUM_NODES_PER_GROUP 25
#define PRINTING 0
#define TYPE float


#define VIRTUAL 0
#define REAL 1

#if TESTBED==INDRIYA
#define NODES 42
int nodes[NODES-1]={1,5,7,9,10,12,13,15,16,17,18,19,20,22,23,24,26,27,28,29,30,35,40,41,42,44,45,46,47,48,49,51,52,53,54,55,56,57,58,60,61};
#endif
#if TESTBED==FLOCKLAB
#define NODES 28
int nodes[NODES-1]={1, 2, 3, 4, 6,7, 8, 10, 11, 13,14, 15, 16, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 31, 32, 33};
#endif
#if TESTBED==DCUBE
#define NODES 48
int nodes[NODES-1]={100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 200, 201, 202, 203, 204, 205, 206, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227};
#endif

TYPE hp_cnt[NODES][NODES];

void generate_net_file()
{
	cout<<"*Vertices "<<NODES-1<<"\n";
	for(int i=0;i<NODES-1;i++)
	{
		cout<<nodes[i]<<"	 "<<nodes[i]<<"\n";
	}
	cout<<"*Edges "<<"\n";
	for(int i=1;i<NODES;i++)
	{
		for(int j=1;j<NODES;j++)
		{
#if HOP_COUNT_MATRIX_FROM_RELAY==1
			if((hp_cnt[i][j])>=1 && hp_cnt[i][j]<=1.4)
			{
				cout<<hp_cnt[i][0]<<"	 ";
				cout<<hp_cnt[0][j]<<"	 ";
				cout<<hp_cnt[i][j]<<"	 ";
				cout<<"\n";

			}
#endif
#if HOP_COUNT_MATRIX_FROM_PRR==1
			if(hp_cnt[i][j]>=0.8)
			{
				cout<<hp_cnt[i][0]<<"	 ";
				cout<<hp_cnt[0][j]<<"	 ";
				cout<<hp_cnt[i][j]<<"	 ";
				cout<<"\n";

			}
#endif
		}
		//	cout<<"\n";
	}

}
int main () {
	int i;
	for(i=0;i<NODES;i++)
	{
		for(int j=0;j<NODES;j++)
			hp_cnt[i][j]=0;
	}
	for(int j=1;j<NODES;j++)
	{
		hp_cnt[0][j]=nodes[j-1];
		hp_cnt[j][0]=nodes[j-1];
	}

	for(i=0;i<NODES;i++)
	{
		//for(int j=0;j<NODES;j++)
		//cout << std::setw(2) << std::setfill('0') << hp_cnt[i][j] << "  ";

		//cout<<"\n";
	}
	string line;
#if TESTBED==INDRIYA
	ifstream myfile ("/home/jagnyashini/Code_Base/experiment-data/Centrality/data/Indriya/Neighbour_Discovery/neighbour_prr/neighbour_prr.txt");
#endif
#if TESTBED==FLOCKLAB

	ifstream myfile ("/home/jagnyashini/Code_Base/experiment-data/glossy-group/Centrality/data/Flocklab/Neighbour_Discovery/With_7_14/Neighbour/Neighbour.txt");
#endif
#if TESTBED==DCUBE

	ifstream myfile ("/home/jagnyashini/Code_Base/experiment-data/DCUDE_EVALUATION/Neighbour/Neighbour.txt");
#endif

	string word;
	i=0;
	int row,column;
	TYPE data;
	int row_insert=0,column_insert=0;
	if (myfile.is_open())
	{
		while ( myfile.good() )
		{
			getline (myfile,line);
		//	cout << line << endl;
			stringstream iss(line);
			row_insert=-1;
			column_insert=-1;
			i=0 ;
			while (iss >> word)
			{
				stringstream geek(word);
				TYPE x = 0;
				geek >> x;
				if (i==0)
					row=x;
				if (i==1)
					column=x;
				if (i==2)
					data=x;
				i=i+1;
			}
			for(int j=1;j<NODES;j++)
			{
				if (hp_cnt[j][0]==row)
				{
					//cout<<hp_cnt[j][0]<<row<<"\n";
					row_insert=j;
				}
				if (hp_cnt[0][j]==column)
				{
					//cout<<hp_cnt[j][0]<<column<<"\n";
					column_insert=j;
				}
			}
			//cout<<row<<" "<<column<<" "<<row_insert<<" "<<column_insert<<" "<<data<<"\n";
			if(column_insert!=-1 && row_insert!=-1)
			{
			hp_cnt[row_insert][column_insert]=data;
			}
		}
		myfile.close();
	}

	else cout << "Unable to open file";

#if (PRINTING==1)

	for(i=0;i<NODES;i++)
	{
		for(int j=0;j<NODES;j++)
			//cout <<  hp_cnt[i][j] << "  ";
		//	cout << std::setw(2) <<std::setfill('0') << (hp_cnt[i][j]) << " ";
		cout<<"\n";
	}
	cout<<"\n\n";

#endif
	cout<<";";
	//PRINT VIRTUAL NODE_ID(Mapped to sorted order)
	for(i=1;i<NODES;i++)
	{
		#if REAL ==1
		cout<<std::setw(2) <<std::setfill('0') <<int(hp_cnt[0][i])<<";";
		#elif VIRTUAL ==1
		cout<<std::setw(2) <<std::setfill('0') <<i<<";";
		#endif
	}
	cout<<"\n";

	for(i=1;i<NODES;i++)
	{
		for(int j=0;j<NODES;j++)
		{
			if(j==0)
			{
				#if REAL ==1
				cout<<std::setw(2) <<std::setfill('0') <<int(hp_cnt[i][j])<<";";
				#elif VIRTUAL ==1
				cout<<std::setw(2) <<std::setfill('0') <<i<<";";
				#endif
			}
#if HOP_COUNT_MATRIX_FROM_RELAY==1
			else if(hp_cnt[i][j]>=1 && hp_cnt[i][j]<=1.5)
				cout<<std::setw(2) <<std::setfill('0') <<int(hp_cnt[i][j])<<";";
#endif
#if HOP_COUNT_MATRIX_FROM_PRR==1
			else if( hp_cnt[i][j]>=0.75)
				cout<<std::setw(2) <<std::setfill('0') <<1<<";";
#endif
			else
				cout<<std::setw(2) <<std::setfill('0') << 0 <<";";
		}

		cout<<"\n";
	}
	cout<<"\n";



	return 0;

}
